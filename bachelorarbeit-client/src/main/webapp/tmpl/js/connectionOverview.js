/**
 * Created with IntelliJ IDEA 12.
 * User: liebsoer
 * Date: 11.06.13
 * Time: 10:17
 * To change this template use File | Settings | File Templates.
 */

function connection_overview(data) {
    var table = $("<table class='table table-condensed table-hover'>");
    var thead = $("<thead>").append($("<tr>")
        .append($("<th>").text("Icon"))
        .append($("<th>").text("Name"))
        .append($("<th>").text("User"))
        .append($("<th>").text("Type"))
        .append($("<th>").text("URL"))
        .append($("<th>").css("width","190px").text("Actions"))
    );

    var tbody = $("<tbody>");

    for(var i = 0; i < data.length; i++){
        tbody.append(connection_addConnectionLine(data[i]));
    }

    tbody.append($("<tr><td colspan='5'></tr>")
            .append($("<td>")
                .append($("<button class='btn btn-mini btn-primary' type='submit' style='float:right;margin-right:5px;'>Add Connection</button>")
                    .click(function(){
                        $("#site-content").empty().append(connection_connectionView(true)).ready(function(){
                            $(".wikiPageTree").treeview();
                        });

                    })
                )
            )
    );

    table.append(thead);
    table.append(tbody);

    return table;
}

function connection_addConnectionLine(data) {
    return $("<tr>")
        .append($("<td><img src='" + data.iconUrl + "' alt='" + data.iconType + "' width='24' height='24'></td>"))
        .append($("<td>" + data.name + "</td>"))
        .append($("<td>" + data.user + "</td>"))
        .append($("<td><img src='" + data.typeUrl + "' alt='" + data.typeName + "' width='24' height='24'></td>"))
        .append($("<td>" + data.url + "</td>"))
        .append($("<td>")
            .append($("<button type='submit' data-item='" + data.key + "' class='btn btn-mini btn-primary'><i class='icon-eye-open'></i>&nbsp;&nbsp;View&nbsp;&nbsp;</button>")
                .click(function(){
                    $("#site-content").empty().append(connection_connectionView(false, data));
                })
            )
            .append($("<button type='submit' data-item='" + data.key + "' class='btn btn-mini'><i class='icon-download'></i>&nbsp;Import</button>")
                .click(function(){
                    var content = $("#site-content").empty();
                    switch(data.typeName.toLowerCase()){
                        case "jira":
                            $.getJSON(REST_URIS.import.getJIRAFilterValues, function(data){
                                content.append(JIRAImportView(data));
                            });
                            break;
                        case "confluence":
                            $.getJSON(REST_URIS.import.getConfluencePages, function(data){
                                content.append(confluenceImportView(data));
                            });
                            break;
                    }
                })
            )
            .append($("<button type='submit' data-item='" + data.key + "' class='btn btn-mini'><i class='icon-remove-circle'></i>&nbsp;Delete</button>")
                .click(function(){
                    bootbox.confirm("<div style='text-align:center;'>Do you really want to delete <span style='font-weight:bold;'>\"" + data.name + "\"</span>?</div>", function(result){
                        if(result){
                            $.ajax({
                                url: REST_URIS.connection.deleteConnection + "/" + data.key,
                                data: data,
                                dataType: "json",
                                contentType: "application/json; charset=UTF-8",
                                type: "DELETE",
                                accept: "json"
                            })
                            .fail(function(){
                                alert("Deletion wasn't successfully!")
                            })
                            .success(function(){
                                alert("Connection was successfully deleted!");
                            })
                            ;
                            connection_returnToOverview();
                        }
                    });
                })
            )
        )
    ;
}

function connection_connectionView(isNew, data){
    var table = $("<table class='table table-hover table-condensed'>");

    if(!data){
        isNew = true;
    }
    var name = isNew ? "<input type='text' placeholder='Enter connection name...'>" : data.name;
    var description = isNew ? "<input type='text' placeholder='Enter description...'>" : data.description;
    var type = isNew ? "<select><option value='jira'>JIRA</option><option value='confluence'>Confluence</option></select>" : data.typeName;
    var uri = isNew ? "<input type='text' placeholder='Enter connection uri...'>" : data.url;
    var user = isNew ? "<input type='text' placeholder='Enter user name...'>" : data.user;
    var password = isNew ? "<input type='password' placeholder='Enter password...'>" : "******";

    var tbody = $("<tbody>")
        .append($("<tr><th>Name</th><td>" + name + "</td></tr>"))
        .append($("<tr><th>Description</th><td>" + description + "</tr>"))
        .append($("<tr><th>Server Type</th><td>" + type + "</td>"))
        .append($("<tr><th>URI</th><td>" + uri + "</td></tr>"))
        .append($("<tr><th>User name</th><td>" + user + "</td></tr>"))
        .append($("<tr><th>Password</th><td>" + password + "</td></tr>"))

    table.append(tbody);

    var div = $("<div class='container-fluid'>");
    div.append(table);

//    div.append($("<div class='alert alert-success'>Connection was tested successfully</div>"));

//    div.append($("<div class='alert alert-error'>Connection couldn't be established.</div>"));

//    div.append($("<div class='container-fluid float-left pull-left'><button type='submit' class='btn btn-primary'>Test Connection</button></div>"));

    div.append($("<div class='container-fluid float-left pull-left'></div>")
        .append($("<button type='submit' class='btn'>Save</button>")
        .click(function(){
                alert("Connection saved");
                connection_returnToOverview();
        }))
    );

    div.append($("<div class='container-fluid pull-left'></div>")
        .append($("<button type='submit' class='btn'>Cancel</button>")
            .click(function(){
                connection_returnToOverview();
            }))
    );

    return div;


}

function connection_returnToOverview(){
    var content = $("#site-content").empty();

    $.get(REST_URIS.connection.getOverview, function(data){
        content.append(connection_overview(data));
    });
}