/**
 * Created with IntelliJ IDEA 12.
 * User: liebsoer
 * Date: 14.06.13
 * Time: 21:38
 * To change this template use File | Settings | File Templates.
 */

var namespace_jiraImportView = "jiraImportView";

function JIRAImportView(data){
    var container = $("<div id='jiraImportView'>");

    /*
     * Project information
     */
    var prjInfos = $("<div class='container-fluid'>");

    prjInfos.append($("<div class='pull-left " + namespace_jiraImportView + "-project-img'>")
        .append($("<img src='" + data.projectImageUrl +"' + alt='" + data.name + "'>"))
    );
    prjInfos.append($("<div>")
        .append($("<div class='pull-left " + namespace_jiraImportView + "-project-id'><span>ID:</span><span>" + data.projectID + "</span>"))
        .append($("<div class='pull-left " + namespace_jiraImportView + "-project-name'><span>Servername:</span><span>" + data.name + "</span>"))
        .append($("<div class='pull-left " + namespace_jiraImportView + "-project-key'><span>Key:</span><span>" + data.key + "</span>"))
        .append($("<div class='" + namespace_jiraImportView + "-project-amountIssues'><span>Count of entire issues:</span><span>" + data.entireIssues + "</span>"))
    );
    prjInfos.append($("<div class='" + namespace_jiraImportView + "-project-ServerUrl'>")
        .append($("<div class='pull-left'><span>URL: </span></div>"))
        .append($("<div><span>" + data.serverUrl + "</span></div>"))
    );
    prjInfos.append($("<div class='" + namespace_jiraImportView + "-project-description'>")
        .append($("<div class='pull-left'><span>Description: </span></div>"))
        .append($("<span>" + data.description + "</span>"))
    );

    container.append(prjInfos);

    container.append(jiraImportView_addTable("issueTypes", "Available Issue Types", data.issueTypes));
    container.append(jiraImportView_addTable("statusTypes", "Available Status Types", data.statusTypes));
    container.append(jiraImportView_addTable("priorities", "Available Priorities", data.priorities));
    container.append(jiraImportView_addTable("components", "Available Components", data.components));

    var saveButton = $("<div>").css({"float":"left","width":"18.5em"});
    saveButton.append($("<button>Import</button>")
        .attr({"type":"submit","class":"btn btn-primary", "style":"float:right;margin-right:5%;"})
        .click(function(){
            var test = [];
            var obj = {
                "projectId": data.projectID
            }

            var items = items = $("#jiraImportView").find("div[data-checked='true']");
            for(var i = 0; i < items.length; i++){
                var item = $(items[i]);
                test.push(item);

                var type = item.attr("data-type");
                var id = item.attr("data-id");

                if(!obj[type]){
                    obj[type] = [];
                }
                obj[type].push(id);
            }

            alert("From \"jira\" imported:\n" + JSON.stringify(obj));
            connection_returnToOverview();
        })
        .prepend("<i class='icon-ok icon-white'></i> "));
    container.append(saveButton);

    var cancelButton = $("<div>").css({"float":"left","width":"20%"});
    cancelButton.append($("<button type='submit' class='btn' style='float:right;margin-right:5%;'><i class='icon-remove'></i>Cancel</button>").click(function(){
        connection_returnToOverview();
    }));
    container.append(cancelButton);

    return container;
}

function jiraImportView_addTable(type, typeName, data){
    var div = $("<div class='container-fluid'>")
        .append("<div class='" + namespace_jiraImportView + "-title'>" + typeName + "</div>");


    var table = $("<table class='table table-condensed table-hover " + namespace_jiraImportView + "-table'>");

    var thead = $("<thead>");
    var trHead = $("<tr>");

    trHead.append("<th class='" + namespace_jiraImportView + "-checkbox'></th>");
    if(data[0].iconUrl){
        trHead.append("<th class='" + namespace_jiraImportView + "-icon'>Icon</th>");
    }
    trHead.append("<th class='" + namespace_jiraImportView + "-id'>Id</th>");
    trHead.append("<th class='" + namespace_jiraImportView + "-name'>Name</th>");
    trHead.append("<th class='" + namespace_jiraImportView + "-count'>Count</th>");
    trHead.append("<th class='" + namespace_jiraImportView + "-description'>Description</th>");

    thead.append(trHead);
    table.append(thead);

    var tbody = $("<tbody>")

    for(var i in data){
        var item = data[i];
        var tr = $("<tr></tr>").click(function(){
            var checkbox = $(this).find("div[data-checked]");

            checkbox.attr("data-checked", checkbox.attr('data-checked') == "true" ? false : true);
            if(checkbox.attr('data-checked') == "true"){
                $(this).find(".jiraImportView-checkbox").addClass("icon-ok");
            }else{
                $(this).find(".jiraImportView-checkbox").removeClass("icon-ok");
            }
        });

        //tr.append($("<td>").append($("<input type='checkbox' data-type='" + type + "' data-id='" + item.id + "'>")));
        tr.append($("<td>").append($("<div style='width:14px;height:13px;border:thin solid black;margin-top:5px;padding:1px;' data-checked='false' data-type='" + type + "' data-id='" + item.id + "'>").append($("<i class='jiraImportView-checkbox' style='margin-top:-3px;'></i>"))));
        if(item.iconUrl){
            tr.append($("<td>").append($("<img src='" + item.iconUrl + "' alt='" + item.name + "' width='16' height='16'>")));
        }
        tr.append($("<td>").text(item.id));
        tr.append($("<td>").text(item.name));
        tr.append($("<td>").text(item.count));
        tr.append($("<td>").text(item.description));


        tbody.append(tr);
    }

    table.append(tbody);

    div.append(table);

    return div;
}