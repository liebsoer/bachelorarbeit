/**
 * Created with IntelliJ IDEA 12.
 * User: liebsoer
 * Date: 25.06.13
 * Time: 23:38
 * To change this template use File | Settings | File Templates.
 */

function setNavigation(){
    return navigation_mainMenu();


}

function navigation_mainMenu(){
    var ul = $("<ul class='nav nav-tabs'>");

    var homeSection = $("<li data-section='home'><a href='#' class='active'>Home</a></li>")
        .click(function(){
            navigation_contextMenu(this);
        });

    var importSection = $("<li data-section='import'><a href='#'>Import</a></li>")
        .click(function(){
            navigation_contextMenu(this);
        });

    var manageSection = $("<li data-section='manage'><a href='#'>Manage</a></li>")
        .click(function(){
            navigation_contextMenu(this);
        });

    ul.append(homeSection);
    ul.append(importSection);
    ul.append(manageSection);

    return ul;
}

function navigation_contextMenu(link){
    var nav = $("#site-context-nav ul");
    nav.empty();
    nav.append("<li class='nav-header'>Aktionen</li>");

    switch($(link).attr("data-section")){
        case "home":
            break;
        case "import":
            nav.append("<li class='context-action active' data-link='connectionOverview'><a href='#context-tab1' data-toggle='tab'>Connection Overview</a></li>")
            nav.append("<li class='context-action' data-link='importedLinks'><a href='#context-tab2' data-toggle='tab'>Imported Issues</a></li>");

            nav.find("li.context-action").click(function(){
                navigation_setImportContent(this);
            });

            navigation_setImportContent(nav.find("li[data-link='connectionOverview']"));
            break;
        case "manage":
            nav.append("<li class='context-action active' data-link='manageComponentOverview'><a href='context-tab1' data-toggle='tab'>Component Overview</a></li>");
            nav.append("<li class='context-action' data-link='manageComponentCreate'><a href='context-tab1' data-toggle='tab'>Create Component</a></li>");

            nav.find("li.context-action").click(function(){
                navigation_setManageContent(this);
            });

            navigation_setManageContent(nav.find("li[data-link='manageComponentOverview']"));
            break;
    }
}

function navigation_setImportContent(link){
    var content = $("#site-content");
    content.empty();

    switch($(link).attr("data-link")){
        case 'connectionOverview':
            $.get(REST_URIS.connection.getOverview, function(data){
                content.append(connection_overview(data));
            });
            break;
        case 'importedLinks':
            content.append(importedEntryShowView());
            break;
    }
}

function navigation_setManageContent(link){
    var content = $("#site-content");
    content.empty();

    switch($(link).attr("data-link")){
        case "manageComponentOverview":
            $.get(REST_URIS.manage.getComponentsFilter, function(data){
                content.append(components(data));
            })
            ;
            break;
        case "manageComponentCreate":
            content.append(components_createComponent());
            break;
    }
}