/**
 * Created with IntelliJ IDEA 12.
 * User: liebsoer
 * Date: 14.06.13
 * Time: 19:10
 * To change this template use File | Settings | File Templates.
 */

var REST_URIS = {
    "connection":{
        "getOverview": "res/connections.json",
        "deleteConnection": "connection",
        "postConnection": "connection",
        "getConnection": "connection"
    },
    "import": {
        "getJIRAIssues": "res/jiraIssues.json",
        "getJIRAFilterValues": "res/jiraFilterValues.json",
        "getConfluencePages": "res/confluencePages.json",
        "getConfluenceImportedPages": "res/confluenceImportedPages.json",
        "getConfluenceFilterValues": "res/confluenceFilterValues.json",
        "postImport": ""
    },
    "manage": {
        "getComponentsFilter": "res/componentOverviewFilter.json",
        "getComponentType": "res/componentTypes.json",
        "getComponents": "res/componentOverview.json",
        "getComponent": "res/component",
        "postComponent": "component",
        "postComponentType": "componentType",
        "deleteComponent": "component"

    },
    "analyse": {

    },
    "report":{

    }
}