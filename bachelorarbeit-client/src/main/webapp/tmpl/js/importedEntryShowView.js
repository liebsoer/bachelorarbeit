/**
 * Created with IntelliJ IDEA 12.
 * User: liebsoer
 * Date: 17.06.13
 * Time: 20:51
 * To change this template use File | Settings | File Templates.
 * TODO: Confluence Filter
 * TODO: Confluence Results
 */

function importedEntryShowView(){
    var container = $("<div class='container-fluid' id='importedEntryShowView'>");


    var selectBox = $("<div class='container-fluid'><div class='pull-left'>Server Type: </div></div>")
        .append($("<div>")
                .append($("<select id='importedEntryShowView_serverType'>")
                    .change(function(){
                        switch($(this).val()){
                            case "jira":
                                $.getJSON(REST_URIS.import.getJIRAIssues, function(data){
                                    $("#importedEntryShowView_filter").remove();
                                    $("#importedEntryShowView_results").remove();
                                    $("#importedEntryShowView_updateButton").remove();

                                    container.append(importedEntryShowView_JIRAFilter(data));
                                    container.append(importedEntryShowView_addUpdateButton("jira"));
                                });
                                break;
                            case "confluence":
                                $.getJSON(REST_URIS.import.getConfluenceFilterValues, function(data){
                                    $("#importedEntryShowView_filter").remove();
                                    $("#importedEntryShowView_results").remove();
                                    $("#importedEntryShowView_updateButton").remove();

                                    container.append(importedEntryShowView_confluenceFilter(data));
                                    container.append(importedEntryShowView_addUpdateButton("confluence"));
                                });
                                break;

                        }
                    })
                    .append($("<option>Please select...</option>"))
                    .append($("<option value='jira'>JIRA</option>"))
                    .append($("<option value='confluence'>Confluence</option>"))
            )
        )
    ;
    container.append(selectBox);

    container.append("<div class='container-fluid'>");


    return container;
}

function importedEntryShowView_validateJIRAView() {
    if (!$("#importedEntryShowView_filter input[type='checkbox'][data-type='type']:checked").length) {
        alert("Please select at least one issue type!");
        return 0;
    }
    if (!$("#importedEntryShowView_filter input[type='checkbox'][data-type='status']:checked").length) {
        alert("Please select at least one status type!");
        return 0;
    }
    if (!$("#importedEntryShowView_filter input[type='checkbox'][data-type='priority']:checked").length) {
        alert("Please select at least one priority!");
        return 0;
    }
    if (!$("#importedEntryShowView_filter input[type='checkbox'][data-type='component']:checked").length) {
        alert("Please select at least one component!");
        return 0;
    }

    return 1;
}

function importedEntryShowView_validateConfluenceView(){
    if(!$("#importedEntryShowView_filter input[type='checkbox'][data-type='connection']:checked").length){
        alert("Please select at least one connection!");
        return 0;
    }
    if(!$("#importedEntryShowView_filter input[type='checkbox'][data-type='project']:checked").length){
        alert("Please select at least one project!");
        return 0;
    }

    return 1;

}

function importedEntryShowView_addUpdateButton(type){
    return $("<div class='container-fluid' id='importedEntryShowView_updateButton' style='margin-left:40%;position:relative;left:-48px'>")
        .append(
            $("<a class='btn btn-primary' data-type='" + type + "'>Update Results</a>")
                .click(function(){
                    switch(type){
                        case "jira":

                            if(!importedEntryShowView_validateJIRAView()){
                                break;
                            }

                            $("#importedEntryShowView_results").remove();

                            var selects = {
                                "status": [],
                                "type": [],
                                "priority": [],
                                "component": []
                            };

                            $("#importedEntryShowView_filter input[type='checkbox']:checked").each(function(i, item){
                                selects[$(item).attr("data-type")].push($(item).attr("data-value"));
                            });

                            $.getJSON(REST_URIS.import.getJIRAIssues, function(data){
                                var selected = [];
                                $.each(data, function(key, item){
                                    var status = $.inArray(item.status, selects.status) + 1;
                                    var type = $.inArray(item.type, selects.type) + 1;
                                    var priority = $.inArray(item.priority, selects.priority) + 1;
                                    var component = $.inArray(item.component, selects.component) + 1;

                                    if(status && type && priority && component){
                                        selected.push(item);
                                    }
                                });

                                $("#importedEntryShowView").append(importedEntryShowView_JIRAResults(selected));
                            });
                            break;
                        case "confluence":
                            if(!importedEntryShowView_validateConfluenceView()){
                                break;
                            }

                            $("#importedEntryShowView_results").remove();

                            var selects = {
                                "connections": [],
                                "projects": []
                            };

                            $("#importedEntryShowView_connections").find("td input[type='checkbox']:checked").each(function(i, item){
                                selects.connections.push($(item).attr("data-id"));
                            });
                            $("#importedEntryShowView_projects").find("td input[type='checkbox']:checked").each(function(i, item){
                                selects.projects.push($(item).attr("data-id"));
                            });

                            if($("#importedEntryShowView_pageName input[type='checkbox']:checked").length){
                                var pageName = $("#importedEntryShowView_pageName").find("input[type='text']").val();
                                if(!$.isEmptyObject(pageName)){
                                    selects["pageName"] = pageName;
                                }
                            }

                            $.getJSON(REST_URIS.import.getConfluenceImportedPages, function(data){
                                var selected = [];
                                $.each(data, function(key, item){
                                    var connection = $.inArray(item.connection, selects.connections) + 1;
                                    var project = $.inArray(item.project, selects.projects) + 1;
                                    var pageName = selects.pageName ? item.pageName.search(selects.pageName) + 1 : 1;

                                     if(connection && project && pageName){
                                         selected.push(item);
                                     }
                                });

                                $("#importedEntryShowView").append(importedEntryShowView_confluenceResults(selected));
                            });

                            break;
                    }
                })
        )
    ;
}

function importedEntryShowView_JIRAFilter(data){
    var filters = _calcJIRAFilters(data);

    var container = $("<div class='container-fluid' id='importedEntryShowView_filter'>");

    var table = $("<table class='table table-hover table-condensed'>");

    var tbody = _addJIRAFilterLines(filters);

    table.append(tbody);

    container.append(table);

    return container;
}

function _addJIRAFilterLines(data){
    var tbody = $("<tbody>");

    var trType = $("<tr>");
    var countType = 0;
    trType.append("<th>Issue Types (<span></span>): </th>");
    for(var key in data.type){
        var item = data.type[key];
        countType += item.ids.length;
        trType.append("<td><label class='checkbox'>" +
            "<input type='checkbox' data-type='type' data-value='" + item.name + "' checked='checked'><img src='" + CONFIG.images.issueType[item.name.replace(/ /g,"").toLowerCase()] + "' alt='" + item.name + "'>&nbsp;"
            + item.name + " (<span>" + item.ids.length + "</span>)</label></td>");
    }
    trType.find("th span").text(countType);
    tbody.append(trType);

    var trStatus = $("<tr>");
    var countStatus = 0;
    trStatus.append("<th>Status Type (<span></span>): </th>");
    for(var key in data.status){
        var item = data.status[key];
        countStatus += item.ids.length;
        trStatus.append("<td><label class='checkbox'>" +
            "<input type='checkbox' data-type='status' data-value='" + item.name + "' checked='checked'><img src='" + CONFIG.images.statusType[item.name.replace(/ /g,"").toLowerCase()] + "' alt='" + item.name + "'>&nbsp;"
            + item.name + " (<span>" + item.ids.length + "</span>)</label></td>");
    }
    trStatus.find("th span").text(countStatus);
    tbody.append(trStatus);

    var trPriority = $("<tr>");
    var countPriority = 0;
    trPriority.append("<th>Priority (<span></span>): </th>");
    for(var key in data.priority){
        var item = data.priority[key];
        countPriority += item.ids.length;
        trPriority.append("<td><label class='checkbox'>" +
            "<input type='checkbox' data-type='priority' data-value='" + item.name + "' checked='checked'><img src='" + CONFIG.images.priority[item.name.replace(/ /g,"").toLowerCase()] + "' alt='" + item.name + "'>&nbsp;"
            + item.name + " (<span>" + item.ids.length + "</span>)</label></td>");
    }
    trPriority.find("th span").text(countPriority);
    tbody.append(trPriority);

    var trComponent = $("<tr>");
    var countComponent = 0;
    trComponent.append("<th>Component (<span></span>): </th>");
    for(var key in data.component){
        var item = data.component[key];
        countComponent += item.ids.length;
        trComponent.append("<td><label class='checkbox'><input type='checkbox' data-type='component' data-value='" + item.name + "' checked='checked'>" + item.name + " (<span>" + item.ids.length + "</span>)</label></td>");
    }
    trComponent.find("th span").text(countComponent);
    tbody.append(trComponent);

    return tbody;

}

function _calcJIRAFilters(data){
    var filters = {
        "type": {},
        "status": {},
        "priority": {},
        "component": {}
    };

    for(var i = 0; i < data.length; i++){
        var item = data[i];

        var id = item.id;
        var type = item.type.replace(/ /g, "");
        var status = item.status.replace(/ /g, "");
        var priority = item.priority.replace(/ /g, "");
        var component = item.component.replace(/ /g, "");

        if (!filters.type[type]) {
            filters.type[type] = {"ids": [], "imgUrl": item.imgUrl, "name": item.type};
        }
        if (!filters.status[status]) {
            filters.status[status] = {"ids": [], "imgUrl": item.imgUrl, "name": item.status};
        }
        if (!filters.priority[priority]) {
            filters.priority[priority] = {"ids": [], "imgUrl": item.imgUrl, "name": item.priority};
        }
        if (!filters.component[component]) {
            filters.component[component] = {"ids": [], "imgUrl": item.imgUrl, "name": item.component};
        }

        filters.type[type].ids.push(id);
        filters.status[status].ids.push(id);
        filters.priority[priority].ids.push(id);
        filters.component[component].ids.push(id);
    }

    return filters;
}

function importedEntryShowView_JIRAResults(data){
    var container = $("<div class='container-fluid' id='importedEntryShowView_results'>");

    var table = $("<table class='table table-hover table-condensed'>");
    var thead = $("<thead><th></th><th>Issue Type</th><th>Status Type</th><th>Priority</th><th>Key</th><th>Name</th><th>Component</th><th>Resolution</th></thead>");

    var tbody = $("<tbody>");

    for(var i = 0; i < data.length; i++){
        var item = data[i];
        var tr = $("<tr>" +
            "<td>" + (i + 1) + "</td>" +
            "<td><img src='" + CONFIG.images.issueType[item.type.replace(/ /g,"").toLowerCase()] + "' alt='" + item.type + "'></td>" +
            "<td><img src='" + CONFIG.images.statusType[item.status.replace(/ /g,"").toLowerCase()] + "' alt='" + item.status + "'></td>" +
            "<td><img src='" + CONFIG.images.priority[item.priority.replace(/ /g,"").toLowerCase()] + "' alt='" + item.type + "'></td>" +
            "<td>" + item.key + "</td>" +
            "<td>" + item.name + "</td>" +
            "<td>" + item.component + "</td>" +
            "<td>" + item.resolution + "</td>" +
        "</tr>");

        tbody.append(tr);
    }

    table.append(thead).append(tbody);
    container.append(table);

    return container;
}


function importedEntryShowView_confluenceFilter(data){
    var container = $("<div class='container-fluid' id='importedEntryShowView_filter'>");


    var table = $("<table class='table table-hover table-condensed'>");
    var tbody = $("<tbody>");

    var connections = data.connections;
    var connectionLine = $("<tr id='importedEntryShowView_connections'><td></td></tr>");
    var pagesCount = 0;
    for(var i = 0; i < connections.length; i++){
        var item = connections[i];
        pagesCount += parseInt(item.count);
        connectionLine.find("td").append("<label class='checkbox pull-left'><input type='checkbox' data-id='" + item.name + "' data-type='connection' checked='checked'>" + item.name + " (" + item.count + ")</label>")
    }
    connectionLine.prepend($("<th>").append(
        $("<label class='checkbox pull-left' style='font-weight:bold;'></label>")
            .append(
                $("<input type='checkbox' checked='checked'>").change(function(){
                    importedEntryShowView_toggleCheckboxes(this);
                })
            )
            .append(" Connections (" + pagesCount + "): ")
    ));
    tbody.append(connectionLine);


    var projects = data.projects;
    var projectLine = $("<tr id='importedEntryShowView_projects'><td></td></tr>");
    pagesCount = 0;
    for(var i = 0; i < projects.length; i++){
        var item = projects[i];
        pagesCount += parseInt(item.count);
        projectLine.find("td").append("<label class='checkbox pull-left'><input type='checkbox' data-id='" + item.name + "' data-type='project' checked='checked'>" + item.name + " (" + item.count + ")</label>")
    }
    projectLine.prepend($("<th>").append(
        $("<label class='checkbox pull-left' style='font-weight:bold;'></label>")
            .append(
                $("<input type='checkbox' checked='checked'>").change(function(){
                    importedEntryShowView_toggleCheckboxes(this);
                })
            )
            .append(" Projects (" + pagesCount + "): ")
    ));
    tbody.append(projectLine);

    tbody.append(
      $("<tr id='importedEntryShowView_pageName'><th><label class='checkbox pull-left' style='font-weight:bold;'><input type='checkbox' checked='checked'>Page name</label></th><td><input type='text' style='width:97%;' placeholder='Enter substring of page name...'></td></tr>")
    );

    table.append(tbody);

    container.append(table);

    return container;
}

function importedEntryShowView_confluenceResults(data){
    var container = $("<div class='container-fluid' id='importedEntryShowView_results'>");

    var table = $("<table class='table table-hover table-condensed'>");

    var thead = $("<thead><tr><th></th><th>Id</th><th>Page name</th><th>Content preview</th><th>Connection</th><th>Project</th></tr></thead>")
    table.append(thead);

    var tbody = $("<tbody>");
    $(data).each(function(i, item){
        var tr = $("<tr>");
        tr.append("<td>" + (i + 1) + "</td>");
        tr.append("<td>" + item.id + "</td>") ;
        tr.append("<td>" + item.pageName + "</td>");
        var content = item.content;
        if(content.length > 125){
            if(content.charAt(124) == " "){
                content = content.substr(0, 125);
            }else{
                for(var j = 125; j > 0; j--){
                    if(content.charAt(j) == " "){
                        content = content.substr(0, j + 1);
                        break;
                    }
                }
                content += "[...]";
            }
        }
        tr.append("<td>" + content + "</td>");
        tr.append("<td>" + item.connection + "</td>");
        tr.append("<td>" + item.project + "</td>");

        tbody.append(tr);
    });
    table.append(tbody);

    container.append(table);

    return container;
}


function importedEntryShowView_toggleCheckboxes(ele){
    $(ele).parent().parent().next().find("input[type='checkbox']").each(function(i, data){data.checked = $(ele).is(":checked");});
}