/**
 * Created with IntelliJ IDEA 12.
 * User: liebsoer
 * Date: 16.06.13
 * Time: 21:18
 * To change this template use File | Settings | File Templates.
 */

function confluenceImportView(data){

    var container = $("<div class='container-fluid confluenceImportView'>");

    var prjInfo = $("<div class='container-fluid'>");

    prjInfo.append(
        $("<div class='pull-left'>").append(
            $("<img src='" + data.imageUrl + "' alt='" + data.mame + "'>")
        )
    );
    prjInfo.append(
        $("<div>")
            .append("<span>Servername: </span>")
            .append("<span>" + data.name + "</span>")
    );
    prjInfo.append(
        $("<div class='pull-left'>")
            .append("<span>Key: </span>")
            .append("<span>" + data.key + "</span>")
    );
    prjInfo.append(
        $("<div>")
            .append("<span>Id: </span>")
            .append("<span>" + data.id + "</span>")
    );

    container.append(prjInfo);

    var contentTree = $("<div class='container-fluid'>")
        .append(addRootLevel(data.contentTree)
    );

    container.append(contentTree);

    container.append($("<div style='float:left;width:8em;'>").append(
        $("<button type='submit' class='btn btn-primary' style='float:right;margin-right:5%;'><i class='icon-ok icon-white'></i>Import</button>")
            .click(function(){
                connection_returnToOverview();
            })));

    container.append($("<div style='float:left;width:20%;'>").append(
        $("<button type='submit' class='btn' style='float:left;margin-left:5%;'><i class='icon-remove'></i>Cancel</button>")
            .click(function(){
                connection_returnToOverview();
            })
    ));

    return container;
}

function addRootLevel(data){
    var ul = $("<ul class='filetree wikiPageTree'>");
    for(var i = 0; i < data.length; i++){
        var item = data[i];

        var li = $("<li>");
        li.append(addPage("folder", item.name));
        li.append(addLevel(item.data));
        ul.append(li);
    }

    return ul;
}

function addLevel(data){
    var ul = $("<ul>").append("<li class='empty'>");
    for(var i = 0; i < data.length; i++){
        var item = data[i];

        var li = $("<li class='closed'>");

        if(item instanceof Object){
            li.append(addPage("folder", item.name))
            li.append(addLevel(item.data));
        }else{
            li.append(addPage("file", item))
        }

        if(i == data.length - 1){
            li.addClass("last");
        }

        ul.append(li);
    }
    return ul;
}

function addPage(pageType, pageName){
    return $("<input type='checkbox'><span class='" + pageType + " wikiPageName'>" + pageName + "</span>");
}

