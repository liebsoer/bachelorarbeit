/**
 * Created with IntelliJ IDEA 12.
 * User: liebsoer
 * Date: 16.06.13
 * Time: 21:18
 * To change this template use File | Settings | File Templates.
 */

var namespace_confluenceImportView = "confluenceImportView";

function confluenceImportView(projectId, meta){

    var container = $("<div class='container-fluid confluenceImportView'>");

    var prjInfo = $("<div class='container-fluid'>");

    /*prjInfo.append(
        $("<div class='pull-left'>").append(
            $("<img src='" + data.imageUrl + "' alt='" + data.mame + "'>")
        )
    );*/
    prjInfo.append(
        $("<div class='pull-left " +  namespace_confluenceImportView + "-metaInfo'>")
            .append("<span>Servername: </span>")
            .append("<span>" + meta.name + "</span>")
    );
    prjInfo.append(
        $("<div class='pull-left " +  namespace_confluenceImportView + "-metaInfo'>")
            .append("<span>Id: </span>")
            .append("<span>" + meta.id + "</span>")
    );
    prjInfo.append(
        $("<div class='" +  namespace_confluenceImportView + "-metaInfo'>")
            .append("<span>Username: </span>")
            .append("<span>" + meta.username + "</span>")
    );

    prjInfo.append(
        $("<div class='pull-left " +  namespace_confluenceImportView + "-metaInfo'>")
            .append("<span>Key: </span>")
            .append("<span>" + meta.key + "</span>")
    );

    container.append(prjInfo);

    var ul = $("<ul class='filetree wikiPageTree'>");
    ul.treeview({
        url: REST_URIS.import.getConfluenceProjects + "/" + projectId
    });

    var contentTree = $("<div class='container-fluid' id='" + namespace_confluenceImportView + "-pages'>")
        .append(ul)
    ;

    container.append(contentTree);

    container.append($("<div style='float:left;width:8em;'>").append(
        $("<button type='submit' class='btn btn-primary' style='float:right;margin-right:5%;'><i class='icon-ok icon-white'></i>Import</button>")
            .click(function(){
                var pages = [];
                $("#" + namespace_confluenceImportView + "-pages input:checked").each(function(i, item){
                    pages.push($(item).parent().attr("id"));
                });

                $.ajax({
                    url: REST_URIS.import.postConfluencePages + "/" + projectId,
                    type:"POST",
                    data: JSON.stringify(pages),
                    contentType:"application/json; charset=utf-8",
                    dataType:"json",
                    success: function(){
                        bootbox.dialog("Confluence pages successfully imported",[{
                            "label" : "Close",
                            "class" : "btn-success",
                            "callback": function(){
                                connection_returnToOverview();
                            }
                        }]
                        );
                    }
                });

                connection_returnToOverview();
            })));

    container.append($("<div style='float:left;width:20%;'>").append(
        $("<button type='submit' class='btn' style='float:left;margin-left:5%;'><i class='icon-remove'></i>Cancel</button>")
            .click(function(){
                connection_returnToOverview();
            })
    ));

    return container;
}
/*
function addRootLevel(data, projectId){
    var ul = $("<ul class='filetree wikiPageTree'>");
    for(var i = 0; i < data.length; i++){
        var item = data[i];

        var li = $("<li>");
        li.append(
            $(addPage("folder", item.name))
                .attr("data-project", item.key)
                .change(function(){

                    var keys = [];
                    $(this).parent().parent().find("input:checked").each(function(i, item){
                        keys.push($(item).parent().attr("data-project"));
                    });

                    $.getJSON(REST_URIS.import.getConfluencePages + "/" + projectId, {"data": JSON.stringify(keys)}, function(data){
                        alert("success");
                    })
                    .fail(function(a, b, c){
                        alert("fail");
                    })
                    ;
                })

        );

        ul.append(li);
    }

    return ul;
}

function addLevel(data){
    var ul = $("<ul>").append("<li class='empty'>");
    for(var i = 0; i < data.length; i++){
        var item = data[i];

        var li = $("<li class='closed'>");

        if(item instanceof Object){
            li.append(addPage("folder", item.name))
            li.append(addLevel(item.data));
        }else{
            li.append(addPage("file", item))
        }

        if(i == data.length - 1){
            li.addClass("last");
        }

        ul.append(li);
    }
    return ul;
}

function addPage(pageType, pageName){
    return $("<label type='checkbox'><input type='checkbox' class='pull-left'><span class='" + pageType + " wikiPageName' style='margin-left:20px;'>" + pageName + "</span></label>");
}    */

