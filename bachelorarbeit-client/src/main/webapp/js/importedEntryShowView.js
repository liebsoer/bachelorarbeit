/**
 * Created with IntelliJ IDEA 12.
 * User: liebsoer
 * Date: 17.06.13
 * Time: 20:51
 * To change this template use File | Settings | File Templates.
 * TODO: Confluence Filter
 * TODO: Confluence Results
 */

function importedEntryShowView(){
    var container = $("<div class='container-fluid' id='importedEntryShowView'>");


    var selectBox = $("<div class='container-fluid'><div class='pull-left'>Server Type: </div></div>")
        .append($("<div>")
                .append($("<select id='importedEntryShowView_serverType'>")
                    .change(function(){
                        switch($(this).val()){
                            case "jira":
                                $.getJSON(REST_URIS.import.getJIRAIssuesFilter, function(data){
                                    $("#importedEntryShowView_filter").remove();
                                    $("#importedEntryShowView_results").remove();
                                    $("#importedEntryShowView_updateButton").remove();

                                    container.append(importedEntryShowView_JIRAFilter(data));
                                    container.append(importedEntryShowView_addUpdateButton("jira"));
                                });
                                break;
                            case "confluence":
                                $.getJSON(REST_URIS.import.getConfluenceFilterValues, function(data){
                                    $("#importedEntryShowView_filter").remove();
                                    $("#importedEntryShowView_results").remove();
                                    $("#importedEntryShowView_updateButton").remove();

                                    container.append(importedEntryShowView_confluenceFilter(data));
                                    container.append(importedEntryShowView_addUpdateButton("confluence"));
                                });
                                break;

                        }
                    })
                    .append($("<option>Please select...</option>"))
                    .append($("<option value='jira'>JIRA</option>"))
                    .append($("<option value='confluence'>Confluence</option>"))
            )
        )
    ;
    container.append(selectBox);

    container.append("<div class='container-fluid'>");


    return container;
}

function importedEntryShowView_validateJIRAView() {
    if (!$("#importedEntryShowView_filter input[type='checkbox'][data-type='issuetype']:checked").length) {
        alert("Please select at least one issue type!");
        return 0;
    }
    if (!$("#importedEntryShowView_filter input[type='checkbox'][data-type='status']:checked").length) {
        alert("Please select at least one status type!");
        return 0;
    }
    if (!$("#importedEntryShowView_filter input[type='checkbox'][data-type='priority']:checked").length) {
        alert("Please select at least one priority!");
        return 0;
    }
    if (!$("#importedEntryShowView_filter input[type='checkbox'][data-type='components']:checked").length) {
        alert("Please select at least one component!");
        return 0;
    }

    return 1;
}

function importedEntryShowView_validateConfluenceView(){
    /*if(!$("#importedEntryShowView_filter input[type='checkbox'][data-type='connection']:checked").length){
        alert("Please select at least one connection!");
        return 0;
    }*/
    if(!$("#importedEntryShowView_filter input[type='checkbox'][data-type='project']:checked").length){
        alert("Please select at least one project!");
        return 0;
    }

    return 1;

}

function importedEntryShowView_addUpdateButton(type, options){
    var isCheckbox = false;
    var isBootbox = false;

    if(typeof options !== "undefined"){
        if(typeof options["isCheckbox"] !== "undefined"){isCheckbox = options.isCheckbox}
        if(typeof options["isBootbox"] !== "undefined"){isBootbox = options.isBootbox}
    }

    return $("<div class='container-fluid' id='importedEntryShowView_updateButton' style='margin-left:40%;position:relative;left:-48px'>")
        .append(
            $("<a class='btn btn-primary' data-type='" + type + "'>Update Results</a>")
                .click(function(){
                    switch(type){
                        case "jira":

                            if(!importedEntryShowView_validateJIRAView()){
                                break;
                            }

                            $("#importedEntryShowView_results").remove();

                            var selects = {};

                            $("#importedEntryShowView_filter input[type='checkbox']:checked").each(function(i, item){
                                if(!selects[$(item).attr("data-type")]){
                                    selects[$(item).attr("data-type")] = [];
                                }

                                selects[$(item).attr("data-type")].push($(item).attr("data-value"));
                            });

                            $.getJSON(REST_URIS.import.getJIRAIssues, {data: JSON.stringify(selects)}, function(data){
                                var selected = [];
                                $.each(data, function(key, item){
                                    var status = $.inArray(item.status, selects.status) + 1;
                                    var type = $.inArray(item.type, selects.type) + 1;
                                    var priority = $.inArray(item.priority, selects.priority) + 1;
                                    var component = $.inArray(item.component, selects.component) + 1;

                                    if(status && type && priority && component){
                                        selected.push(item);
                                    }
                                });

                                $("#importedEntryShowView").append(importedEntryShowView_JIRAResults(data, isCheckbox));
                            });
                            break;
                        case "confluence":
                            if(!importedEntryShowView_validateConfluenceView()){
                                break;
                            }

                            $("#importedEntryShowView_results").remove();

                            var selects = {};

                            /*$("#importedEntryShowView_connections").find("td input[type='checkbox']:checked").each(function(i, item){
                                selects.connections.push($(item).attr("data-id"));
                            });    */
                            var projects = [];
                            $("#importedEntryShowView_projects").find("td input[type='checkbox']:checked").each(function(i, item){
                                projects.push($(item).attr("data-id"));
                            });
                            selects["projects"] = projects;

                            if($("#importedEntryShowView_pageName input[type='checkbox']:checked").length){
                                var pageName = $("#importedEntryShowView_pageName").find("input[type='text']").val();
                                if(!$.isEmptyObject(pageName)){
                                    selects["pageName"] = pageName;
                                }
                            }

                            $.getJSON(REST_URIS.import.getConfluenceImportedPages, selects, function(data){
                                var selected = [];
                                /*$.each(data, function(key, item){
                                    var connection = $.inArray(item.connection, selects.connections) + 1;
                                    var project = $.inArray(item.project, selects.projects) + 1;
                                    var pageName = selects.pageName ? item.pageName.search(selects.pageName) + 1 : 1;

                                     if(connection && project && pageName){
                                         selected.push(item);
                                     }
                                });*/



                                if(isBootbox){
                                    $("#importedEntryShowView").parent().parent()
                                        .animate({
                                            "width": "80%",
                                            "left": "0",
                                            "margin-left": "10%"
                                        }, 200)
                                    ;
                                }
                                $("#importedEntryShowView").append(importedEntryShowView_confluenceResults(data, isCheckbox));
                            });

                            break;
                    }
                })
        )
    ;
}

function importedEntryShowView_JIRAFilter(data){
    var container = $("<div class='container-fluid' id='importedEntryShowView_filter'>");

    var table = $("<table class='table table-hover table-condensed'>");

    var tbody = _addJIRAFilterLines(data);

    table.append(tbody);

    container.append(table);

    return container;
}

function _addJIRAFilterLines(data){
    var tbody = $("<tbody>");

    for(var typeKey in data){
        var type = data[typeKey];
        var tr = $("<tr>");
        var count = 0;
        tr.append("<th>" + typeKey + " (<span></span>): </th>");
        for(var key in type){
            var item = type[key];
            count += parseInt(item);
            var td = $("<td>");
            var label = $("<label class='checkbox'></label>");
            label.append("<input type='checkbox' data-type='" + typeKey + "' data-value='" + key + "' checked='checked'>");
            label.append(key + " (<span>" + item + "</span>)");
            td.append(label);
            tr.append(td);
        }

        tr.find("th span").text(count);
        tbody.append(tr);

    }

    /*
    var trType = $("<tr>");
    var countType = 0;
    trType.append("<th>Issue Types (<span></span>): </th>");
    for(var key in data.issuetype){
        var item = data.issuetype[key];
        countType += parseInt(item);
        trType.append("<td><label class='checkbox'>" +
            "<input type='checkbox' data-type='type' data-value='" + key + "' checked='checked'><img src='" + CONFIG.images.issueType[key.replace(/ /g,"").toLowerCase()] + "' alt='" + "" + "'>&nbsp;"
            + key + " (<span>" + item + "</span>)</label></td>");
    }
    trType.find("th span").text(countType);
    tbody.append(trType);

    var trStatus = $("<tr>");
    var countStatus = 0;
    trStatus.append("<th>Status Type (<span></span>): </th>");
    for(var key in data.status){
        var item = data.status[key];
        countStatus += parseInt(item);
        trStatus.append("<td><label class='checkbox'>" +
            "<input type='checkbox' data-type='status' data-value='" + key + "' checked='checked'><img src='" + CONFIG.images.statusType[key.replace(/ /g,"").toLowerCase()] + "' alt='" + "" + "'>&nbsp;"
            + key + " (<span>" + item + "</span>)</label></td>");
    }
    trStatus.find("th span").text(countStatus);
    tbody.append(trStatus);

    var trPriority = $("<tr>");
    var countPriority = 0;
    trPriority.append("<th>Priority (<span></span>): </th>");
    for(var key in data.priority){
        var item = data.priority[key];
        countPriority += parseInt(item);
        trPriority.append("<td><label class='checkbox'>" +
            "<input type='checkbox' data-type='priority' data-value='" + key + "' checked='checked'><img src='" + CONFIG.images.priority[key.replace(/ /g,"").toLowerCase()] + "' alt='" + "" + "'>&nbsp;"
            + key + " (<span>" + item + "</span>)</label></td>");
    }
    trPriority.find("th span").text(countPriority);
    tbody.append(trPriority);

    var trComponent = $("<tr>");
    var countComponent = 0;
    trComponent.append("<th>Component (<span></span>): </th>");
    for(var key in data.component){
        var item = data.component[key];
        countComponent += parseInt(item);
        trComponent.append("<td><label class='checkbox'><input type='checkbox' data-type='component' data-value='" + key + "' checked='checked'>" + key + " (<span>" + item + "</span>)</label></td>");
    }
    trComponent.find("th span").text(countComponent);
    tbody.append(trComponent);

    var trProject = $("<tr>");
    var countProject = 0;
    trProject.append("<th>Component (<span></span>): </th>");
    for(var key in data.project){
        var item = data.project[key];
        countProject += parseInt(item);
        trProject.append("<td><label class='checkbox'><input type='checkbox' data-type='component' data-value='" + key + "' checked='checked'>" + key + " (<span>" + item + "</span>)</label></td>");
    }
    trProject.find("th span").text(countProject);
    tbody.append(trProject); */

    return tbody;

}

function importedEntryShowView_JIRAResults(data, isCheckbox){
    var container = $("<div class='container-fluid' id='importedEntryShowView_results' style='max-height:350px;'>");

    var table = $("<table class='table table-hover table-condensed'>");
    var thead = $("<thead><tr><th></th><th>Issue Type</th><th>Status Type</th><th>Priority</th><th>Key</th><th>Name</th><th>Component</th><th>Resolution</th></tr></thead>");
    if(typeof isCheckbox !== "undefined" && isCheckbox) thead.find("tr").prepend("<th>");

    var tbody = $("<tbody>");

    for(var i = 0; i < data.length; i++){
        var item = data[i];
        var tr = $("<tr>" +
            "<td>" + (i + 1) + "</td>" +
            "<td><img src='" + CONFIG.images.issueType[item.type.replace(/ /g,"").toLowerCase()] + "' alt='" + item.type + "'></td>" +
            "<td><img src='" + CONFIG.images.statusType[item.status.replace(/ /g,"").toLowerCase()] + "' alt='" + item.status + "'></td>" +
            "<td><img src='" + CONFIG.images.priority[item.priority.replace(/ /g,"").toLowerCase()] + "' alt='" + item.priority + "'></td>" +
            "<td>" + item.key + "</td>" +
            "<td>" + item.name + "</td>" +
            "<td>" + item.components.replace(/([\[\]"])/g,"") + "</td>" +
            "<td>" + item.resolution + "</td>" +
        "</tr>");
        if(typeof isCheckbox !== "undefined" && isCheckbox) tr.prepend("<td><input type='checkbox' data-id='" + item.key + "'></td>");

        if(isCheckbox){
            importedEntryShowView_toggleCheckboxesElements(tr);
        }

        tbody.append(tr);
    }

    table.append(thead).append(tbody);
    container.append(table);

    return container;
}


function importedEntryShowView_confluenceFilter(data){
    var container = $("<div class='container-fluid' id='importedEntryShowView_filter'>");


    var table = $("<table class='table table-hover table-condensed'>");
    var tbody = $("<tbody>");

    /*var connections = data.connections;
    var connectionLine = $("<tr id='importedEntryShowView_connections'><td></td></tr>");
    var pagesCount = 0;
    for(var i = 0; i < connections.length; i++){
        var item = connections[i];
        pagesCount += parseInt(item.count);
        connectionLine.find("td").append("<label class='checkbox pull-left'><input type='checkbox' data-id='" + item.name + "' data-type='connection' checked='checked'>" + item.name + " (" + item.count + ")</label>")
    }
    connectionLine.prepend($("<th>").append(
        $("<label class='checkbox pull-left' style='font-weight:bold;'></label>")
            .append(
                $("<input type='checkbox' checked='checked'>").change(function(){
                    importedEntryShowView_toggleCheckboxes(this);
                })
            )
            .append(" Connections (" + pagesCount + "): ")
    ));
    tbody.append(connectionLine);
    */

    var projectLine = $("<tr id='importedEntryShowView_projects'><td></td></tr>");
    var pagesCount = 0;
    for(var i = 0; i < data.length; i++){
        var item = data[i];
        pagesCount += parseInt(item.count);
        projectLine.find("td").append("<label class='checkbox pull-left'><input type='checkbox' data-id='" + item.key + "' data-type='project' checked='checked'>" + item.name + " (" + item.count + ")</label>")
    }
    projectLine.prepend($("<th>").append(
        $("<label class='checkbox pull-left' style='font-weight:bold;'></label>")
            .append(
                $("<input type='checkbox' checked='checked'>").change(function(){
                    importedEntryShowView_toggleCheckboxes(this);
                })
            )
            .append(" Projects (" + pagesCount + "): ")
    ));
    tbody.append(projectLine);

    tbody.append(
      $("<tr id='importedEntryShowView_pageName'><th><label class='checkbox pull-left' style='font-weight:bold;'><input type='checkbox' checked='checked'>Page name</label></th><td><input type='text' style='width:97%;' placeholder='Enter substring of page name...'></td></tr>")
    );

    table.append(tbody);

    container.append(table);

    return container;
}

function importedEntryShowView_confluenceResults(data, isCheckbox){
    var container = $("<div class='container-fluid' id='importedEntryShowView_results'>");

    var table = $("<table class='table table-hover table-condensed'>");

    var thead = $("<thead><tr><th></th><th>Page Id</th><th>Title</th><th>Project</th><th>Comments</th><th>Attachments</th><th>Child Pages</th></tr></thead>");
    if(typeof isCheckbox !== "undefined" && isCheckbox) thead.find("tr").prepend("<th>");
    table.append(thead);

    var tbody = $("<tbody>");
    $(data).each(function(i, item){
        var tr = $("<tr>");
        if(typeof isCheckbox !== "undefined" && isCheckbox) tr.append("<td><input type='checkbox' data-id='" + item.pageId + "'></td>");
        tr.append("<td>" + (i + 1) + "</td>");
        tr.append("<td>" + item.pageId + "</td>") ;
        tr.append("<td><a href='" + item.url + "'>" + item.title + "</a></td>");
        tr.append("<td>" + item.projectName + "</td>");
        tr.append("<td>" + item.comments + "</td>");
        tr.append("<td>" + item.attachments + "</td>");
        tr.append("<td>" + item.childPages + "</td>");


        if(isCheckbox){
            importedEntryShowView_toggleCheckboxesElements(tr);
        }

        tbody.append(tr);
    });
    table.append(tbody);

    container.append(table);

    return container;
}

function importedEntryShowView_spiraTestfilter(data){

}

function importedEntryShowView_toggleCheckboxes(ele){
    $(ele).parent().parent().next().find("input[type='checkbox']").each(function(i, data){data.checked = $(ele).is(":checked");});
}

function importedEntryShowView_toggleCheckboxesElements(ele){
    var checked = false;
    ele.click(function(){
        checked = ele.find("td:first input").is(":checked");
        ele.find("td:first input")[0].checked = !checked;
    });
    ele.find("td:first input").click(function(){
        ele.find("td:first input")[0].checked = !checked;
    });
}