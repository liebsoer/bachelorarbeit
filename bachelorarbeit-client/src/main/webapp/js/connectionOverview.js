/**
 * Created with IntelliJ IDEA 12.
 * User: liebsoer
 * Date: 11.06.13
 * Time: 10:17
 * To change this template use File | Settings | File Templates.
 */

function connection_overview(data) {
    var table = $("<table class='table table-condensed table-hover'>");
    var thead = $("<thead>").append($("<tr>")
        //.append($("<th>").text("Icon"))
        .append($("<th>").text("Name"))
        .append($("<th>").text("User"))
        .append($("<th>").text("Type"))
        .append($("<th>").text("URL"))
        .append($("<th>").css("width","190px").text("Actions"))
    );

    var tbody = $("<tbody>");

    for(var i = 0; i < data.length; i++){
        tbody.append(connection_addConnectionLine(data[i]));
    }

    tbody.append($("<tr><td colspan='4'></tr>")
            .append($("<td>")
                .append($("<button class='btn btn-mini btn-primary' type='submit' style='float:right;margin-right:5px;'>Add Connection</button>")
                    .click(function(){
                        $("#site-content").empty().append(connection_connectionView(true)).ready(function(){
                            $(".wikiPageTree").treeview();
                        });
                    })
                )
                .append($("<button class='btn btn-mini'>Upload File</button>")
                    .click(function(){

                        bootbox.dialog(upload_form(REST_URIS.test.postUpload, "xml"),[{
                            "label" : "Close",
                            "class": "btn"
                        }]
                    );
                    })
                )
            )
    );

    table.append(thead);
    table.append(tbody);

    return table;
}

function connection_addConnectionLine(data) {
    return $("<tr>")
        //.append($("<td><img src='" + data.iconImageUrl + "' alt='" + data.iconText + "' width='24' height='24'></td>"))
        .append($("<td>" + data.name + "</td>"))
        .append($("<td>" + data.username + "</td>"))
        .append($("<td><img src='" + data.typeUrl + "' alt='" + data.type + "' width='24' height='24'></td>"))
        .append($("<td>" + data.url + "</td>"))
        .append($("<td>")
            .append($("<button type='submit' data-item='" + data.id + "' class='btn btn-mini btn-primary'><i class='icon-eye-open'></i>&nbsp;&nbsp;View&nbsp;&nbsp;</button>")
                .click(function(){
                    $("#site-content").empty().append(connection_connectionView(false, data));
                })
            )
            .append($("<button type='submit' data-item='" + data.id + "' class='btn btn-mini'><i class='icon-download'></i>&nbsp;Import</button>")
                .click(function(){
                    var content = $("#site-content").empty();
                    var projectId = data.id;
                    switch(data.type.toLowerCase()){
                        case "jira":
                            $.getJSON(REST_URIS.import.getJIRAProjects, {"id": data.id}, function(data){
                                content.append(JIRAImportView(projectId, data));
                            });
                            break;
                        case "confluence":
                            //$.getJSON(REST_URIS.import.getConfluenceProjects + "/" + projectId, function(resp){
                                content.append(confluenceImportView(projectId, data));
                            //});
                            break;
                    }
                })
            )
            .append($("<button type='submit' data-item='" + data.id + "' class='btn btn-mini'><i class='icon-remove-circle'></i>&nbsp;Delete</button>")
                .click(function(){
                    bootbox.confirm("<div style='text-align:center;'>Do you really want to delete <span style='font-weight:bold;'>\"" + data.name + "\"</span>?</div>", function(result){
                        if(result){
                            $.ajax({
                                url: REST_URIS.connection.deleteConnection + "/" + data.id,
                                data: {},
                                dataType: "json",
                                contentType: "application/json; charset=UTF-8",
                                type: "DELETE",
                                accept: "json"
                            })
                            .fail(function(){
                                connection_returnToOverview();
                                alert("Deletion wasn't successfully!")
                            })
                            .success(function(){
                                connection_returnToOverview();
                                alert("Connection was successfully deleted!");
                            })
                            ;
                        }
                    });
                })
            )
        )
    ;
}

function connection_connectionView(isNew, data){
    var table = $("<table class='table table-hover table-condensed'>");

    if(!data){
        isNew = true;
    }
    var name = (isNew ? "<input type='text' placeholder='Enter connection name...' id='connection_newConnection_name'>" : data.name);
    var description = (isNew ? "<input type='text' placeholder='Enter description...' id='connection_newConnection_description'>" : data.description);
    var type = (isNew ? "<select id='connection_newConnection_type'><option value='jira'>JIRA</option><option value='confluence'>Confluence</option></select>" : data.type);
    var uri = (isNew ? "<input type='text' placeholder='Enter connection uri...' id='connection_newConnection_url'>" : data.url);
    var username = (isNew ? "<input type='text' placeholder='Enter user name...' id='connection_newConnection_username'>" : data.username);
    var password = (isNew ? "<input type='password' placeholder='Enter password...' id='connection_newConnection_password'>" : "******");

    var tbody = $("<tbody>")
        .append($("<tr" + (!isNew? " data-type='name' data-value='" + data.name + "'": "") + "><th style='width:200px;'>Name</th><td>" + name + "</td></tr>"))
        .append($("<tr" + (!isNew? " data-type='description' data-value='" + data.description + "'": "") + "><th>Description</th><td>" + description + "</tr>"))
        .append($("<tr" + (!isNew? " data-type='type' data-value='" + data.typeName + "'": "") + "><th>Server Type</th><td>" + type + "</td>"))
        .append($("<tr" + (!isNew? " data-type='url' data-value='" + data.url + "'": "") + "><th>URI</th><td>" + uri + "</td></tr>"))
        .append($("<tr" + (!isNew? " data-type='username' data-value='" + data.username + "'": "") + "><th>User name</th><td>" + username + "</td></tr>"))
        .append($("<tr" + (!isNew? " data-type='password' data-value='" + data.password + "'": "") + "><th>Password</th><td>" + password + "</td></tr>"))

    if(!isNew){
        var id = data.id
        tbody.find("tr").each(function(i, item){

            $(item).append(
                $("<td style='width: 200px;'>")
                    .append($("<button class='btn btn-mini'>Edit</button>").click(function(){
                        $(this).parent().prev().empty()
                        var content = $(this).parent().parent();
                        var text = content.attr("data-value");
                        switch(content.attr("data-type")){
                            case "type":
                                var select = $("<select id='connection_newConnection_type'><option value='jira'" + (text == "jira" ? " selected" : "") + ">JIRA</option>" +
                                    "<option value='confluence'" + (text == "confluence" ? " selected" : "") + ">Confluence</option></select>");
                                content.find("td:first").append(select);
                                break;
                            case "password":
                                content.find("td:first").append($("<input type='password'>").val(text));
                                break;
                            default:
                                content.find("td:first").append($("<input type='text'>").val(text));
                                break;
                        }

                        $(this).parent().find("button").show();
                        $(this).hide();

                    }))
                    .append($("<button class='btn btn-mini btn-primary' style='display:none;'>Save</button>").click(function(){
                        var content = $(this).parent().parent();

                        var text = "";
                        switch(content.attr("data-type")){
                            case "type":
                                text = content.find("td:first select").val();
                                break;
                            default:
                                text = content.find("td:first input").val();
                                break;
                        }

                        var updateData = {};
                        updateData[content.attr("data-type")] = text;

                        $.ajax({
                            url:REST_URIS.connection.putConnection + "/" + id + "/update",
                            type:"PUT",
                            data: JSON.stringify(updateData),
                            contentType:"application/json; charset=utf-8",
                            dataType:"json",
                            success: function(){console.log("Connection successfully updated");}
                        });

                        content.find("td:first").empty();
                        switch(content.attr("data-type")){
                            case "password":
                                content.find("td:first").append("********");
                                break;
                            default:
                                content.attr("data-value", text);
                                content.find("td:first").append(text);
                                break;
                        }

                        $(this).parent().find("button").hide();
                        $(this).parent().find("button:first").show();
                    }))
                    .append($("<button class='btn btn-mini' style='display:none;'>Cancel</button>").click(function(){
                        $(this).parent().prev().empty();
                        var content = $(this).parent().parent();
                        var text = content.attr("data-value");

                        switch(content.attr("data-type")){
                            case "password":
                                content.find("td:first").append("********");
                                break;
                            default:
                                content.find("td:first").append(text);
                                break;
                        }

                        $(this).parent().find("button").hide();
                        $(this).parent().find("button:first").show();
                    }))
            );
        });
    }

    table.append(tbody);

    var div = $("<div class='container-fluid'>");
    div.append(table);

//    div.append($("<div class='alert alert-success'>Connection was tested successfully</div>"));

//    div.append($("<div class='alert alert-error'>Connection couldn't be established.</div>"));

//    div.append($("<div class='container-fluid float-left pull-left'><button type='submit' class='btn btn-primary'>Test Connection</button></div>"));

    if(isNew){
        div.append($("<div class='container-fluid float-left pull-left'></div>")
            .append($("<button type='submit' class='btn'>Save</button>")
            .click(function(){

                    var data = {
                        "name": $("#connection_newConnection_name").val(),
                        "description": $("#connection_newConnection_description").val(),
                        "type": $("#connection_newConnection_type").val(),
                        "url": $("#connection_newConnection_url").val(),
                        "username": $("#connection_newConnection_username").val(),
                        "password": $("#connection_newConnection_password").val()
                    };
                    $.ajax({
                        url:REST_URIS.connection.postConnection,
                        type:"POST",
                        data:JSON.stringify(data),
                        contentType:"application/json; charset=utf-8",
                        dataType:"json",
                        success: function(){
                            bootbox.dialog("Connection successfully added",[{
                                "label" : "Close",
                                "class" : "btn-success",
                                "callback": function(){
                                    connection_returnToOverview();
                                }
                                }]
                            );
                        }
                    });
            }))
        );
    }
    div.append($("<div class='container-fluid pull-left'></div>")
        .append($("<button type='submit' class='btn'>" + (isNew ? "Cancel" : "Close") + "</button>")
            .click(function(){
                connection_returnToOverview();
            }))
    );

    return div;


}

function connection_returnToOverview(){
    var content = $("#site-content").empty();

    $.get(REST_URIS.connection.getOverview, function(data){
        content.append(connection_overview(data));
    });
}