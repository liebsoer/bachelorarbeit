/**
 * Created with IntelliJ IDEA 12.
 * User: liebsoer
 * Date: 14.06.13
 * Time: 21:38
 * To change this template use File | Settings | File Templates.
 */

var namespace_jiraImportView = "jiraImportView";

function JIRAImportView(id, data){

    var prjId = id;


    var container = $("<div id='jiraImportView'>");

    /*
     * Project information
     */

    var prjInfos = $("<div class='container-fluid'>");

    prjInfos.append($("<div class='pull-left " + namespace_jiraImportView + "-project-img'>")
    );
    prjInfos.append($("<div>")
        .append($("<div class='pull-left " + namespace_jiraImportView + "-metaInfo' id='" + namespace_jiraImportView + "-project-id'><span>Id: </span>"))
        .append($("<div class='pull-left " + namespace_jiraImportView + "-metaInfo' id='" + namespace_jiraImportView + "-project-name'><span>Servername: </span>"))
        .append($("<div class='pull-left " + namespace_jiraImportView + "-metaInfo' id='" + namespace_jiraImportView + "-project-key'><span>Key: </span>"))

        .append($("<div id='" + namespace_jiraImportView + "-project-amountIssues' class='" + namespace_jiraImportView + "-metaInfo'><span>Count of entire issues: </span>"))
    );
    prjInfos.append($("<div id='" + namespace_jiraImportView + "-project-url' class='" + namespace_jiraImportView + "-metaInfo'>")
        .append($("<span class='pull-left' style='min-width: 90px;'>URL: </span>"))
    );
    prjInfos.append($("<div id='" + namespace_jiraImportView + "-project-description' class='" + namespace_jiraImportView + "-metaInfo'>")
        .append($("<span class='pull-left' style='min-width: 90px;'>Description: </span>"))
    );

    container.append(prjInfos);

    var projects = $("<div id='" + namespace_jiraImportView + "-projects' class='container-fluid " + namespace_jiraImportView + "-metaInfo'>");
    projects.append("<span class='pull-left' style='min-width: 85px;'>Projects</span>");
    for(var i = 0; i < data.length; i++){
        var item = data[i];
        var prjName = item.name;
        var prjKey = item.key;

        projects.append($("<label class='checkbox pull-left " + namespace_jiraImportView + "-project'> " + prjName  + "</label>")
            .prepend($("<input type='checkbox' data-key='" + prjKey + "' style='margin-top:2px;'>")
                .change(function(){
                    if($(this).is(":checked")){
                        $(this).parent().css("background-color", "lightgrey");
                    }else{
                        $(this).parent().css("background", "none");

                    }

                    var data = [];
                    $("#" + namespace_jiraImportView + "-projects :checked").each(function(i, item){
                        data.push($(item).attr("data-key"));
                    });

                    $("." + namespace_jiraImportView + "-filterValues").remove();
                    $("#" + namespace_jiraImportView + "-SaveButton").remove();
                    $("#" + namespace_jiraImportView + "-CancelButton").remove();

                    if(data.length){
                        $.getJSON(REST_URIS.import.getJIRAFilterValues + "/" + id, {"data": data} ,function(data){

                            container.append(jiraImportView_addTable("issuetype", "Available Issue Types", data.issuetype));
                            container.append(jiraImportView_addTable("status", "Available Status Types", data.status));
                            container.append(jiraImportView_addTable("priority", "Available Priorities", data.priority));
                            container.append(jiraImportView_addTable("component", "Available Components", data.component));

                            var saveButton = $("<div id='" + namespace_jiraImportView + "-SaveButton'>").css({"float":"left","width":"18.5em"});
                            saveButton.append($("<button>Import</button>")
                                .attr({"type":"submit","class":"btn btn-primary", "style":"float:right;margin-right:5%;"})
                                .click(function(){
                                    var test = [];
                                    var obj = {};

                                    var issuetype = "issuetype=";
                                    var status = "status=";
                                    var priority = "priority=";
                                    var component = "component=";
                                    var project = "project=";

                                    var data = "{";

                                    var items = $("#jiraImportView").find("div[data-checked='true']");
                                    for(var i = 0; i < items.length; i++){
                                        var item = $(items[i]);
                                        test.push(item);

                                        var type = item.attr("data-type");
                                        var id = item.attr("data-id");

                                        if(!obj[type]){
                                            obj[type] = [];
                                        }
                                        obj[type].push(id);


                                        if(type == "issuetype"){
                                            issuetype += id + ",";
                                        }
                                        if(type == "status"){
                                            status += id + ",";
                                        }
                                        if(type == "priority"){
                                            priority += id + ",";
                                        }
                                        if(type == "component"){
                                            component += id + ",";
                                        }

                                    }

                                    $("#jiraImportView-projects :checked").each(function(i, item){
                                        if(!obj["project"])obj["project"] = [];

                                        obj["project"].push($(item).attr("data-key"));
                                        project += $(item).attr("data-key") + ",";
                                    });

                                    var url = REST_URIS.import.postJIRAImport + "/" + prjId + "?" + issuetype + "&" + status + "&" + priority + "&" + component + "&" + project;
                                    $.ajax({
                                        url: url,
                                        type:"POST",
                                        //data: obj,
                                        contentType:"application/json; charset=utf-8",
                                        dataType:"json",
                                        success: function(){
                                            bootbox.dialog("JIRA issues successfully imported",[{
                                                "label" : "Close",
                                                "class" : "btn-success",
                                                "callback": function(){
                                                    connection_returnToOverview();
                                                }
                                            }]
                                            );
                                        }
                                    });

                                    //alert("From \"jira\" imported:\n" + JSON.stringify(obj));
                                    connection_returnToOverview();
                                })
                                .prepend("<i class='icon-ok icon-white'></i> "));
                            container.append(saveButton);

                            var cancelButton = $("<div id='" + namespace_jiraImportView + "-CancelButton'>").css({"float":"left","width":"20%"});
                            cancelButton.append($("<button type='submit' class='btn' style='float:right;margin-right:5%;'><i class='icon-remove'></i>Cancel</button>").click(function(){
                                connection_returnToOverview();
                            }));
                            container.append(cancelButton);
                        })
                        ;
                    }
                })
            )
        )
    }
    container.append(projects);

    $.getJSON(REST_URIS.connection.getConnectionMeta.replace("{id}", id), {}, function(data){
        $("#" + namespace_jiraImportView + "-project-id").append("<span>" + data.id + "</span>");
        $("#" + namespace_jiraImportView + "-project-name").append("<span>" + data.name + "</span>");
        $("#" + namespace_jiraImportView + "-project-key").append("<span>" + data.key + "</span>");
        $("#" + namespace_jiraImportView + "-project-url").append("<span>" + data.url + "</span>");
        $("#" + namespace_jiraImportView + "-project-description").append("<span>" + data.description + "</span>");
        $("#" + namespace_jiraImportView + "-project-amountIssues").append("<span>" + data.totalCount + "</span>");
    });

    return container;
}

function jiraImportView_addTable(type, typeName, data){
    var div = $("<div class='container-fluid " + namespace_jiraImportView + "-filterValues'>")
        .append("<div class='" + namespace_jiraImportView + "-title'>" + typeName + "</div>");


    var table = $("<table class='table table-condensed table-hover " + namespace_jiraImportView + "-table'>");

    var thead = $("<thead>");
    var trHead = $("<tr>");

    trHead.append("<th class='" + namespace_jiraImportView + "-checkbox'></th>");
    if(typeof data[0].iconUrl !== "undefined"){
        trHead.append("<th class='" + namespace_jiraImportView + "-icon'>Icon</th>");
    }
    trHead.append("<th class='" + namespace_jiraImportView + "-id'>Id</th>");
    trHead.append("<th class='" + namespace_jiraImportView + "-name'>Name</th>");
    trHead.append("<th class='" + namespace_jiraImportView + "-count'>Count</th>");
    trHead.append("<th class='" + namespace_jiraImportView + "-description'>Description</th>");

    thead.append(trHead);
    table.append(thead);

    var tbody = $("<tbody>")

    for(var i in data){
        var item = data[i];
        var tr = $("<tr></tr>").click(function(){
            var checkbox = $(this).find("div[data-checked]");

            checkbox.attr("data-checked", checkbox.attr('data-checked') == "true" ? false : true);
            if(checkbox.attr('data-checked') == "true"){
                $(this).find(".jiraImportView-checkbox").addClass("icon-ok");
            }else{
                $(this).find(".jiraImportView-checkbox").removeClass("icon-ok");
            }
        });

        //tr.append($("<td>").append($("<input type='checkbox' data-type='" + type + "' data-id='" + item.id + "'>")));
        tr.append($("<td>").append($("<div style='width:14px;height:13px;border:thin solid black;margin-top:5px;padding:1px;' data-checked='false' data-type='" + type + "' data-id='" + item.id + "'>").append($("<i class='jiraImportView-checkbox' style='margin-top:-3px;'></i>"))));
        if(typeof item.iconUrl !== "undefined"){
            tr.append($("<td>").append($("<img src='" + item.iconUrl + "' alt='" + item.name + "' width='16' height='16'>")));
        }
        tr.append($("<td>").text(item.id));
        tr.append($("<td>").text(item.name));
        tr.append($("<td>").text(item.count));
        tr.append($("<td>").text(item.description));


        tbody.append(tr);
    }

    table.append(tbody);

    div.append(table);

    return div;
}