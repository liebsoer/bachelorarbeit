/**
 * Created with IntelliJ IDEA 12.
 * User: liebsoer
 * Date: 29.06.13
 * Time: 16:25
 * To change this template use File | Settings | File Templates.
 * TODO: Auswahl der Items implementieren, damit diese hinzugefügt werden können.
 */

function components(data){

    var container = $("<div class='container-fluid'>");

    var table = $("<table class='table table-hover table-condensed' id='components_filter'>");

    var tbody = $("<tbody>");

    var filter = {types:[]};

    var types = $("<tr id='components_filterType'><th><label class='checkbox' style='font-weight:bold'><input type='checkbox' checked> Types:</label></th></tr>");
    var typesTd = $("<td>");
    for(var i = 0; i < data.length; i++){
        typesTd.append("<label class='checkbox pull-left'><input type='checkbox' data-value='" + data[i].name +"' checked>" + data[i].name + " (" + data[i].count + ")</label>");
        filter.types.push(data[i].name);
    }
    tbody.append(types.append(typesTd));

    tbody.append($("<tr><th><label class='checkbox' style='font-weight:bold'><input type='checkbox'> Name:</label></th><td><input type='text' style='width:98%' placeholder='Enter sitename here...' id='components_filterName' /></td></tr>"));

    table.append(tbody);

    tbody.find("input[type='checkbox']").change(function(){
        components_toggleFilter(this);
    });
    container.append(table);

    container.append($("<div class='container-fluid'>").append(
        $("<button class='btn btn-primary'>Update Results</button>").click(function(){

            var filter = {
                types: []
            };

            if($("#components_filterName").parent().prev().find("input[type='checkbox']").is(":checked") && $("#components_filterName").val().length){
                filter["name"] = $("#components_filterName").val();
            }

            $("#components_filterType td input[type='checkbox']:checked").each(function(i, item){
                filter.types.push($(item).attr("data-value"));
            });

            if(filter.types.length < 1 && typeof filter.name == "undefined"){
                bootbox.dialog("Please enter at least one type or name!", [components_viewItemsAddButtonChoice("close", "btn-warning")]);
                return;
            }

            $.getJSON(REST_URIS.manage.getComponents, filter, function(data){
                $("#components_results").empty().append(components_showResult(data, filter));
            });

        })
    ));

    container.append("<div class='container-fluid' id='components_results'></div>");



    $.getJSON(REST_URIS.manage.getComponents, filter, function(data){
        var result = components_showResult(data, filter);
        $("#components_results").empty().append(result);
    });

    return container;
}

function components_showResult(data, filter){
    var table = $("<table class='table table-hover table-condensed'></table>");

    var thead = $("<thead><th>Id</th><th>Name</th><th>Description</th><th>Type</th><th>Specs</th><th>TestCases</th><th>Bugs</th><th>Actions</th></thead>");
    table.append(thead);

    var tbody = $("<tbody></tbody>");
    for(var i = 0; i < data.length; i++){
        var item = data[i];

        if($.inArray(item.type, filter.types) == -1 || (typeof filter.name != "undefined" && item.name.indexOf(item.name) >= 0 )){
            continue;
        }

        tbody.append($("<tr>" +
            "<td>" + item.id + "</td>" +
            "<td>" + item.name + "</td>"+
            "<td>" + item.description + "</td>" +
            "<td>" + item.type + "</td>" +
            "<td>" + item.specCount + "</td>" +
            "<td>" + item.testCount + "</td>" +
            "<td>" + item.bugCount + "</td>" +
            "</tr>"
        )
            .append($("<td>")
                .append(
                    $("<button class='btn btn-mini btn-primary' data-id='" + item.id + "'><i class='icon-eye-open'></i> View</button>")
                        .click(function(){
                            var content = $("#site-content").empty();
                            components_setComponentIdField($(this).attr("data-id"));

                            $.getJSON(REST_URIS.manage.getComponent + "/" + components_getComponentId() , function(data){
                                content.append(components_viewComponent(data));
                            });
                        })
                )
                .append(
                    $("<button class='btn btn-mini' data-name='" + item.name + "' data-id='" + item.id + "'><i class='icon-remove-circle'></i> Delete</button>")
                        .click(function(){
                            var name = $(this).attr("data-name");
                            var id = $(this).attr("data-id");
                            bootbox.confirm("<div style='text-align:center;'>Do you really want to delete <span style='font-weight:bold;'>\"" + name + "\"</span>?</div>", function(result){
                                if(result){
                                    $.ajax({
                                        url: REST_URIS.manage.deleteComponent + "/" + id,
                                        data: data,
                                        dataType: "json",
                                        contentType: "application/json; charset=UTF-8",
                                        type: "DELETE",
                                        accept: "json"
                                    })
                                        .fail(function(){
                                            alert("Deletion wasn't successfully!")
                                        })
                                        .success(function(){
                                            alert("Connection was successfully deleted!");
                                        })
                                    ;
                                    components_returnToOverview();
                                }
                            })
                        })
                )
            )
        )
        ;
    }

    table.append(tbody);

    return table;
}


function components_createComponent(){

    var container = $("<div class='container-fluid'>");
    var table = $("<table class='table table-hover table-condensed'>");

    var tbody = $("<tbody>");

    tbody.append("<tr><th style='width:100px;'>Name: </th><td><input type='text' placeholder='Enter name here...' style='width:98%;' id='component_addComponent_name'/></td></tr>");
    tbody.append("<tr><th>Description: </th><td><textarea placeholder='Enter description here...' style='resize:horizontal;width:98%;' id='component_addComponent_description'></textarea></td></tr>");
    tbody.append("<tr><th>Component Type: </th><td><input type='text' placeholder='Enter component type here...' style='width:98%;' id='component_addComponent_type'/></td></tr>");

    table.append(tbody);
    container.append(table);

    container.append(
        $("<button class='btn btn-primary'>Save</button>").click(function(){

            if($("#component_addComponent_name").val().length < 1){
                bootbox.dialog("Please enter a name!", [{
                    "label" : "Close",
                    "class" : "btn-warning"
                }]);
                return;
            }
            if($("#component_addComponent_description").val().length < 1){
                bootbox.dialog("Please enter a description!", [{
                    "label" : "Close",
                    "class" : "btn-warning"
                }]);

                return;
            }
            if($("#component_addComponent_type").val().length < 1){
                bootbox.dialog("Please enter a component type!", [{
                    "label" : "Close",
                    "class" : "btn-warning"
                }]);
                return;
            }

            var data = {
                "name": $("#component_addComponent_name").val(),
                "description": $("#component_addComponent_description").val(),
                "type": $("#component_addComponent_type").val()
            };
            $.post(REST_URIS.manage.postComponent, data)
                .done(function(){
                    bootbox.dialog("Component successfully saved.", [{
                        "label" : "Close",
                        "class" : "btn-success",
                        "icon": "icon-ok"
                    }]);
                })
                .fail(function(){
                    bootbox.dialog("Saving component failed.", [{
                        "label" : "Close",
                        "class" : "btn-danger",
                        "icon": "icon-remove"
                    }]);
                })
            ;

            components_returnToOverview();
        })
    ).append(
        $("<button class='btn'>Cancel</button>").click(function(){
            $(".context-action").removeClass("active");
            $(".context-action:first").addClass("active");

            components_returnToOverview();
        })
    );

    return container;
}

function components_viewComponent(data){

    var container = $("<div class='container-fluid'>");

    var tbMeta = $("<table class='table table-hover table-condensed'></table>");

    var tbodyMeta = $("<tbody>");
    tbodyMeta.append($("<tr><th>ID:</th><td>" + data.id + "</td><th>Name:</th><td>" + data.name + "</td><th>Component Type:</th><td>" + data.type + "</td></tr>"));
    tbodyMeta.append($("<tr><th>Description:</th><td colspan='6'>" + data.description + "</td></tr>"));
    tbMeta.append(tbodyMeta);

    var tbOverview = $("<table class='table table-hover table-condensed'>");
    var tBodyOverview = $("<tbody>");
    tBodyOverview.append("<tr><th>Specification Elements:</th><td>" + data.specificationElements + "</td><th>TestCases:</th><td>" + data.testCaseElements + "</td><th>Bugs:</th><td>" + data.bugElements + "</td></tr>");
    tBodyOverview.append("<tr><th>Average Dev Time</th><td>" + data.averageDevTime + "</td><th>Average Test Time</th><td>" + data. averageTestTime + "</td><th>Average Bug Fix Time</th><td>" + data.averageBugfixTime + "</td></tr>");
    tBodyOverview.append("<tr><th>Average Total Time</th><td>" + data.averageTotalTime + "</td></tr>");
    tBodyOverview.append("<tr><th>Average Dev Costs</th><td>" + data.averageDevCosts + "</td><th>Average TestCase Costs</th><td>" + data.averageTestCosts + "</td><th>Average Bug Costs</th><td>" + data.averageBugfixCosts + "</td></tr>");
    tBodyOverview.append("<tr><th>Average Total Costs</th><td>" + data.averageTotalCosts + "</td></tr>");
    tBodyOverview.append("<tr><th>Average Component Value</th><td>" + data.averageAmount + "</td><th>Average Benifit</th><td>" + data.averageBenifit + "</td></tr>");
    tBodyOverview.append("<tr><th>Average Risk</th><td>" + data.averageRisk + "</td><th>Average Complexity (1-10)</th><td>" + data.averageComplexity + "</td></tr>");
    tbOverview.append(tBodyOverview);

//    var tbProjects =  $("<table class='table table-hover table-condensed'>");
//    var tHeadProjects = $("<thead><tr>" +
//            "<th style='width:20px;'>Id</th>" +
//            "<th>Project name</th>" +
//            "<th><div>Dev Time</div><div>Dev Costs</div></th>" +
//            "<th><div>Test Time</div><div>Test Costs</div></th>" +
//            "<th><div>Bugfix Time</div><div>Bugfix Costs</div></th>" +
//            "<th>Amount</th>" +
//            "<th>Benifit</th>" +
//            "<th>Actions</th>" +
//        "</tr></thead>");
//    var tBodyProjects = $("<tbody>");
//    for(var i = 0; i < data.projects.length; i++){
//        var project = data.projects[i];
//        tBodyProjects.append(
//            $("<tr>" +
//                "<td>" + project.id + "</td>" +
//                "<td>" + project.name + "</td>" +
//                "<td><div>" + project.devTime + "</div><div>" + project.devCosts + "</div></td>" +
//                "<td><div>" + project.testTime + "</div><div>" + project.testCosts + "</div></td>" +
//                "<td><div>" + project.bugfixTime + "</div><div>" + project.bugfixCosts + "</div></td>" +
//                "<td><div>" + project.amount + "</td>" +
//                "<td><div>" + project.benifit + "</td>" +
//                "<td data-role='action' data-type='project' data-id='" + project.id + "'></td>" +
//            "</tr>")
//        );
//    }
//    tBodyProjects.append($("<tr><td colspan='7'></td></tr>").append($("<td></td>").append(
//        $("<button class='btn btn-small'><i class='icon-plus'></i>Add Project</button>")
//        .click(function(){
//            bootbox.dialog("Project added.", [{
//              "label": "Close",
//              "class": "btn-info"
//            }])
//        })
//    ))
//    );
//    tbProjects.append(tHeadProjects)
//        .append(tBodyProjects)
//    ;


    var tbSpecs =  $("<table class='table table-hover table-condensed'>");
    var tHeadSpecs = $("<thead><tr>" +
        "<th style='width:20px;'>Id</th>" + "<th>Name</th>" + "<th>Value</th>" + "<th>Dev Time</th>" +
        "<th>Priority</th>" + "<th>Project</th>" + "<th>System</th>" + "<th>Actions</th>" +
        "</tr></thead>");
    var tBodySpecs = $("<tbody></tbody>");
    for(var i = 0; i < data.confluenceSpecs.length; i++){
        var spec = data.confluenceSpecs[i];
        tBodySpecs.append("<tr>" +
            "<td>" + spec.id + "</td>" + "<td>" + spec.name + "</td>" +
            "<td>" + spec.costs + "</td>" + "<td>" + spec.devTime + "</td>" +
            "<td>" + spec.priority + "</td>" + "<td>" + spec.project + "</td>" +
            "<td>" + spec.system + "</td>" + "<td data-role='action' data-type='spec' data-id='" + spec.id + "'></td>" +
            "</tr>");
    }
    tBodySpecs.append($("<tr><td colspan='7'></td></tr>").append($("<td></td>").append(
        $("<button class='btn btn-small'><i class='icon-plus'></i>Add Specification</button>")
        .click(function(){
                components_addItemsView(["jira", "confluence"]);
        })
    ))
    );
    tbSpecs.append(tHeadSpecs).append(tBodySpecs);

    var tbTestCases =  $("<table class='table table-hover table-condensed'>");
    var tHeadTestCases = $("<thead><tr>" +
        "<th style='width:20px;'>Id</th>" + "<th>Name</th>" + "<th>Costs</th>" + "<th>Test Time</th>" +
        "<th>Priority</th>" + "<th>Project</th>" + "<th>System</th>" + "<th>Actions</th>" +
        "</tr></thead>");
    var tBodyTestCases = $("<tbody></tbody>");
    for(var i = 0; i < data.spiraTestCases.length; i++){
        var tc = data.spiraTestCases[i];
        tBodyTestCases.append("<tr>" +
            "<td>" + tc.id + "</td>" + "<td>" + tc.name + "</td>" + "<td>" + tc.costs + "</td>" + "<td>" + tc.devTime + "</td>" +
            "<td>" + tc.priority + "</td>" + "<td>" + tc.project + "</td>" + "<td>" + tc.system + "</td>" + "<td data-role='action' data-type='testCase' data-id='" + tc.id + "'></td>" +
            "</tr>")
    }
    tBodyTestCases.append($("<tr><td colspan='7'></td></tr>").append($("<td></td>").append(
        $("<button class='btn btn-small'><i class='icon-plus'></i>Add TestCase</button>")
        .click(function(){
              components_addItemsView(["spiraTest"], "test");
//            var dialog = bootbox.dialog("Please select type to import:",[
//                {
//                   "label": "JIRA",
//                    "class": "btn",
//                    "options":{
//                        "data": "jira"
//                    }
//                }
//            ]);
//
//            dialog.find("a.btn").each(function(i, item){
//                $(item).click(function(){
//                    var content = $("#site-content");
//                    switch($(this).attr("data-store")){
//                        case "jira":
//                            content.empty(components_addItemsView("jira"));
//                            break;
//                    }
//                })
//            });
        })
    ))
    );
    tbTestCases.append(tHeadTestCases).append(tBodyTestCases);


    var tbBugs =  $("<table class='table table-hover table-condensed'>");
    var tHeadBugs = $("<thead><tr>" +
        "<th style='width:20px;'>Id</th>" + "<th>Name</th>" + "<th>Costs</th>" + "<th>Fix Time</th>" +
        "<th>Priority</th>" + "<th>Project</th>" + "<th>System</th>" + "<th>Actions</th>" +
        "</tr></thead>");
    var tBodyBugs = $("<tbody></tbody>");
    for(var i = 0; i < data.jiraBugIssues.length; i++){
        var bug = data.jiraBugIssues[i];
        tBodyBugs.append("<tr>" +
            "<td>" + bug.id + "</td>" + "<td>" + bug.name + "</td>" + "<td>" + bug.costs + "</td>" + "<td>" + bug.devTime + "</td>" +
            "<td>" + bug.priority + "</td>" + "<td>" + bug.project + "</td>" + "<td>" + bug.system + "</td>" + "<td data-role='action' data-type='bug' data-id='" + bug.id + "'></td>" +
            "</tr>")
    }
    tBodyBugs.append($("<tr><td colspan='7'></td></tr>").append($("<td></td>").append(
        $("<button class='btn btn-small'><i class='icon-plus'></i>Add Bug</button>")
        .click(function(){
                components_addItemsView(["jira"], "bug");
//            var dialog = bootbox.dialog("Please select type to import:",[
//                {
//                    "label": "JIRA",
//                    "class": "btn",
//                    "options": {
//                        "data": "jira"
//                    }
//                }
//            ]);
//
//            dialog.find("a.btn").each(function(i, item){
//               $(item).click(function(){
//                   var content = $("#site-content");
//                   switch($(this).attr("data-store")){
//                       case "jira":
//                           content.empty(components_addItemsView(["jira"], "bug"));
//                           break;
//                   }
//               })
//            });
        })
    ))
    );
    tbBugs.append(tHeadBugs).append(tBodyBugs);

    container.append(tbMeta).append(tbOverview).append(tbProjects).append(tbSpecs).append(tbTestCases).append(tbBugs);

    container.find("td[data-role='action']").each(function(i, item){
        $(item)
        .append($("<button class='btn btn-mini btn-primary'><i class='icon-eye-open'></i> View</button>").click(function(){
            var id = $(item).attr("data-id");
            var type = $(item).attr("data-type");

            var url = REST_URIS.manage.getComponent + "/" + data.id + "/" + type + "/" + id + ".json";

            var content = $("#site-content").empty();

            var role = $(item).attr("data-type");
            $.getJSON(url, function(data){
                var content = $("#site-content");
                switch(role){
                    case "project":
                        content.append(components_component_project(data, id));
                        break;
                    case "spec":
                        content.append(components_component_spec(data, id));
                        break;
                    case "testCase":
                        content.append(components_component_testCase(data, id));
                        break;
                    case "bug":
                        content.append(components_component_bug(data, id));
                        break;
                }
            });
        }))
        .append($("<button class='btn btn-mini'><i class='icon-remove'></i> Delete</button>").click(function(){
            var id = $(item).attr("data-id");
            var type = $(item).attr("data-type");
            var componentId = data.id;

            bootbox.confirm("<div style='text-align:center;'>Do you really want to delete this?</div>", function(result){
                if(result){
                    $.ajax({
                        url: REST_URIS.manage.deleteComponent + "_" + type + "/" + id,
                        data: data,
                        dataType: "json",
                        contentType: "application/json; charset=UTF-8",
                        type: "DELETE",
                        accept: "json"
                    })
                        .fail(function(){
                            alert("Deletion wasn't successfully!")
                        })
                        .done(function(){
                            alert("Connection was successfully deleted!");
                        })
                    ;
                    components_viewComponent(componentId);
                }
            })
        }))
    });

    return container;
}

function components_component_project(data, componentId){
    var container = $("<div class='container-fluid'>");

    var table = $("<table class='table table-hover table-condensed'>");
    var tbody = $("<tbody>");
    tbody.append($("<tr><th>Id</th><td>" + data.id + "</td></tr>"));
    tbody.append($("<tr><th>Name</th><td>" + data.name + "</td></tr>"));
    tbody.append($("<tr><th>Description</th><td>" + data.description + "</td></tr>"));
    tbody.append($("<tr><th>Specification Items</th><td>" + data.specItems + "</td></tr>"));
    tbody.append($("<tr><th>TestCase Items</th><td>" + data.testCaseItems + "</td></tr>"));
    tbody.append($("<tr><th>Bug Items</th><td>" + data.bugItems + "</td></tr>"));
    tbody.append($("<tr><th>Value</th><td>" + data.value + "</td></tr>"));
    tbody.append($("<tr><th>Total Costs</th><td>" + data.totalCosts + "</td></tr>"));
    tbody.append($("<tr><th>Benefit</th><td>" + data.benefit + "</td></tr>"));
    tbody.append($("<tr><th>Total Time</th><td>" + data.totalTime + "</td></tr>"));
    tbody.append($("<tr><th>Development Time</th><td>" + data.devTime + "</td></tr>"));
    tbody.append($("<tr><th>Test Time</th><td>" + data.testTime + "</td></tr>"));
    tbody.append($("<tr><th>Bugfix Time</th><td>" + data.bugfixTime + "</td></tr>"));
    tbody.append($("<tr><th>Development Costs</th><td>" + data.devCosts + "</td></tr>"));
    tbody.append($("<tr><th>Test Costs</th><td>" + data.testCosts + "</td></tr>"));
    tbody.append($("<tr><th>Bugfix Time</th><td>" + data.bugfixCosts + "</td></tr>"));

    /*
    var comments = $("<table class='table table-hover table-condensed'></table>")
    var tbodyComments = $("<tbody>");
    for(var i = 0; i < data.comments.length; i++){
        var comment = data.comments[i];
        tbodyComments.append($("<tr><th>#" + (i + 1) + "</th><td>" + comment + "</td></tr>"));
    }
    comments.append(tbodyComments);
    tbody.append($("<tr><th>Comments</th></tr>").append($("<td>").append(comments)));
    */

    table.append(tbody);
    container.append(table);

    container.append(components_closeButton(componentId));
    return container;
}

function components_component_spec(data, componentId){
    var container = $("<div class='container-fluid'>");

    var table = $("<table class='table table-hover table-condensed'>");
    var tbody = $("<tbody>");
    tbody.append($("<tr><th>Id</th><td>" + data.id + "</td></tr>"));
    tbody.append($("<tr><th>Name</th><td>" + data.name + "</td></tr>"));
    tbody.append($("<tr><th>Description</th><td>" + data.description + "</td></tr>"));
    tbody.append($("<tr><th>Project</th><td>" + data.project + "</td></tr>"));
    tbody.append($("<tr><th>Component</th><td>" + data.component + "</td></tr>"));
    tbody.append($("<tr><th>Priority</th><td>" + data.priority + "</td></tr>"));
    tbody.append($("<tr><th>Development Time</th><td>" + data.devTime + "</td></tr>"));
    tbody.append($("<tr><th>Costs</th><td>" + data.costs + "</td></tr>"));

    var comments = $("<table class='table table-hover table-condensed'></table>");
    var tbodyComments = $("<tbody>");
    for(var i = 0; i < data.comments.length; i++){
        var comment = data.comments[i];
        tbodyComments.append($("<tr><th>#" + (i + 1) + "</th><td>" + comment + "</td></tr>"));
    }
    comments.append(tbodyComments);
    tbody.append($("<tr><th>Comments</th></tr>").append($("<td>").append(comments)));

    table.append(tbody);
    container.append(table);

    container.append(components_closeButton(componentId));
    return container;
}

function components_component_testCase(data, componentId){
    var container = $("<div class='container-fluid'>");

    var table = $("<table class='table table-hover table-condensed'>");
    var tbody = $("<tbody>");
    tbody.append($("<tr><th>Id</th><td>" + data.id + "</td></tr>"));
    tbody.append($("<tr><th>Name</th><td>" + data.name + "</td></tr>"));
    tbody.append($("<tr><th>Description</th><td>" + data.description + "</td></tr>"));
    tbody.append($("<tr><th>Project</th><td>" + data.project + "</td></tr>"));
    tbody.append($("<tr><th>Component</th><td>" + data.component + "</td></tr>"));
    tbody.append($("<tr><th>Priority</th><td>" + data.priority + "</td></tr>"));
    tbody.append($("<tr><th>Test Time</th><td>" + data.testTime + "</td></tr>"));
    tbody.append($("<tr><th>Costs</th><td>" + data.costs + "</td></tr>"));

    var steps = $("<table class='table table-hover table-condensed'></table>");
    var tbodyTestStapes = $("<tbody>");
    for(var i = 0; i < data.testSteps.length; i++){
        var step = data.testSteps[i];
        tbodyTestStapes.append($("<tr><th>Step " + (i + 1) + "</th><td>" + step + "</td></tr>"));
    }
    steps.append(tbodyTestStapes);
    tbody.append($("<tr><th>Comments</th></tr>").append($("<td>").append(steps)));

    var comments = $("<table class='table table-hover table-condensed'></table>");
    var tbodyComments = $("<tbody>");
    for(var i = 0; i < data.comments.length; i++){
        var comment = data.comments[i];
        tbodyComments.append($("<tr><th>#" + (i + 1) + "</th><td>" + comment + "</td></tr>"));
    }
    comments.append(tbodyComments);
    tbody.append($("<tr><th>Comments</th></tr>").append($("<td>").append(comments)));

    table.append(tbody);
    container.append(table);

    container.append(components_closeButton(componentId));
    return container;
}

function components_component_bug(data, componentId){
    var container = $("<div class='container-fluid'>");

    var table = $("<table class='table table-hover table-condensed'>");
    var tbody = $("<tbody>");
    tbody.append($("<tr><th>Id</th><td>" + data.id + "</td></tr>"));
    tbody.append($("<tr><th>Name</th><td>" + data.name + "</td></tr>"));
    tbody.append($("<tr><th>Description</th><td>" + data.description + "</td></tr>"));
    tbody.append($("<tr><th>Project</th><td>" + data.project + "</td></tr>"));
    tbody.append($("<tr><th>Component</th><td>" + data.component + "</td></tr>"));
    tbody.append($("<tr><th>Priority</th><td>" + data.priority + "</td></tr>"));
    tbody.append($("<tr><th>Bugfix Time</th><td>" + data.bugfixTime + "</td></tr>"));
    tbody.append($("<tr><th>Costs</th><td>" + data.name + "</td></tr>"));

    var comments = $("<table class='table table-hover table-condensed'></table>");
    var tbodyComments = $("<tbody>");
    for(var i = 0; i < data.comments.length; i++){
        var comment = data.comments[i];
        tbodyComments.append($("<tr><th>#" + (i + 1) + "</th><td>" + comment + "</td></tr>"));
    }
    comments.append(tbodyComments);
    tbody.append($("<tr><th>Comments</th></tr>").append($("<td>").append(comments)));

    table.append(tbody);
    container.append(table);

    container.append(components_closeButton(componentId));
    return container;
}

function components_toggleFilter(ele){
    $(ele).parent().parent().next().find("input[type='checkbox']").each(function(i, data){data.checked = $(ele).is(":checked");});
}

function components_returnToOverview(){
    var content = $("#site-content").empty();

    $.get(REST_URIS.manage.getComponentsFilter, function(data){
        content.append(components(data));
    })
}

function components_closeButton(componentId){
    return $("<div class='container-fluid'>").append("<button class='btn btn-primary'>Close</button>").click(function(){
        $.getJSON(REST_URIS.manage.getComponent + "/" + componentId + ".json", function(data){
            $("#site-content").empty().append(components_viewComponent(data))
        });
    });
}

function components_setComponentIdField(componentId){
    $("#site-content").prepend("<input type='hidden' value='" + componentId + "' id='components_componentId' />");
}

function components_getComponentId(){
    return $("#components_componentId").val();
}

function components_addItemsView(types, section){

    var buttons = [];
    $.each(types, function(i, element){buttons.push(components_viewItemsAddButtonChoice(element))});
    var dialog = bootbox.dialog("Please select type to import:", buttons);

    dialog.find("a.btn").each(function (i, item) {
        $(item).click(function () {

            var id = components_getComponentId();

            var overview = $("<div class='container-fluid' id='importedEntryShowView'>");

            var url = "";
            var fkt = null;
            var expand = false;

            switch ($(item).attr("data-store")) {
                case "jira":
                    url = REST_URIS.import.getJIRAIssuesFilter;
                    fkt = importedEntryShowView_JIRAFilter;
                    expand = true;
                    break;
                case "confluence":
                    url = REST_URIS.import.getConfluenceFilterValues;
                    fkt = importedEntryShowView_confluenceFilter;
                    break;
                case "spiraTest":
                    url = REST_URIS.import.getSpiraTestFilter;
                    fkt = importedEntryShowView_spiraTestfilter;
            }
            components_viewItemAddTypeView(overview, url, $(item).attr("data-store"), true, fkt);

            var dia = bootbox.dialog(overview, [
                components_viewItemsAddButtonChoice("addItems","btn-primary","bootbox-btn-add"),
                components_viewItemsAddButtonChoice("close")
            ]);

            if(expand){
                dia.animate({
                    "width": "80%",
                    "left": "0",
                    "margin-left": "10%"
                }, 200);
            }

            dia.find("#bootbox-btn-add").click(function () {
                var ids = [];
                $("#importedEntryShowView_results input:checked").each(function (i, element) {
                    ids.push($(element).attr("data-id"));
                });

                if (ids.length <= 0) {
                    bootbox.dialog("Please select at least one item!", [components_viewItemsAddButtonChoice("ok", "btn-danger")]);
                    return;
                }

                var data = {
                    "type": section,
                    "data": ids

                };

                $.post(REST_URIS.manage.postAddItem.replace("{id}", id), data, function () {
                    bootbox.dialog("Import successfully!", [components_viewItemsAddButtonChoice("close", "btn-success")]);
                })
                    .fail(function () {
                        bootbox.dialog("Import failed!", [components_viewItemsAddButtonChoice("close", "btn-danger")]);
                    });
            });

        })
    });
}

function components_viewItemsAddButtonChoice(type, clazz, id){

    var data = {};
    var options = {};
    var label = "";


    data["class"] = typeof clazz !== "undefined" ? clazz : "btn";
    if(typeof id !== "undefined"){data["id"] = id;}

    switch(type){
        case "jira":
            label = "JIRA";
            options = {"data": "jira"};
            break;
        case "confluence":
            label = "Confluence";
            options = {"data": "confluence"};
            break;
        case "spiraTest":
            label =  "SpiraTest";
            options =  {"data": "spiraTest"};
            break;
        case "addItems":
            label = "Add Items";
            break;
        case "ok":
            label = "OK";
            break;
        case "close":
        default:
            label = "Close";
    }

    data["label"] = label;
    data["options"] = options;

    return data;
}

function components_viewItemAddTypeView(element, url, type, isCheckbox, fkt){
    $.getJSON(url , {}, function (data) {
        element.append(fkt(data));
        element.append(importedEntryShowView_addUpdateButton(type, {"isCheckbox":isCheckbox,"isBootbox":true}));
    });
}