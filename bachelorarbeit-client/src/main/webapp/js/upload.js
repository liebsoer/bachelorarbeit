/**
 * Created with IntelliJ IDEA.
 * User: liebsoer
 * Date: 06.08.13
 * Time: 21:23
 * To change this template use File | Settings | File Templates.
 */


function upload_form(url, acceptFileTypes, maxFileSize){

    var uploadButton = $('<button/>')
        .addClass('btn')
        .prop('disabled', true)
        .text('Processing...')
        .on('click', function () {
            var $this = $(this);
            var data = $this.data();
            $this.off('click')
                .text('Abort')
                .on('click', function () {
                    $this.remove();
                    data.abort();
                })
            ;
            data.submit().always(function () {
                $this.remove();
            });
        })
    ;

    var uploadForm = $("<div class='container-fluid'/>")
            .append(
                $("<span class='btn btn-success fileinput-button'><i class='icon-plus icon-white'></i><span>Add file...</span></span>")
                    .append(
                        $("<input id='fileupload' type='file' name='file'>")
                    )
            )
            .append("<br/><br/>")
            .append("<div id='progress' class='progress progress-success progress-striped'><div class='bar' style='text-align:center'></div></div>")
            .append("<div id='files' class='files'></div>")
        ;

    var filesize = 4000000000;
    var filetypes = ".*";

    if(typeof maxFileSize != "undefined"){
        filesize == maxFileSize;
    }

    if(typeof acceptFileTypes != "undefined"){
        filetypes = "";
        for(var i = 0; i < acceptFileTypes.length; i++){
            filetypes += acceptFileTypes[i];
            if(i < acceptFileTypes.length - 1){
                filetypes += "|"
            }
        }
    }

    acceptFileTypes= new RegExp("(\\.|\\/)(" + filetypes + ")","g");

    uploadForm.find("#fileupload").fileupload({
        url: url,
        dataType: 'json',
        autoUpload: false,
        acceptFileTypes: acceptFileTypes,
        maxFileSize: filesize, // default 4 GB
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        previewCrop: true
        })
        .on('fileuploadadd', function (e, data) {
            $("#files").empty();
            data.context = $('<div/>').appendTo('#files');
            $.each(data.files, function (index, file) {
                var node = $('<p/>')
                    .append($('<span/>').text(file.name));
                if (!index) {
                    node
                        .append('<br>')
                        .append(uploadButton.clone(true).data(data));
                }
                node.appendTo(data.context);
            });
        })
        .on('fileuploadprocessalways', function (e, data) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.preview) {
                node
                    .prepend('<br>')
                    .prepend(file.preview);
            }
            if (file.error) {
                node
                    .append('<br>')
                    .append(file.error);
            }
            if (index + 1 === data.files.length) {
                data.context.find('button')
                    .text('Upload')
                    .prop('disabled', !!data.files.error);
            }
        })
        .on('fileuploaddone', function (e, data) {
            bootbox.dialog("Upload successfully!",{
                "label": "Close",
                "class": "btn-success"
            });
        })
        .on('fileuploadfail', function (e, data) {
            var alertError = $("<div class='container-fluid'>Upload failed!</div>");
            $.each(data.result.files, function (index, file) {
                var error = $('<span/>').text(file.error);
                alertError.append('<br>').append(error);
            });

            bootbox.dialog(alertError,[{
                "label": "Close",
                "class": "btn-danger"
            }])
        })
        .prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled')

    return uploadForm;;
}