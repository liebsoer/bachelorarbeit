var CONFIG = {
    "baseRESTUri": "http://127.0.0.1:8080/bacServer/rest/",
    "images": {
        "priority": {
            "critical": "img/blocker.png",
            "major": "img/major.png",
            "minor": "img/minor.png"
        },
        "issueType": {
            "task": "img/task.png",
            "bug": "img/bug.png",
            "changerequest": "img/newfeature.png"
        },
        "statusType": {
            "open": "img/open.png",
            "closed": "img/closed.png",
            "technicalreview": "img/information.png"
        }
    }
};