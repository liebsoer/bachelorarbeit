/**
 * Created with IntelliJ IDEA 12.
 * User: liebsoer
 * Date: 11.06.13
 * Time: 10:17
 * To change this template use File | Settings | File Templates.
 */

/**
 *
 * @param element
 * @param onClick
 * @param buttonType
 */
function addViewButton(element, onClick, buttonType){
    $(element).append(
        $("<a>")
            .text(" View")
            .addClass("btn btn-mini " + (typeof buttonType == 'undefined' ? "btn-primary" : (buttonType == "default" ? "" : buttonType)))
            .prepend(
                $("<i>").addClass("icon-white icon-eye-open").removeClass(buttonType != "default" ? "": "icon-white")
            )
            .click(onClick)
    );
    //<a class="btn btn-mini btn-primary"><i class="icon-download"></i>&nbsp;Import </a>
}

function addImportButton(element, onClick, buttonType){
    $(element).append(
        $("<a>")
            .text(" Import")
            .addClass("btn btn-mini " + (typeof buttonType == 'undefined' ? "btn-primary" : (buttonType == "default" ? "" : buttonType)))
            .prepend(
                $("<i>").addClass("icon-white icon-download").removeClass(buttonType != "default" ? "": "icon-white")
            )
            .click(onClick)
    );
    //<a class="btn btn-mini btn-primary"><i class="icon-download"></i>&nbsp;Import </a>
}

function addEditButton(element, onClick, buttonType){
    $(element).append(
        $("<a>")
            .text("  Edit  ")
            .addClass("btn btn-mini " + (typeof buttonType == 'undefined' ? "btn-warning" : (buttonType == "default" ? "" : buttonType)))
            .prepend(
                $("<i>").addClass("icon-white icon-edit").removeClass(buttonType != "default" ? "": "icon-white")
            )
            .click(onClick)
    );
    //<a class="btn btn-mini"><i class="icon-edit"></i>&nbsp;&nbsp;&nbsp;Edit&nbsp;&nbsp;</a>
}

function addSaveButton(element, onClick, buttonType){
    $(element).append(
        $("<a>")
            .text(" Save")
            .addClass("btn btn-mini " + (typeof buttonType == 'undefined' ? "btn-success" : (buttonType == "default" ? "" : buttonType)))
            .prepend(
                $("<i>").addClass("icon-white icon-ok").removeClass(buttonType != "default" ? "": "icon-white")
            )
            .click(onClick)
    );
}

function addCancelButton(element, onClick, buttonType){
    $(element).append(
        $("<a>")
            .text(" Cancel")
            .addClass("btn btn-mini " + (typeof buttonType == 'undefined' ? "btn-inverse" : (buttonType == "default" ? "" : buttonType)))
            .prepend(
                $("<i>").addClass("icon-white icon-ban-circle").removeClass(buttonType != "default" ? "": "icon-white")
            )
            .click(onClick)
    );
}

function addDeleteButton(element, onClick, buttonType){
    $(element).append(
        $("<a>")
            .text(" Delete")
            .addClass("btn btn-mini " + (typeof buttonType == 'undefined' ? "btn-danger" : (buttonType == "default" ? "" : buttonType)))
            .prepend(
                $("<i>").addClass("icon-white icon-remove-circle").removeClass(buttonType != "default" ? "": "icon-white")
            )
            .click(onClick)
    );
}