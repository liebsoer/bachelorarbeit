/**
 * Created with IntelliJ IDEA 12.
 * User: liebsoer
 * Date: 14.06.13
 * Time: 19:10
 * To change this template use File | Settings | File Templates.
 */

var REST_URIS = {
    "test":{
        "postUpload": CONFIG.baseRESTUri + "test/upload"
    },
    "connection":{
        "getOverview": CONFIG.baseRESTUri + "connection",
        "deleteConnection": CONFIG.baseRESTUri + "connection",
        "postConnection": CONFIG.baseRESTUri + "connection",
        "getConnection": CONFIG.baseRESTUri + "connection",
        "putConnection": CONFIG.baseRESTUri + "connection",
        "getConnectionMeta": CONFIG.baseRESTUri + "connection/{id}/meta"
    },
    "import": {
        "getJIRAProjects": CONFIG.baseRESTUri + "import/jira/config/projects",
        "getJIRAImportConfig" : CONFIG.baseRESTUri + "import/jira/config",
        "getJIRAIssues": CONFIG.baseRESTUri + "import/jira/issues",
        "getJIRAIssuesFilter": CONFIG.baseRESTUri + "import/jira/issues/filter",
        "getJIRAFilterValues": CONFIG.baseRESTUri + "import/jira/filter",
        "getConfluenceProjects": CONFIG.baseRESTUri + "import/confluence/projects",
        "getConfluencePages": CONFIG.baseRESTUri + "import/confluence/pages",
        "getConfluenceImportedPages": CONFIG.baseRESTUri + "import/confluence/pages",
        "getConfluenceFilterValues": CONFIG.baseRESTUri + "import/confluence/filter",
        "postJIRAImport": CONFIG.baseRESTUri + "import/jira/import",
        "postConfluencePages": CONFIG.baseRESTUri + "import/confluence/import",
        "getSpiraTestFilter": CONFIG.baseRESTUri + "import/spiraTest/cases/filter"
    },
    "manage": {
        "getComponentsFilter": CONFIG.baseRESTUri + "manage/component/meta",
        "getComponentType": CONFIG.baseRESTUri + "/componentTypes.json",
        "getComponents": CONFIG.baseRESTUri + "manage/component",
        "getComponent": CONFIG.baseRESTUri + "manage/component",
        "postComponent": CONFIG.baseRESTUri + "manage/component",
        "postComponentType": CONFIG.baseRESTUri + "/componentType",
        "deleteComponent": CONFIG.baseRESTUri + "manage/component",
        "postComponentItems": CONFIG.baseRESTUri + "/componentItems",
        "postAddItem": CONFIG.baseRESTUri + "manage/component/{id}/item"

    },
    "analyse": {

    },
    "report":{

    }
};