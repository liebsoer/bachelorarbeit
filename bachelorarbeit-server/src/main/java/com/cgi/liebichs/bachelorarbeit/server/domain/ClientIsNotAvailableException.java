package com.cgi.liebichs.bachelorarbeit.server.domain;

public class ClientIsNotAvailableException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5409016959027762876L;

	public ClientIsNotAvailableException(){
		super();
	}
	
	public ClientIsNotAvailableException(String message){
		super(message);
	}
	
	public ClientIsNotAvailableException(Throwable cause){
		super(cause);
	}
	
	public ClientIsNotAvailableException(String message, Throwable cause){
		super(message, cause);
	}
	
	
}
