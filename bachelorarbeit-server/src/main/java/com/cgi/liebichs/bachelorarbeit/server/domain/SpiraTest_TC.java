package com.cgi.liebichs.bachelorarbeit.server.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;

@Entity
public class SpiraTest_TC implements Serializable{
    private static final long serialVersionUID = -3358797557084834092L;

    @Column @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    private int testCaseId;
    private int projectId;
    private String name;
    private String executionDate;
    private String creationDate;
    private String concurrencyDate;
    private String lastUpdateDate;
    
    @ElementCollection(targetClass = SpiraTest_TR.class)
    @JoinTable(name = "SpiraTest_TC_SpiraTest_TR",joinColumns = @JoinColumn(name="SpiraTest_TC__id"))
    @org.hibernate.annotations.IndexColumn(name = "POSITION", base = 1)
    @Column(name = "TestRun", nullable = false)
    private Set<SpiraTest_TR> testRuns;
    
    @ElementCollection(targetClass = SpiraTest_TS.class)
    @JoinTable(name = "SpiraTest_TC_SpiraTest_TS",joinColumns = @JoinColumn(name="SpiraTest_TC__id"))
    @org.hibernate.annotations.IndexColumn(name = "POSITION", base = 1)
    @Column(name = "TestStep", nullable = false)
    private Set<SpiraTest_TS> testSteps;
    
    @ElementCollection(targetClass = SpiraTest_TI.class)
    @JoinTable(name = "SpiraTest_TC_SpiraTest_TI",joinColumns = @JoinColumn(name="SpiraTest_TC__id"))
    @org.hibernate.annotations.IndexColumn(name = "POSITION", base = 1)
    @Column(name = "Incident", nullable = false)
    private Set<SpiraTest_TI> incidents;
    
    public SpiraTest_TC() {
        this.testRuns = new HashSet<SpiraTest_TR>();
        this.testSteps = new HashSet<SpiraTest_TS>();
        this.incidents = new HashSet<SpiraTest_TI>();
    }
    
    public long getId() {
        return this.id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public int getTestCaseId() {
        return this.testCaseId;
    }
    public void setTestCaseId(int testCaseId) {
        this.testCaseId = testCaseId;
    }
    public int getProjectId() {
        return this.projectId;
    }
    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getExecutionDate() {
        return this.executionDate;
    }
    public void setExecutionDate(String executionDate) {
        this.executionDate = executionDate;
    }
    public String getCreationDate() {
        return this.creationDate;
    }
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }
    public String getConcurrencyDate() {
        return this.concurrencyDate;
    }
    public void setConcurrencyDate(String concurrencyDate) {
        this.concurrencyDate = concurrencyDate;
    }
    public String getLastUpdateDate() {
        return this.lastUpdateDate;
    }
    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }
    public Set<SpiraTest_TR> getTestRuns() {
        return this.testRuns;
    }
    public void setTestRuns(Set<SpiraTest_TR> testRuns) {
        this.testRuns = testRuns;
    }
    public Set<SpiraTest_TS> getTestSteps() {
        return this.testSteps;
    }
    public void setTestSteps(Set<SpiraTest_TS> testSteps) {
        this.testSteps = testSteps;
    }
    public Set<SpiraTest_TI> getIncidents() {
        return this.incidents;
    }
    public void setIncidents(Set<SpiraTest_TI> incidents) {
        this.incidents = incidents;
    }

    
    
    
}
