package com.cgi.liebichs.bachelorarbeit.server.domain;

import org.codehaus.jackson.annotate.JsonCreator;

public enum EServerType {

    JIRA("jira"), CONFLUENCE("confluence"), SPIRATEST("spiraTest");
    
    private String type;
    private EServerType(final String type){
        this.type = type;
    }
    
    public String getType(){
        return this.type;
    }
    
    @Override
    public String toString() {
        return this.type;
    }
    
    @JsonCreator
    public static EServerType create (String value) {
        if(value == null) {
            throw new IllegalArgumentException();
        }
        for(EServerType v : values()) {
            if(value.equals(v.getType())) {
                return v;
            }
        }
        throw new IllegalArgumentException();
    }
    
}
