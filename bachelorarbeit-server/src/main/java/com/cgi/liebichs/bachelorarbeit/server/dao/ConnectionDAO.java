package com.cgi.liebichs.bachelorarbeit.server.dao;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.cgi.liebichs.bachelorarbeit.server.domain.Connection;
import com.cgi.liebichs.bachelorarbeit.server.utils.HibernateUtil;

public class ConnectionDAO {

    private static final String TABLE_NAME_CONNECTION = "Connection";
    
    private static Logger log = Logger.getLogger(ConnectionDAO.class);
    static {
        try {
            log.addAppender(new FileAppender(new SimpleLayout(), "/home/liebsoer/logs/bacServer/" + ConnectionDAO.class.getSimpleName(), false));
            log.setLevel(Level.ALL);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private SessionFactory sessionFactory;

    public ConnectionDAO() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    public void addConnection(final Connection connection) {
        final Session session = getSession();
        final Transaction tx = session.beginTransaction();
       
        log.debug("addConnection(): " + connection.toString());
        session.save(connection);
       
        tx.commit();
        session.close();
    }

    public Connection getConnection(long id) {
        final Session session = getSession();
        final Connection con = (Connection) session.get(Connection.class, id);
        session.close();
        return con;
    }
    
    public Session getSession(){
        return sessionFactory.openSession();
    }

    public List<Connection> getConnections() {
        final Session session = getSession();
        final Query q = session.createQuery("from " + TABLE_NAME_CONNECTION);
        @SuppressWarnings("unchecked") final List<Connection> res = q.list();
        for(final Connection con: res){
            log.debug("getConnections(): " + con.toString());
        }
        session.close();
        return res;
    }

    public void updateConnection(final Connection con) {
        final Session session = getSession();
        final Transaction tx = session.beginTransaction();
        
        log.debug("updateConnection(): " + con.toString());
        session.saveOrUpdate(con);
        
        tx.commit();
        session.close();
        
    }
    
    public void deleteConnection(final long id){
        final Session session = getSession();
        final Transaction tx = session.beginTransaction();
        
        final Connection con = (Connection) session.get(Connection.class, id); 
        log.debug("deleteConnection(): " + con.toString());
        session.delete(con);
        
        tx.commit();
        session.close();

    }
    
    

}
