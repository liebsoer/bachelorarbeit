package com.cgi.liebichs.bachelorarbeit.server.filter;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerResponse;
import com.sun.jersey.spi.container.ContainerResponseFilter;

public class ResponseCorsFilter implements ContainerResponseFilter {

    @Override
    public ContainerResponse filter(ContainerRequest request, ContainerResponse response) {
        
        ResponseBuilder resp = Response.fromResponse(response.getResponse());
        resp.header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE")
            .header("Access-Control-Allow-Headers", "Content-Type, accept, origin, x-csrftoken, content-type, accept")
//            .header("Access-Control-Allow-Headers", "accept, origin, authorization, "
//                + "content-type, content-length, connection, x-requested-with, user-agent")
        ;

        String reqHead = request.getHeaderValue("Access-Control-Request-Headers");

        if (null != reqHead && !reqHead.equals("")) {
            resp.header("Access-Control-Allow-Headers", reqHead);
        }
        
        
        response.setResponse(resp.build());
        return response;
    }

}
