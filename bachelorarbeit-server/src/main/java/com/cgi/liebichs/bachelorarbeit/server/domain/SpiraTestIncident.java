package com.cgi.liebichs.bachelorarbeit.server.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class SpiraTestIncident implements Serializable {
    private static final long serialVersionUID = 3998619932041679938L;
    
    @Column @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private int incidentId;
    private int projectId;
    private int testCaseId;
    private String name;
    @Column(columnDefinition="TEXT") @Lob
    private String description;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "incidentId")
//    private List<String> jiraIssue;
    
    public long getId() {
        return this.id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public int getIncidentId() {
        return this.incidentId;
    }
    public void setIncidentId(int incidentId) {
        this.incidentId = incidentId;
    }
    public int getProjectId() {
        return this.projectId;
    }
    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return this.description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
//    public List<String> getJiraIssue() {
//        return this.jiraIssue;
//    }
//    public void setJiraIssue(List<String> jiraIssue) {
//        this.jiraIssue = jiraIssue;
//    }
    
    public int getTestCaseId() {
        return testCaseId;
    }
    public void setTestCaseId(int testCaseId) {
        this.testCaseId = testCaseId;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.description == null) ? 0 : this.description.hashCode());
        result = prime * result + (int) (this.id ^ (this.id >>> 32));
        result = prime * result + this.incidentId;
//        result = prime * result + ((this.jiraIssue == null) ? 0 : this.jiraIssue.hashCode());
        result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
        result = prime * result + this.projectId;
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SpiraTestIncident other = (SpiraTestIncident) obj;
        if (this.id != other.id)
            return false;
        if (this.incidentId != other.incidentId)
            return false;
        if (this.name == null) {
            if (other.name != null)
                return false;
        } else if (!this.name.equals(other.name))
            return false;
        if (this.projectId != other.projectId)
            return false;
        return true;
    }
    @Override
    public String toString() {
        return "SpiraTestIncident [id=" + this.id + ", incidentId=" + this.incidentId + ", projectId=" + this.projectId
            + ", name=" + this.name + ", description=" + this.description + ", "
//                + "jiraIssue=" + this.jiraIssue 
                + "]";
    }
    
    

}
