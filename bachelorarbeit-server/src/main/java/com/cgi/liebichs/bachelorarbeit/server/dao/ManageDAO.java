package com.cgi.liebichs.bachelorarbeit.server.dao;

import com.cgi.liebichs.bachelorarbeit.server.domain.Component;
import com.cgi.liebichs.bachelorarbeit.server.utils.HibernateUtil;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ManageDAO {

    private static final String TABLE_NAME_MANAGE = "Manage";
    private static final String TABLE_NAME_COMPONENT = "Component";

    private static Logger log = Logger.getLogger(ManageDAO.class);
    static {
        try {
            log.addAppender(new FileAppender(new SimpleLayout(), "/home/liebsoer/logs/bacServer/"
                + ManageDAO.class.getSimpleName(), false));
            log.setLevel(Level.ALL);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private SessionFactory sessionFactory;

    public ManageDAO() {
        sessionFactory = HibernateUtil.getSessionFactory();
    }

    public Session getSession() {
        return sessionFactory.openSession();
    }

    public void addComponent(final Component component) {
        final Session session = getSession();
        final Transaction tx = session.beginTransaction();

        log.debug("addConnection(): " + component.toString());
        session.save(component);

        tx.commit();
        session.close();
    }

    public Component getComponent(final long id) {
        final Session session = getSession();
        final Component component = (Component) session.get(Component.class, id);
        session.close();
        return component;
    }

    public List<Component> getComponents(List<String> filter) {
        final Session session = getSession();
        final StringBuilder sb = new StringBuilder("FROM " + TABLE_NAME_COMPONENT);
        
        if(filter != null && filter.size() > 0){
            sb.append(" WHERE type IN (");
            
            final Iterator<String> it = filter.iterator();
            while(it.hasNext()){
                sb.append("\'"+ it.next() + "\'");
                if(it.hasNext()){
                    sb.append(",");
                }
            }
            
            sb.append(")");
            
        }
        
        log.debug(sb.toString());
        
        final Query q = session.createQuery(sb.toString());
        
        final List list = q.list();
        
        @SuppressWarnings("unchecked")
        List<Component> res = (List<Component>) list;
        for (final Component component : res) {
            log.debug("getConnections(): " + component.toString());
        }
        session.close();
        return res;
    }

    public void updateComponent(final Component component) {
        final Session session = getSession();
        final Transaction tx = session.beginTransaction();

        log.debug("updateConnection(): " + component.toString());
        session.saveOrUpdate(component);

        tx.commit();
        session.close();

    }

    public void removeComponent(final long id) {
        final Session session = getSession();
        final Transaction tx = session.beginTransaction();

        final Component component = (Component) session.get(Component.class, id);
        log.debug("deleteComponent(): " + component.toString());
        session.delete(component);

        tx.commit();
        session.close();

    }

    public Map<String, Integer> getComponentsMeta() {
        final Session session = getSession();
        final Map<String, Integer> map = new HashMap<String, Integer>();
        
        final Query q = session.createQuery("SELECT DISTINCT type, count(type) FROM " + TABLE_NAME_COMPONENT + " GROUP BY type");
        @SuppressWarnings("unchecked") final List<Object[]> list = q.list();
        final Iterator<Object[]> it = list.iterator();
        while(it.hasNext()){
            final Object[] ele = (Object[]) it.next();
            
            map.put(String.valueOf(ele[0]), Integer.valueOf(String.valueOf(ele[1])));
        }
        
        return map;
    }

}
