package com.cgi.liebichs.bachelorarbeit.server.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class SpiraTest_PRJ implements Serializable {
    private static final long serialVersionUID = -632057082420402896L;

    @Column @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
    private String name;
    private String description;
    private int projectId;
    private int projectGroupId;
    private String projectGroupName;
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public int getProjectId() {
        return projectId;
    }
    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }
    public int getProjectGroupId() {
        return projectGroupId;
    }
    public void setProjectGroupId(int projectGroupId) {
        this.projectGroupId = projectGroupId;
    }
    public String getProjectGroupName() {
        return projectGroupName;
    }
    public void setProjectGroupName(String projectGroupName) {
        this.projectGroupName = projectGroupName;
    }
    @Override
    public String toString() {
        return "SpiraTest_PRJ [id=" + id + ", name=" + name + ", description=" + description + ", projectId="
            + projectId + ", projectGroupId=" + projectGroupId + ", projectGroupName=" + projectGroupName + "]";
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + projectGroupId;
        result = prime * result + ((projectGroupName == null) ? 0 : projectGroupName.hashCode());
        result = prime * result + projectId;
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SpiraTest_PRJ other = (SpiraTest_PRJ) obj;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (id != other.id)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (projectGroupId != other.projectGroupId)
            return false;
        if (projectGroupName == null) {
            if (other.projectGroupName != null)
                return false;
        } else if (!projectGroupName.equals(other.projectGroupName))
            return false;
        if (projectId != other.projectId)
            return false;
        return true;
    }
    
}
