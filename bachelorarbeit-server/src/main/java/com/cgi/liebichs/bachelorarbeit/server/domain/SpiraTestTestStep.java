package com.cgi.liebichs.bachelorarbeit.server.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class SpiraTestTestStep implements Serializable{
    private static final long serialVersionUID = 3401441590014912090L;

    @Column @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private int testStepId;
    private int testCaseId;
    private int position;
    @Column(columnDefinition="TEXT") @Lob
    private String description;
    @Column(columnDefinition="TEXT") @Lob
    private String expectedResult;
    
    public long getId() {
        return this.id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public int getTestStepId() {
        return this.testStepId;
    }
    public void setTestStepId(int testStepId) {
        this.testStepId = testStepId;
    }
    public int getTestCaseId() {
        return this.testCaseId;
    }
    public void setTestCaseId(int testCaseId) {
        this.testCaseId = testCaseId;
    }
    public int getPosition() {
        return this.position;
    }
    public void setPosition(int position) {
        this.position = position;
    }
    public String getDescription() {
        return this.description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getExpectedResult() {
        return this.expectedResult;
    }
    public void setExpectedResult(String expectedResult) {
        this.expectedResult = expectedResult;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.description == null) ? 0 : this.description.hashCode());
        result = prime * result + (int) (this.id ^ (this.id >>> 32));
        result = prime * result + this.position;
        result = prime * result + this.testCaseId;
        result = prime * result + this.testStepId;
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SpiraTestTestStep other = (SpiraTestTestStep) obj;
        if (this.id != other.id)
            return false;
        if (this.position != other.position)
            return false;
        if (this.testCaseId != other.testCaseId)
            return false;
        if (this.testStepId != other.testStepId)
            return false;
        return true;
    }
    @Override
    public String toString() {
        return "SpiraTestTestStep [id=" + this.id + ", testStepId=" + this.testStepId + ", testCaseId="
            + this.testCaseId + ", position=" + this.position + ", description=" + this.description
            + ", expectedResult=" + this.expectedResult + "]";
    }
    
    
}
