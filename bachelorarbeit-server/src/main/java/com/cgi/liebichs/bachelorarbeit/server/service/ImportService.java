package com.cgi.liebichs.bachelorarbeit.server.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import com.cgi.liebichs.bachelorarbeit.server.dao.client.ConfluenceClient;
import com.cgi.liebichs.bachelorarbeit.server.dao.ImportDAO;
import com.cgi.liebichs.bachelorarbeit.server.dao.client.JIRAClient;
import com.cgi.liebichs.bachelorarbeit.server.domain.ClientIsNotAvailableException;
import com.cgi.liebichs.bachelorarbeit.server.domain.ConfluencePage;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.UniformInterfaceException;

@Path("/import")
public class ImportService {

    private static Logger log = Logger.getLogger(ImportService.class);
    static {
        try {
            log.addAppender(new FileAppender(new SimpleLayout(), "/home/liebsoer/logs/bacServer/ImportService", false));
            log.addAppender(new ConsoleAppender(new SimpleLayout(), "System.out"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private ObjectMapper mapper;

    public ImportService() {
        mapper = new ObjectMapper().configure(Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String defaultRes() {
        return "{\"status\":\"ok\"}";
    }

    @GET
    @Path("/jira/config/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getJIRAImportConfig(@PathParam("id") final long id) {

        return "{status:'success',id:'" + id + "'}";
    }

    @GET
    @Path("/jira/config/projects")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayNode getJIRAImportConfigProjects(@QueryParam("id") final long id) throws IOException,
        ClientIsNotAvailableException {
        log.debug("getJIRAImportCONFIGProjects(): " + id);
        return JIRAClient.createJIRAClientForConnection(id).getProjects();
    }

    @GET
    @Path("/jira/issues")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayNode getJIRAIssues(@QueryParam("data") String dataString) throws JsonProcessingException, IOException, ClientIsNotAvailableException {
        final ImportDAO importDAO = new ImportDAO();
        final JsonNode data = mapper.readTree(dataString);
        
        final ObjectNode node = (ObjectNode) data;
        
        final Iterator<Entry<String, JsonNode>> it = node.getFields();
        
        final HashMap<String, List<String>> selects = new HashMap<String, List<String>>();
        
        while(it.hasNext()){
            final Entry<String, JsonNode> item = it.next();
            final String key = item.getKey(); 
            final ArrayNode value = (ArrayNode) item.getValue();
            
            final Iterator<JsonNode> itValue = value.iterator();
            
            selects.put(key, new ArrayList<String>());
            
            while(itValue.hasNext()){
                String type =  itValue.next().asText();
                selects.get(key).add(type);
            }
            
        }
        
        return importDAO.getIssues(selects);
    }

    @GET
    @Path("/jira/issues/filter")
    @Produces(MediaType.APPLICATION_JSON)
    public ObjectNode getJIRAIssueViewFilter() {
        final ObjectNode res = mapper.createObjectNode();

        for (final String field : new String[] { "issuetype", "status", "priority", "project" }) {

            res.put(field, getFieldCount(field));
        }

        res.put("components", getComponentCount());

        return res;
    }

    private JsonNode getComponentCount() {
        final ImportDAO importDAO = new ImportDAO();
        final Map<String, String> map = importDAO.getImportetComponentCounts();
        
        final ObjectNode res = mapper.createObjectNode();
        for(final String s: map.keySet())
        {
            res.put(s, map.get(s));
        }
        return res;
    }

    public ObjectNode getFieldCount(final String field) {
        final ImportDAO importDao = new ImportDAO();
        final ObjectNode res = mapper.createObjectNode();

        final Iterator<Entry<String, String>> it = importDao.getImportetFieldCounts(field).entrySet().iterator();
        while (it.hasNext()) {
            final Entry<String, String> item = it.next();
            res.put(item.getKey(), item.getValue());

        }

        return res;
    }

    @GET
    @Path("/jira/filter/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ObjectNode getJIRAFilter(@QueryParam("data[]") List<String> projects, @PathParam("id") final long id)
        throws JsonProcessingException, ClientIsNotAvailableException, IOException {
        final String[] fields = new String[] { "issuetype", "status", "priority", "component" };

        final ObjectNode node = mapper.createObjectNode();

        final JIRAClient client = JIRAClient.createJIRAClientForConnection(id);
        for (final String project : projects) {

            final ObjectNode values = client.getFilterValues(project);
            for (final String field : fields) {
                if (!node.has(field)) {
                    node.put(field, values.get(field));
                } else {
                    int count = node.get(field).get("count").asInt() + values.get(field).get("count").asInt();

                    ((ObjectNode) node.get(field)).put("count", count);
                }
            }

        }

        return node;
    }

    @POST
    @Path("/jira/import/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ObjectNode postImportJIRAIssues(@PathParam("id") final long id,
        @QueryParam("issuetype") final String issuetype, @QueryParam("status") final String status,
        @QueryParam("priority") final String priority, @QueryParam("component") final String component,
        @QueryParam("project") final String project) throws JsonProcessingException, IOException,
        ClientIsNotAvailableException {

        final ObjectNode root = mapper.createObjectNode();

        final ArrayNode issueAn = mapper.createArrayNode();
        final ArrayNode statusAn = mapper.createArrayNode();
        final ArrayNode prioAn = mapper.createArrayNode();
        final ArrayNode compAn = mapper.createArrayNode();
        final ArrayNode prjAn = mapper.createArrayNode();

        if (!issuetype.isEmpty()) {
            for (final String s : issuetype.split(",")) {
                issueAn.add(s);
            }
            root.put("issuetype", issueAn);
        }

        if (!status.isEmpty()) {
            for (final String s : status.split(",")) {
                statusAn.add(s);
            }
            root.put("status", statusAn);
        }

        if (!priority.isEmpty()) {
            for (final String s : priority.split(",")) {
                prioAn.add(s);
            }
            root.put("priority", prioAn);
        }

        if (!component.isEmpty()) {
            for (final String s : component.split(",")) {
                compAn.add(s);
            }
            root.put("component", compAn);
        }

        if (!project.isEmpty()) {
            for (final String s : project.split(",")) {
                prjAn.add(s);
            }
            root.put("project", prjAn);
        }

        final JIRAClient client = JIRAClient.createJIRAClientForConnection(id);
        final ArrayNode issues = client.getImportIssues(root);

        final ImportDAO importDAO = new ImportDAO();
        final boolean success = importDAO.saveImportedIssues(issues);

        final ObjectNode result = mapper.createObjectNode();
        result.put("status", success ? "success" : "fail");
        result.put("count", issues.size());
        result.put("projects", prjAn.asText());
        result.put("data", issues);

        return result;
    }

    @GET @Path("/confluence/projects/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayNode getConfluenceProjects(@PathParam("id") final long id, @QueryParam("root") final String root) throws JsonProcessingException, ClientIsNotAvailableException, IOException{
        final ConfluenceClient client = ConfluenceClient.createClient(id);
        if(root.equals("source")){
            return client.getProjects();
        }else{
            return client.getPages(root);
        }
    }
    
    @GET
    @Path("/confluence/pages")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayNode getConfluencePages(@QueryParam("projects[]") final List<String> projects, @QueryParam("pageName") final String search) throws JsonProcessingException, IOException {
        final ImportDAO dao = new ImportDAO(); 
        final ArrayNode result = mapper.createArrayNode();
        
        for(final String project: projects){
            log.debug("project: " + project + "; search: "+ (search != null ? search : "null"));
            final ArrayNode projectPages = search != null && !search.isEmpty() ? dao.getConfluencePages(project, search) : dao.getConfluencePages(project);
            result.addAll(projectPages);
        }
        
        return result;
    }

    @POST
    @Path("/confluence/import/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ObjectNode postConfluencePages(@PathParam("id") final long id, final List<String> data) throws JsonProcessingException, IOException, ClientHandlerException, UniformInterfaceException, ClientIsNotAvailableException {
        final ImportDAO dao = new ImportDAO();
        final ConfluenceClient client = ConfluenceClient.createClient(id);

        final List<ConfluencePage> pageList = new ArrayList<ConfluencePage>();
        for(final String pageId: data){
            pageList.add(client.getConfluencePage(pageId));
        }
        
        dao.saveImportedPages(pageList);
        
        return null;
    }
    @GET
    @Path("/confluence/filter")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayNode getConfluenceFilter() {
        final ImportDAO dao = new ImportDAO();
        final Map<String, Long> map = dao.getConfluenceProjectsWithCount();
        final Iterator<Entry<String, Long>> it = map.entrySet().iterator();
        
        final ArrayNode result = mapper.createArrayNode();
        while(it.hasNext()){
            final Entry<String, Long> item = it.next();
            final String[] key = item.getKey().split("\\$\\{#\\}");
            final Long value = item.getValue();
            
            final ObjectNode on = mapper.createObjectNode();
            on.put("key", key[0]);
            on.put("name", key[1]);
            on.put("count", value);
            
            result.add(on);
        }
         
        
        return result;
    }
}
