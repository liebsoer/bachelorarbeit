package com.cgi.liebichs.bachelorarbeit.server.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;

@Entity
public class Project implements Serializable {
    private static final long serialVersionUID = 3634837790312249884L;

    @Column(unique = true, nullable = false) @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    private String name;
    
    @ElementCollection(targetClass = SpiraTest_TI.class)
    @JoinTable(name = "Project_JIRABugIssue",joinColumns = @JoinColumn(name="Project_id"))
    @org.hibernate.annotations.IndexColumn(name = "POSITION", base = 1)
    @Column(name = "JIRABugIssue", nullable = false)
    private Set<JIRAIssue> jiraBugIssues;
    
    @ElementCollection(targetClass = SpiraTest_TI.class)
    @JoinTable(name = "Project_SpiraTestCases",joinColumns = @JoinColumn(name="Project_id"))
    @org.hibernate.annotations.IndexColumn(name = "POSITION", base = 1)
    @Column(name = "SpiraTestCase", nullable = false)
    private Set<SpiraTest_TC> spiraTestCases;
    
    @ElementCollection(targetClass = SpiraTest_TI.class)
    @JoinTable(name = "Project_JIRASpecIssue",joinColumns = @JoinColumn(name="Project_id"))
    @org.hibernate.annotations.IndexColumn(name = "POSITION", base = 1)
    @Column(name = "JIRASpecIssue", nullable = false)
    private Set<JIRAIssue> jiraSpecIssue;
    
    @ElementCollection(targetClass = SpiraTest_TI.class)
    @JoinTable(name = "Project_ConfluenceSpec",joinColumns = @JoinColumn(name="Project_id"))
    @org.hibernate.annotations.IndexColumn(name = "POSITION", base = 1)
    @Column(name = "ConfluenceSpec", nullable = false)
    private Set<ConfluencePage> confluenceSpec;
    
    public Project() {
        this.jiraBugIssues = new HashSet<JIRAIssue>();
        this.spiraTestCases = new HashSet<SpiraTest_TC>();
        this.jiraSpecIssue = new HashSet<JIRAIssue>();
        this.confluenceSpec = new HashSet<ConfluencePage>();
    }
    
    public long getId() {
        return this.id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Set<JIRAIssue> getJiraBugIssues() {
        return this.jiraBugIssues;
    }
    public void setJiraBugIssues(Set<JIRAIssue> jiraBugIssues) {
        this.jiraBugIssues = jiraBugIssues;
    }
    public Set<SpiraTest_TC> getSpiraTestCases() {
        return this.spiraTestCases;
    }
    public void setSpiraTestCases(Set<SpiraTest_TC> spiraTestCases) {
        this.spiraTestCases = spiraTestCases;
    }
    public Set<JIRAIssue> getJiraSpecIssue() {
        return this.jiraSpecIssue;
    }
    public void setJiraSpecIssue(Set<JIRAIssue> jiraSpecIssue) {
        this.jiraSpecIssue = jiraSpecIssue;
    }
    public Set<ConfluencePage> getConfluenceSpec() {
        return this.confluenceSpec;
    }
    public void setConfluenceSpec(Set<ConfluencePage> confluenceSpec) {
        this.confluenceSpec = confluenceSpec;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.confluenceSpec == null) ? 0 : this.confluenceSpec.hashCode());
        result = prime * result + (int) (this.id ^ (this.id >>> 32));
        result = prime * result + ((this.jiraBugIssues == null) ? 0 : this.jiraBugIssues.hashCode());
        result = prime * result + ((this.jiraSpecIssue == null) ? 0 : this.jiraSpecIssue.hashCode());
        result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
        result = prime * result + ((this.spiraTestCases == null) ? 0 : this.spiraTestCases.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Project other = (Project) obj;
        if (this.confluenceSpec == null) {
            if (other.confluenceSpec != null)
                return false;
        } else if (!this.confluenceSpec.equals(other.confluenceSpec))
            return false;
        if (this.id != other.id)
            return false;
        if (this.jiraBugIssues == null) {
            if (other.jiraBugIssues != null)
                return false;
        } else if (!this.jiraBugIssues.equals(other.jiraBugIssues))
            return false;
        if (this.jiraSpecIssue == null) {
            if (other.jiraSpecIssue != null)
                return false;
        } else if (!this.jiraSpecIssue.equals(other.jiraSpecIssue))
            return false;
        if (this.name == null) {
            if (other.name != null)
                return false;
        } else if (!this.name.equals(other.name))
            return false;
        if (this.spiraTestCases == null) {
            if (other.spiraTestCases != null)
                return false;
        } else if (!this.spiraTestCases.equals(other.spiraTestCases))
            return false;
        return true;
    }
    @Override
    public String toString() {
        return "Project [id=" + this.id + ", name=" + this.name + ", jiraBugIssues=" + this.jiraBugIssues
            + ", spiraTestCases=" + this.spiraTestCases + ", jiraSpecIssue=" + this.jiraSpecIssue + ", confluenceSpec="
            + this.confluenceSpec + "]";
    }
    
    
}
