package com.cgi.liebichs.bachelorarbeit.server.dao.client;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.Response.Status;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.cgi.liebichs.bachelorarbeit.server.dao.ConnectionDAO;
import com.cgi.liebichs.bachelorarbeit.server.domain.ClientIsNotAvailableException;
import com.cgi.liebichs.bachelorarbeit.server.domain.ConfluencePage;
import com.cgi.liebichs.bachelorarbeit.server.domain.Connection;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.core.util.Base64;

public class ConfluenceClient implements Serializable{
    private static final long serialVersionUID = 6987668522601103783L;
    
    private final static Logger LOGGER = Logger.getLogger(JIRAClient.class.getName());
    {
        LOGGER.setLevel(Level.ALL);
    }
    
    private String auth;
    private String user;
    private String url;
    private Client client;
    final private ObjectMapper mapper;
    
    private ConfluenceClient(final String username, final String password, final String url){
        this.user = username;
        this.url = url;

        if (this.url.charAt(this.url.length() - 1) == '/') {
            this.url = this.url.substring(0, this.url.length() - 1);
        }

        this.auth = "Basic " + new String(Base64.encode(user + ":" + password));

        this.client = Client.create();

        this.mapper = new ObjectMapper();
    }
    
    public static ConfluenceClient createClient(long id) {
        final ConnectionDAO connectionDAO = new ConnectionDAO();
        final Connection con = connectionDAO.getConnection(id);
        return new ConfluenceClient(con.getUsername(), con.getPassword(), con.getUrl());
    }
    
    private ClientResponse createResponse(final String url) throws ClientIsNotAvailableException {
        if (!this.isClientAvailable()) {
            throw new ClientIsNotAvailableException();
        }

        final ClientResponse response = client.resource(url).header("Authorization", this.auth)
            .type("application/json").accept("application/json").get(ClientResponse.class);
        
        
        LOGGER.info("User " + this.user + " access " + url);

        return response;
    }
    
    private boolean isClientAvailable() {
        return (this.client != null);
    }

    public void destroyClient() {
        this.client.destroy();
    }
    
    public List<String> getProjectKeys() throws ClientIsNotAvailableException, IOException, JSONException{
        final List<String> result = new LinkedList<String>();

        final JSONArray projects = getProjectsJSONArray();
        for(int i = 0; i < projects.length(); i++){
            result.add(projects.getJSONObject(i).getString("key"));
        }

        return result;
    }
    
    public JSONArray getProjectsJSONArray(String...expands) throws JSONException, ClientIsNotAvailableException{
        final StringBuilder sb = new StringBuilder();
        for(int i = 0; i < expands.length; i++){
            sb.append(expands[i]);
            if(i < expands.length - 1){
                sb.append(",");
            }
        }
        final ClientResponse response = this.createResponse(formURL("space" + (sb.length() != 0 ? "?expand=" + sb.toString(): "")));

        LOGGER.info("Status:" + response.getStatus());
        if (response.getStatus() == 200) {
            
            final String resp = response.getEntity(String.class);
            response.close();
            return new JSONObject(resp).getJSONArray("space");
        }
        response.close();
        
        return null;
    }
    
    public JSONObject getProjectJSONObject(final String projectkey, String...expands) throws ClientIsNotAvailableException, JSONException{
        final StringBuilder sb = new StringBuilder();
        for(int i = 0; i < expands.length; i++){
            sb.append(expands[i]);
            if(i < expands.length - 1){
                sb.append(",");
            }
        }
        final ClientResponse response = this.createResponse(formURL("space/" + projectkey + (sb.length() != 0 ? "?expand=" + sb.toString(): "")));
        
        if(response.getStatus() == 200){
            final String resp = response.getEntity(String.class);
            response.close();
            return new JSONObject(resp);
        }
        
        response.close();
        return null;
    }
    
    private String formURL(final String uri){
        return this.url + (this.url.charAt(this.url.length() - 1) == '/' ? "" : "/") + uri;
    }
    
    public static boolean writeJSON(final String fileName, final String content) throws IOException{
        final String filename = ConfluenceClient.class.getResource("/").getPath() + "../../src/main/resources/json/wiki/";
        new File(filename).mkdirs();
        final File file = new File(filename + fileName + ".json");
        file.createNewFile();
        final BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write(content);
        bw.close();
        
        return true;
    }

    public ArrayNode getProjects() throws ClientIsNotAvailableException, JsonProcessingException, IOException {
        ///space
        final String url = this.url + "/space";
        final ClientResponse response = this.createResponse(url);
        if(response.getStatus() == Status.OK.getStatusCode()){
            final JsonNode projectNodes = mapper.readTree(response.getEntity(String.class));
            final ArrayNode spaces = (ArrayNode) projectNodes.get("space");
            
            final ArrayNode result = mapper.createArrayNode();
            final Iterator<JsonNode> it = spaces.iterator();
            
            while(it.hasNext()){
                final JsonNode item = it.next();
                final String homeUrl = this.url + "/space/" + item.get("key").asText();
                final ClientResponse home = this.createResponse(homeUrl);
                
                if(home.getStatus() == Status.OK.getStatusCode()){
                    
                    final JsonNode homeNode = mapper.readTree(home.getEntity(String.class));
                    
                    final String id = homeNode.get("home").get("id").asText();
                    
                    final ObjectNode on = mapper.createObjectNode();
                    on.put("id", id);
                    on.put("text", item.get("name").asText());
                    on.put("hasChildren", "true");
                    
                    result.add(on);
                }
            }
            
            return result;
        }
        return null;
    }

    public ArrayNode getPages(final String root) throws ClientIsNotAvailableException, JsonProcessingException, ClientHandlerException, UniformInterfaceException, IOException {
        
        final ArrayNode result = mapper.createArrayNode(); 
        
        final String url = this.url + "/content/" + root + "?expand=children";
        final ClientResponse home = this.createResponse(url);
        
        if(home.getStatus() == Status.OK.getStatusCode()){
            final ArrayNode children = (ArrayNode) mapper.readTree(home.getEntity(String.class)).get("children").get("content");
            
            final Iterator<JsonNode> it = children.iterator();
            while(it.hasNext()){
                final JsonNode item = it.next();
                
                final ObjectNode on = mapper.createObjectNode();
                on.put("text", item.get("title").asText());
                on.put("id", item.get("id").asText());
                on.put("hasChildren", pageHasChildren(item.get("id").asText()));
                
                result.add(on);
            }
        }
        
        
        return result;
    }
    
    private boolean pageHasChildren(final String pageId) throws ClientIsNotAvailableException, JsonProcessingException, ClientHandlerException, UniformInterfaceException, IOException{
        
        final String url = this.url + "/content/" + pageId;
        final ClientResponse home = this.createResponse(url);
        
        if(home.getStatus() == Status.OK.getStatusCode()){
            final JsonNode children = mapper.readTree(home.getEntity(String.class)).get("children");
            
            if(children.get("size").asInt() > 0){
                return true;
            }
        }
        
        return false;
    }

    public ConfluencePage getConfluencePage(final String id) throws ClientIsNotAvailableException, ClientHandlerException, UniformInterfaceException, IOException{
        
        final String url = this.url + "/content/" + id;
        final ClientResponse home = this.createResponse(url);
        if(home.getStatus() == Status.OK.getStatusCode()){
            final JsonNode node = mapper.readTree(home.getEntity(String.class));
            
            final ConfluencePage page = new ConfluencePage();
            page.setTitle(node.get("title").asText());
            
            final Iterator<JsonNode> it = node.get("link").iterator();
            while(it.hasNext()){
                final ObjectNode linkNode = (ObjectNode) it.next();
                if(linkNode.get("type").asText().equals("text/html")){
                    page.setUrl(linkNode.get("href").asText());
                    break;
                }
            }
            
            page.setProjectName(node.get("space").get("title").asText());
            page.setProjectKey(node.get("space").get("key").asText());
            page.setChildPages(node.get("children").get("size").asInt());
            page.setComments(node.get("comments").get("total").asInt());
            page.setAttachments(node.get("attachments").get("size").asInt());
            page.setParent(node.get("parentId").asText());
            page.setPageId(node.get("id").asText());
            
            return page;
        }
        
        return null;
    }
    
}
