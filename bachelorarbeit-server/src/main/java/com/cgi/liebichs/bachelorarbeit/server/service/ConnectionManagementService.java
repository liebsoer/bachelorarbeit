package com.cgi.liebichs.bachelorarbeit.server.service;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.codehaus.jackson.node.POJONode;

import com.cgi.liebichs.bachelorarbeit.server.dao.ConnectionDAO;
import com.cgi.liebichs.bachelorarbeit.server.dao.client.JIRAClient;
import com.cgi.liebichs.bachelorarbeit.server.domain.ClientIsNotAvailableException;
import com.cgi.liebichs.bachelorarbeit.server.domain.Connection;
import com.cgi.liebichs.bachelorarbeit.server.domain.EServerType;
import com.cgi.liebichs.bachelorarbeit.server.utils.Utils;

@Path("/connection")
public class ConnectionManagementService {

    final private static Logger log = Logger.getLogger(ConnectionManagementService.class);
    static {
        try {
            log.addAppender(new FileAppender(new SimpleLayout(),
                "/home/liebsoer/logs/bacServer/ConnectionManagementService", false));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static final String TYPE_JSON = MediaType.APPLICATION_JSON;
    private ConnectionDAO connectionDAO;
    private ObjectMapper mapper;

    public ConnectionManagementService() {
        connectionDAO = new ConnectionDAO();
        mapper = new ObjectMapper();
    }

    @GET
    @Produces(TYPE_JSON)
    public ArrayNode getConnections() {
        final ArrayNode jsonArray = mapper.createArrayNode();
        final List<Connection> connections = connectionDAO.getConnections();
        for (final Connection con : connections) {
            jsonArray.add(new POJONode(con));
        }
        return jsonArray;
    }

    @POST
    @Produces(TYPE_JSON)
    public Response addConnection(final Connection connection) throws JsonParseException, JsonMappingException,
        IOException {
        try {
            log.debug(connection.toString());

            connection.setKey(connection.getName().toLowerCase().replaceAll(" ", "-").replaceAll("[\\(\\)]", "-"));
            connection.setIconText(connection.getName());
            connection.setIconImageUrl("img/what_icon.png");
            connection.setTypeName(connection.getType().getType());
            connection.setTypeUrl(Utils.getIconTypeImageUrl(connection.getType().getType()));
            
            connectionDAO.addConnection(connection);
            ObjectNode node = mapper.createObjectNode();
            node.put("status", "success");
            node.put("message", connection.getName() + " created");
            return Response.status(Status.CREATED).entity(node).build();
        } catch (Exception e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e).build();
        }
    }
    

    @GET @Path("/{id}/meta") @Produces(TYPE_JSON)
    public ObjectNode getConnectionMeta(@PathParam("id") final long id) throws IOException, ClientIsNotAvailableException{
        final Connection con = connectionDAO.getConnection(id);
        final ObjectNode node = mapper.createObjectNode();
        node.put("name", con.getName());
        node.put("description", con.getDescription());
        node.put("type", con.getType().getType());
        node.put("id", con.getId());
        node.put("key", con.getKey());
        node.put("url", con.getUrl());
        node.put("totalCount", JIRAClient.createJIRAClientForConnection(id).getTotalIssues());
        
        
        return node;
    }

    @PUT
    @Path("/{id}")
    @Produces(TYPE_JSON)
    public Response putConnection(final Connection connection) {
        if (connection == null) {
            return Response.status(Status.NOT_FOUND).build();
        }
        connectionDAO.updateConnection(connection);
        return Response.status(Status.OK).entity("").build();
    }
    
    @PUT
    @Path("/{id}/update")
    @Produces(TYPE_JSON)
    public ObjectNode putUpdateConnection(@PathParam("id") final long id, final String dataString) throws JsonProcessingException, IOException{
        log.debug(dataString);
        final ObjectNode data = (ObjectNode) mapper.readTree(dataString);
        
        final Connection connection = connectionDAO.getConnection(id);
        
        final Iterator<Entry<String, JsonNode>> it = data.getFields();
        while(it.hasNext()){
            final Entry<String, JsonNode> element = it.next();
            final String value = element.getValue().asText();
            
            if(element.getKey().equals("name")){
                connection.setName(value);
                connection.setKey(connection.getName().toLowerCase().replaceAll(" ", "-").replaceAll("[\\(\\)]", "-"));
            } else  if(element.getKey().equals("description")){
                connection.setDescription(value);
            } else if(element.getKey().equals("type")){
                connection.setType(EServerType.create(value));
                connection.setTypeName(connection.getType().getType());
                connection.setTypeUrl(Utils.getIconTypeImageUrl(connection.getType().getType()));
                
            } else if(element.getKey().equals("url")){
                connection.setUrl(value);
            } else if(element.getKey().equals("username")){
                connection.setUsername(value);
            } else if(element.getKey().equals("password")){
                connection.setPassword(value);
            }
        }
        
        connectionDAO.updateConnection(connection);
        
        final ObjectNode result = mapper.createObjectNode();
        result.put("test", "success");
        
        return result;
    }
    


    @DELETE
    @Path("/{id}")
    @Produces(TYPE_JSON)
    public Response deleteConnection(@PathParam("id") final long id) {
        connectionDAO.deleteConnection(id);
        return Response.status(Status.OK).entity("").build();
    }

}
