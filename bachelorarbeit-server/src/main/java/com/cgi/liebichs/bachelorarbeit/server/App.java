package com.cgi.liebichs.bachelorarbeit.server;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javassist.tools.reflect.CannotCreateException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.cgi.liebichs.bachelorarbeit.server.dao.ImportDAO;
import com.cgi.liebichs.bachelorarbeit.server.domain.ClientIsNotAvailableException;
import com.cgi.liebichs.bachelorarbeit.server.domain.Component;
import com.cgi.liebichs.bachelorarbeit.server.domain.ConfluencePage;
import com.cgi.liebichs.bachelorarbeit.server.domain.Connection;
import com.cgi.liebichs.bachelorarbeit.server.domain.EServerType;
import com.cgi.liebichs.bachelorarbeit.server.domain.JIRAIssue;
import com.cgi.liebichs.bachelorarbeit.server.service.ConnectionManagementService;
import com.cgi.liebichs.bachelorarbeit.server.service.ImportService;
import com.cgi.liebichs.bachelorarbeit.server.utils.HibernateUtil;
import com.cgi.liebichs.bachelorarbeit.server.utils.ResourceUtils;

public class App {

    /**
     * @param args
     * @throws ClientIsNotAvailableException
     * @throws JSONException
     * @throws IOException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws CannotCreateException
     */
    public static void main(String[] args) throws IOException, JSONException, ClientIsNotAvailableException,
        IllegalArgumentException, IllegalAccessException, CannotCreateException {

        final ImportService is = new ImportService();
        // final ObjectNode res = is.postImportJIRAIssues(2, "", "", "", "",
        // "DAIMEMB");

        String[] sa = new String[] { "25563900", "25575700", "25575686", "25563908", "25568685", "25568693",
            "25574037", "25568691", "25574034", "25568687", "25568689", "25568697", "25578657", "25578660", "25563910",
            "40665366", "25563913", "25582946", "25572024", "25587379", "25588244", "26912640", "25589879", "40011031",
            "40665105", "24915097", "25561131", "25561134", "25561180", "25561185", "25561136", "26411995", "25561151",
            "25561155", "25561158", "25561588", "25561166", "25561169", "25561175", "25561172", "25562930", "25562932",
            "25562933", "25562890", "25563059", "25563057", "25564092", "25564988", "25566685", "25566690", "25569009",
            "25574326", "25574328", "25576517", "25576861", "25577433", "25578027", "25579818", "25580544", "25580567",
            "25580727", "25580919", "25580975", "25582154", "25582157", "25582956", "25587747", "25589642", "25589820",
            "25590124", "28248508", "26912646", "26913049", "40665212", "24913979", "25579432", "24915568", "24915835",
            "25570486", "24914925", "25577815", "25569851", "24913699", "24913946", "24915605", "26415633", "25582843",
            "25570384", "25570828", "25583319", "40666066", "25590233", "24915837", "25568415", "25565670", "25568664",
            "25588366", "25578090", "25570177", "25576477", "25559127", "25562385", "25559268", "25564161", "25574126",
            "24913038", "25567833", "25573947", "25589956", "25583087", "24915839", "25580210", "24912821", "25567766",
            "25575885", "25582343", "24914345", "24915841", "25568413", "25565007", "28253317", "25568417", "24916134",
            "24916199", "25581913", "25591284", "25569624", "24915827", "24915555", "25574056", "25574058", "25588249",
            "25570168", "25571278", "25571284", "25571280", "26904088", "25589170", "25590015", "25571282" };

        

        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session session = sf.openSession();

        Transaction tx = session.beginTransaction();

        final String[] confPages2import = new String[]{"24912821","24913038","24913699","24913946","24913979"};
        final String[] jiraSpec2import = new String[]{"DAIMEMB-1803","DAIMEMB-494","DAIMEMB-1739","DAIMEMB-903","DAIMEMB-163","DAIMEMB-1268"};
        final String[] jiraBug2import = new String[]{"DAIMEMB-1947","DAIMEMB-701","DAIMEMB-985","DAIMEMB-986","DAIMEMB-2025","DAIMEMB-1426","DAIMEMB-1444"};
        
        final List<String> confSpec = Arrays.asList(confPages2import);
        final List<String> jiraSpec = Arrays.asList(jiraSpec2import);
        final List<String> jiraBug = Arrays.asList(jiraBug2import);
        
        final Map<String, List<String>> confSpecList = new HashMap<String, List<String>>();
        final Map<String, List<String>> jiraSpecList = new HashMap<String, List<String>>();
        final Map<String, List<String>> jiraBugList = new HashMap<String, List<String>>();
        
        confSpecList.put("DAIMEMB", confSpec);
        jiraSpecList.put("DAIMEMB", jiraSpec);
        jiraBugList.put("DAIMEMB", jiraBug);
        
        Component comp = new Component();
        comp.setName("Foo Bar");
        comp.setDescription("Foo Bar description!");
        comp.setType("Test");
        
        
        
        final ImportDAO importDao = new ImportDAO();
        List<JIRAIssue> importedJIRASpecs = importDao.getJIRAIssues(jiraSpecList);
        List<JIRAIssue> importedJIRABugs = importDao.getJIRAIssues(jiraBugList);
        List<ConfluencePage> importedConfSpecs = importDao.getConfluencePages(confSpecList);
        
        comp.getConfluenceSpecs().addAll(importedConfSpecs);
        comp.getJiraBugIssues().addAll(importedJIRASpecs);
        comp.getJiraSpecIssues().addAll(importedJIRABugs);
        
        session.save(comp);
        
        tx.commit();
        session.close();
        
//        System.out.println(session.createQuery("select count(*) from ConfluencePage").uniqueResult());
        //
        // JIRAIssue issue = new JIRAIssue();
        // issue.setIssueKey("TEST-1234");
        // issue.setSummary("Test");
        // session.save(issue);

        // // Query q =
        // session.createQuery("from Component WHERE type IN ('Test Component Test', 'Test Component')");
        // // q.list();
        //
        // ComponentManagementService man = new ComponentManagementService();
        //
        //
        //
        // // for (int i = 5; i < 10; i++) {
        // // Component comp = new Component();
        // // comp.setName("Test " + i);
        // // comp.setDescription("This is a component Description " + i);
        // // comp.setType("Test Component Test");
        // // comp.setConfluenceSpec(new ArrayList<ConfluencePage>());
        // // comp.setJiraBugIssues(new ArrayList<JIRAIssue>());
        // // comp.setJiraSpecIssue(new ArrayList<JIRAIssue>());
        // // comp.setSpiraTestCases(new ArrayList<SpiraTest_TC>());
        // //
        // // man.postComponent(comp);
        // // }
        // // for(JsonNode node: man.getComponentsMeta()){
        // // System.out.println(node.toString());
        // // }
        //
        // ImportDAO dao = new ImportDAO();
        //
        // Map<String, List<String>> map = new HashMap<String, List<String>>();
        // List<String> list = new ArrayList<String>();
        // list.add("DAIMEMB-1968");
        // list.add("DAIMEMB-1961");
        // list.add("DAIMEMB-1960");
        //
        //
        // map.put("DAIMEMB", list);
        //
        // List<JIRAIssue> issues = dao.getJIRAIssues(map);
        //
        // System.out.println(issues);
        // session.close();

    }

    public static void testDatabaseConnection() throws JsonParseException, JsonMappingException, IOException {
        ConnectionManagementService cms = new ConnectionManagementService();
        ObjectMapper mapper = new ObjectMapper();

        final Connection con = new Connection();
        con.setName("Test Connection");
        con.setKey("test-connection");
        con.setDescription("Description");
        con.setIconText("JIRA");
        con.setIconImageUrl("imageUrl/image.png");
        con.setUsername("username");
        con.setPassword("secret");
        con.setType(EServerType.JIRA);
        con.setTypeUrl("type/url");
        con.setUrl("http://127.0.0.1/");
        // final String json = mapper.writeValueAsString(con);
        // System.out.println(json);

        // cms.addConnection(con);

        ArrayNode list = cms.getConnections();
        System.out.println(list.toString());
    }

    public static String convertLength(final String s) {
        System.out.println(s.length());
        int a = 0;
        int length = 0;
        int length2 = 0;
        int tmp = s.length();
        while (tmp / 1024 > 0) {
            int factor = tmp / 1024;
            int rest = tmp - factor * 1024;
            a++;
            tmp = factor;
            length = factor;
            length2 = rest;
            System.out.print(factor + " # ");
            System.out.print(rest + " # ");
            System.out.println(a);
        }

        switch (a) {
        case 0:
            return length + "." + length2 + "B";
        case 1:
            return length + "." + length2 + "KB";
        case 2:
            return length + "." + length2 + "MB";
        case 3:
            return length + "." + length2 + "TB";
        default:
            return length + "";
        }
    }

    public static JSONObject getJSONObj() throws JSONException, IOException {
        final BufferedReader br = new BufferedReader(new FileReader(ResourceUtils.getJSONFile("daimemb.json")));

        final StringBuilder sb = new StringBuilder();
        String input = "";
        while ((input = br.readLine()) != null) {
            sb.append(input.trim());
        }

        br.close();

        return new JSONObject(sb.toString());
    }

}