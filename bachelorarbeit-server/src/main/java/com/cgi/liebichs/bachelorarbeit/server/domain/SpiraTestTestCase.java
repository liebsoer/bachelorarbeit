package com.cgi.liebichs.bachelorarbeit.server.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class SpiraTestTestCase implements Serializable{
    private static final long serialVersionUID = 1824856143339545844L;
    
    @Column @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private int testCaseId;
    private int projectId;
    private String name;
    private String executionDate;
    private String creationDate;
    private String concurrencyDate;
    private String lastUpdateDate;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "testCaseId")
    private List<SpiraTestTestStep> testSteps;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tetsCaseId")
    private List<SpiraTestIncident> incidents;
    
    public SpiraTestTestCase() {
        
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getTestCaseId() {
        return this.testCaseId;
    }

    public void setTestCaseId(int testCaseId) {
        this.testCaseId = testCaseId;
    }

    public int getProjectId() {
        return this.projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExecutionDate() {
        return this.executionDate;
    }

    public void setExecutionDate(String executinoDate) {
        this.executionDate = executinoDate;
    }

    public String getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getConcurrencyDate() {
        return this.concurrencyDate;
    }

    public void setConcurrencyDate(String concurrencyDate) {
        this.concurrencyDate = concurrencyDate;
    }

    public String getLastUpdateDate() {
        return this.lastUpdateDate;
    }

    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public List<SpiraTestTestStep> getTestSteps() {
        return this.testSteps;
    }

    public void setTestSteps(List<SpiraTestTestStep> testSteps) {
        this.testSteps = testSteps;
    }

//    public List<SpiraTestIncident> getIncidents() {
//        return this.incidents;
//    }
//
//    public void setIncidents(List<SpiraTestIncident> incidents) {
//        this.incidents = incidents;
//    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.creationDate == null) ? 0 : this.creationDate.hashCode());
        result = prime * result + ((this.executionDate == null) ? 0 : this.executionDate.hashCode());
        result = prime * result + ((this.concurrencyDate == null) ? 0 : this.concurrencyDate.hashCode());
        result = prime * result + (int) (this.id ^ (this.id >>> 32));
//        result = prime * result + ((this.incidents == null) ? 0 : this.incidents.hashCode());
        result = prime * result + ((this.lastUpdateDate == null) ? 0 : this.lastUpdateDate.hashCode());
        result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
        result = prime * result + this.projectId;
        result = prime * result + this.testCaseId;
        result = prime * result + ((this.testSteps == null) ? 0 : this.testSteps.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SpiraTestTestCase other = (SpiraTestTestCase) obj;
        if (this.creationDate == null) {
            if (other.creationDate != null)
                return false;
        } else if (!this.creationDate.equals(other.creationDate))
            return false;
        if (this.executionDate == null) {
            if (other.executionDate != null)
                return false;
        } else if (!this.executionDate.equals(other.executionDate))
            return false;
        if (this.concurrencyDate == null) {
            if (other.concurrencyDate != null)
                return false;
        } else if (!this.concurrencyDate.equals(other.concurrencyDate))
            return false;
        if (this.id != other.id)
            return false;
        if (this.lastUpdateDate == null) {
            if (other.lastUpdateDate != null)
                return false;
        } else if (!this.lastUpdateDate.equals(other.lastUpdateDate))
            return false;
        if (this.name == null) {
            if (other.name != null)
                return false;
        } else if (!this.name.equals(other.name))
            return false;
        if (this.projectId != other.projectId)
            return false;
        if (this.testCaseId != other.testCaseId)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "SpiraTestTestCase [id=" + this.id + ", testCaseId=" + this.testCaseId + ", projectId=" + this.projectId
            + ", name=" + this.name + ", executinoDate=" + this.executionDate + ", concurrencyDate=" + this.concurrencyDate  + ", creationDate=" + this.creationDate
            + ", lastUpdateDate=" + this.lastUpdateDate + ", testSteps=" + this.testSteps + ", incidents="
//            + this.incidents
            + "]";
    }

    
}
