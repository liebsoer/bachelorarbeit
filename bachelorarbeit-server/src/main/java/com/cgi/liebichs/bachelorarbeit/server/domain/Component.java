package com.cgi.liebichs.bachelorarbeit.server.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Component implements Serializable{
    private static final long serialVersionUID = -6352711135812037390L;

    @Column(name="component_id") @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private String description;
    private String type;
    
    @ElementCollection(targetClass = JIRAIssue.class)
    @JoinTable(name = "Component_JIRABugIssues",joinColumns = @JoinColumn(name="component_id"))
    @org.hibernate.annotations.IndexColumn(name = "POSITION", base = 1)
    @Column(name = "JIRABugIssue", nullable = false)
    private Set<JIRAIssue> jiraBugIssues;
    
    @ElementCollection(targetClass = SpiraTest_TC.class)
    @JoinTable(name = "Component_SpiraTestCases",joinColumns = @JoinColumn(name="component_id"))
    @org.hibernate.annotations.IndexColumn(name = "POSITION", base = 1)
    @Column(name = "SpiraTestCase", nullable = false)
    private Set<SpiraTest_TC> spiraTestCases;
    
    @ElementCollection(targetClass = JIRAIssue.class)
    @JoinTable(name = "Component_JIRASpecIssues",joinColumns = @JoinColumn(name="component_id"))
    @org.hibernate.annotations.IndexColumn(name = "POSITION", base = 1)
    @Column(name = "JIRASpecIssue", nullable = false)
    private Set<JIRAIssue> jiraSpecIssues;
    
    @ElementCollection(targetClass = ConfluencePage.class)
    @JoinTable(name = "Component_ConfluenceSpec",joinColumns = @JoinColumn(name="component_id"))
    @org.hibernate.annotations.IndexColumn(name = "POSITION", base = 1)
    @Column(name = "ConfluenceSpec", nullable = false)
    private Set<ConfluencePage> confluenceSpecs;
    
    @ElementCollection(targetClass = Project.class)
    @JoinTable(name = "Component_Project",joinColumns = @JoinColumn(name="component_id"))
    @org.hibernate.annotations.IndexColumn(name = "POSITION", base = 1)
    @Column(name = "Project", nullable = false)
    private Set<Project> projects;
    
    public Component(){
        this.jiraBugIssues = new HashSet<JIRAIssue>();
        this.spiraTestCases = new HashSet<SpiraTest_TC>();
        this.jiraSpecIssues = new HashSet<JIRAIssue>();
        this.confluenceSpecs = new HashSet<ConfluencePage>();
        this.projects = new HashSet<Project>();
    }
    
    public long getId() {
        return this.id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return this.description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getType() {
        return this.type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public Set<JIRAIssue> getJiraBugIssues() {
        return this.jiraBugIssues;
    }
    public void setJiraBugIssues(Set<JIRAIssue> jiraBugIssues) {
        this.jiraBugIssues = jiraBugIssues;
    }
    public Set<SpiraTest_TC> getSpiraTestCases() {
        return this.spiraTestCases;
    }
    public void setSpiraTestCases(Set<SpiraTest_TC> spiraTestCases) {
        this.spiraTestCases = spiraTestCases;
    }
    public Set<JIRAIssue> getJiraSpecIssues() {
        return this.jiraSpecIssues;
    }
    public void setJiraSpecIssues(Set<JIRAIssue> jiraSpecIssues) {
        this.jiraSpecIssues = jiraSpecIssues;
    }
    public Set<ConfluencePage> getConfluenceSpecs() {
        return this.confluenceSpecs;
    }
    public void setConfluenceSpecs(Set<ConfluencePage> confluenceSpecs) {
        this.confluenceSpecs = confluenceSpecs;
    }
    public Set<Project> getProjects() {
        return this.projects;
    }
    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.confluenceSpecs == null) ? 0 : this.confluenceSpecs.hashCode());
        result = prime * result + ((this.description == null) ? 0 : this.description.hashCode());
        result = prime * result + (int) (this.id ^ (this.id >>> 32));
        result = prime * result + ((this.jiraBugIssues == null) ? 0 : this.jiraBugIssues.hashCode());
        result = prime * result + ((this.jiraSpecIssues == null) ? 0 : this.jiraSpecIssues.hashCode());
        result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
        result = prime * result + ((this.projects == null) ? 0 : this.projects.hashCode());
        result = prime * result + ((this.spiraTestCases == null) ? 0 : this.spiraTestCases.hashCode());
        result = prime * result + ((this.type == null) ? 0 : this.type.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Component other = (Component) obj;
        if (this.confluenceSpecs == null) {
            if (other.confluenceSpecs != null)
                return false;
        } else if (!this.confluenceSpecs.equals(other.confluenceSpecs))
            return false;
        if (this.description == null) {
            if (other.description != null)
                return false;
        } else if (!this.description.equals(other.description))
            return false;
        if (this.id != other.id)
            return false;
        if (this.jiraBugIssues == null) {
            if (other.jiraBugIssues != null)
                return false;
        } else if (!this.jiraBugIssues.equals(other.jiraBugIssues))
            return false;
        if (this.jiraSpecIssues == null) {
            if (other.jiraSpecIssues != null)
                return false;
        } else if (!this.jiraSpecIssues.equals(other.jiraSpecIssues))
            return false;
        if (this.name == null) {
            if (other.name != null)
                return false;
        } else if (!this.name.equals(other.name))
            return false;
        if (this.projects == null) {
            if (other.projects != null)
                return false;
        } else if (!this.projects.equals(other.projects))
            return false;
        if (this.spiraTestCases == null) {
            if (other.spiraTestCases != null)
                return false;
        } else if (!this.spiraTestCases.equals(other.spiraTestCases))
            return false;
        if (this.type == null) {
            if (other.type != null)
                return false;
        } else if (!this.type.equals(other.type))
            return false;
        return true;
    }
    @Override
    public String toString() {
        return "Component ["
            + "id=" + this.id 
            + ", name=" + this.name 
            + ", description=" + this.description 
            + ", type=" + this.type 
            + ", jiraBugIssues=" + this.jiraBugIssues 
            + ", spiraTestCases=" + this.spiraTestCases
            + ", jiraSpecIssue=" + this.jiraSpecIssues 
            + ", confluenceSpec=" + this.confluenceSpecs 
            + ", projects=" + this.projects 
            + "]";
    }
    
    
}
