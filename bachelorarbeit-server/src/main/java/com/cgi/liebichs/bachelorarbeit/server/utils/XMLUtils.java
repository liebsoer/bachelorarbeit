package com.cgi.liebichs.bachelorarbeit.server.utils;

import com.cgi.liebichs.bachelorarbeit.server.dao.ImportDAO;
import com.cgi.liebichs.bachelorarbeit.server.domain.SpiraTestTestCase;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class XMLUtils {

    private static final int maxLevel = 3;
    private final ObjectMapper mapper;

    public static void main(String... strings) throws SAXException, IOException, ParserConfigurationException {
        InputStream stream = new FileInputStream(
            new File(
                "/home/liebsoer/workspace/bachelorarbeit/bachelorarbeit-server/src/main/resources/resources/Generated.aspx.format.xml"));
        XMLUtils utils = new XMLUtils(stream);
    }

    public XMLUtils(final InputStream stream) throws SAXException, IOException, ParserConfigurationException {
        this.mapper = new ObjectMapper().configure(Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true).configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        
        final ImportDAO dao = new ImportDAO();
        
        final DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        final DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        final Document doc = dBuilder.parse(stream);

        doc.getDocumentElement().normalize();
        final ArrayNode projects = extractProjectData(doc.getElementsByTagName("ProjectData").item(0).getChildNodes());

        final ArrayNode testCases = extractTestCaseData(doc.getElementsByTagName("TestCaseData").item(0)
            .getChildNodes());

        final Set<String> incidentIds = new HashSet<String>();
        for (int i = 0; i < testCases.size(); i++) {
            final ArrayNode incidents = (ArrayNode) testCases.get(i).get("incidents");
            for (int j = 0; incidents != null && j < incidents.size(); j++) {
                incidentIds.add(incidents.get(j).get("incidentId").asText());
            }
        }

        final ArrayNode incidents = extractIncidentData(doc.getElementsByTagName("IncidentData").item(0)
            .getChildNodes(), incidentIds);

        final ArrayNode an = matchTestCasesWithIncidents(testCases, incidents);
        
        final ArrayList<SpiraTestTestCase> list = new ArrayList<SpiraTestTestCase>();
        for(final JsonNode jn: an){
            list.add(mapper.readValue(jn, SpiraTestTestCase.class));
        }
         
        dao.saveImportedTestCases(list);
        
        System.out.println();
    }

    private ArrayNode matchTestCasesWithIncidents(final ArrayNode testCases, final ArrayNode incidents) {
        final HashMap<String, JsonNode> incidentMap = new HashMap<String, JsonNode>();
        for (final JsonNode node : incidents) {
            incidentMap.put(node.get("incidentId").asText(), node);
        }

        for (final JsonNode node : testCases) {
            if (node.has("incidents")) {
                final ArrayNode an = (ArrayNode) node.get("incidents");
                for (int i = 0; i < an.size(); i++) {
                    final String incId = an.get(i).get("incidentId").asText();
                    if (incidentMap.containsKey(incId)) {
                        an.remove(i);
                        an.add(incidentMap.get(incId));
                    }
                }
            }
        }
        return testCases;
    }

    private ArrayNode extractIncidentData(final NodeList incidentData, final Set<String> incidentIds) {

        final ArrayNode result = mapper.createArrayNode();

        for (int i = 0; i < incidentData.getLength(); i++) {
            if (incidentData.item(i).getNodeType() != Node.ELEMENT_NODE)
                continue;
            final Element incidentNode = (Element) incidentData.item(i);

            if (!incidentIds.contains(incidentNode.getElementsByTagName("IncidentId").item(0).getTextContent())) {
                continue;
            }

            final ObjectNode incident = mapper.createObjectNode();
            incident.put("incidentId", incidentNode.getElementsByTagName("IncidentId").item(0).getTextContent());
            incident.put("projectId", incidentNode.getElementsByTagName("ProjectId").item(0).getTextContent());
            incident.put("name", incidentNode.getElementsByTagName("Name").item(0).getTextContent());
            incident.put("description", incidentNode.getElementsByTagName("Description").item(0).getTextContent());

            final NodeList attachments = incidentNode.getElementsByTagName("Attachments");
            for (int j = 0; j < attachments.getLength(); j++) {
                if (incidentData.item(i).getNodeType() != Node.ELEMENT_NODE)
                    continue;

                final Element attachment = (Element) attachments.item(j);

                if (attachment.getElementsByTagName("AttachmentTypeName").item(0).getTextContent().toLowerCase()
                    .equals("url")
                    && attachment.getElementsByTagName("Filename").item(0).getTextContent().contains("bugtracking")) {
                    final String[] urlParts = attachment.getElementsByTagName("Filename").item(0).getTextContent()
                        .split("/");
                    incident.put("jiraIssue", urlParts[urlParts.length - 1]);
                    break;
                }
            }

            result.add(incident);

        }

        return result;
    }

    private ArrayNode extractTestCaseData(final NodeList testCaseData) {
        final ArrayNode result = mapper.createArrayNode();

        for (int i = 0; i < testCaseData.getLength(); i++) {
            if (testCaseData.item(i).getNodeType() != Node.ELEMENT_NODE)
                continue;

            final Element testCaseNode = (Element) testCaseData.item(i);

            final ObjectNode testCase = mapper.createObjectNode();

            testCase.put("testCaseId", testCaseNode.getElementsByTagName("TestCaseId").item(0).getTextContent());
            testCase.put("projectId", testCaseNode.getElementsByTagName("ProjectId").item(0).getTextContent());
            testCase.put("name", testCaseNode.getElementsByTagName("Name").item(0).getTextContent());
            testCase.put("executionDate", testCaseNode.getElementsByTagName("ExecutionDate").item(0).getTextContent());
            testCase.put("creationDate", testCaseNode.getElementsByTagName("CreationDate").item(0).getTextContent());
            testCase
                .put("lastUpdateDate", testCaseNode.getElementsByTagName("LastUpdateDate").item(0).getTextContent());
            testCase.put("concurrencyDate", testCaseNode.getElementsByTagName("ConcurrencyDate").item(0)
                .getTextContent());

            if (testCaseNode.getElementsByTagName("TestSteps").item(0).hasChildNodes()) {
                if (testCaseData.item(i).getNodeType() != Node.ELEMENT_NODE)
                    continue;

                final ArrayNode testSteps = mapper.createArrayNode();

                final NodeList testStepsNode = testCaseNode.getElementsByTagName("TestSteps").item(0).getChildNodes();
                for (int j = 0; j < testStepsNode.getLength(); j++) {
                    if (testStepsNode.item(j).getNodeType() != Node.ELEMENT_NODE)
                        continue;

                    final Element testStepNode = (Element) testStepsNode.item(j);
                    final ObjectNode testStep = mapper.createObjectNode();

                    testStep
                        .put("testStepId", testStepNode.getElementsByTagName("TestStepId").item(0).getTextContent());
                    testStep
                        .put("testCaseId", testStepNode.getElementsByTagName("TestCaseId").item(0).getTextContent());
                    testStep.put("description", testStepNode.getElementsByTagName("Description").item(0)
                        .getTextContent());
                    testStep.put("position", testStepNode.getElementsByTagName("Position").item(0).getTextContent());

                    if (testStepNode.hasAttribute("ExpectedResult")) {
                        testStep.put("expectedResult", testStepNode.getElementsByTagName("ExpectedResult").item(0)
                            .getTextContent());
                    }

                    testSteps.add(testStep);
                }

                testCase.put("testSteps", testSteps);
            }

            if (testCaseNode.getElementsByTagName("Incidents").item(0).hasChildNodes()) {
                if (testCaseData.item(i).getNodeType() != Node.ELEMENT_NODE)
                    continue;

                final ArrayNode incidents = mapper.createArrayNode();

                final NodeList incidentsNode = testCaseNode.getElementsByTagName("Incidents").item(0).getChildNodes();
                for (int j = 0; j < incidentsNode.getLength(); j++) {
                    if (incidentsNode.item(j).getNodeType() != Node.ELEMENT_NODE)
                        continue;

                    final Element incidentNode = (Element) incidentsNode.item(j);

                    final ObjectNode incident = mapper.createObjectNode();
                    incident
                        .put("incidentId", incidentNode.getElementsByTagName("IncidentId").item(0).getTextContent());
                    incident.put("projectId", incidentNode.getElementsByTagName("ProjectId").item(0).getTextContent());
                    incident.put("name", incidentNode.getElementsByTagName("Name").item(0).getTextContent());
                    incident.put("description", incidentNode.getElementsByTagName("Description").item(0)
                        .getTextContent());

                    incidents.add(incident);
                }

                testCase.put("incidents", incidents);
            }

            result.add(testCase);
        }

        return result;
    }

    private ArrayNode extractProjectData(final NodeList projectData) {
        final ArrayNode projects = this.mapper.createArrayNode();
        for (int i = 0; i < projectData.getLength(); i++) {
            if (projectData.item(i).getNodeType() != Node.ELEMENT_NODE)
                continue;

            final Element projectNode = (Element) projectData.item(i);
            final ObjectNode project = mapper.createObjectNode();
            project.put("projectId", projectNode.getElementsByTagName("ProjectId").item(0).getTextContent());
            project.put("projectGroupId", projectNode.getElementsByTagName("ProjectGroupId").item(0).getTextContent());
            project.put("name", projectNode.getElementsByTagName("Name").item(0).getTextContent());
            project.put("description", projectNode.getElementsByTagName("Description").item(0).getTextContent());
            project.put("projectGroupName", projectNode.getElementsByTagName("ProjectGroupName").item(0)
                .getTextContent());

            projects.add(project);
        }
        return projects;
    }

    public void printNodes(NodeList nl, int level) {
        if (level > maxLevel)
            return;

        for (int i = 0; i < nl.getLength(); i++) {
            Node n = nl.item(i);
            for (int j = 0; j < level; j++) {
                System.out.print(".");
            }

            System.out.print(n.getNodeName() + ", " + getNameForNodeType(n.getNodeType()) + ", hasChilds: "
                + n.hasChildNodes());

            if (n.getNodeType() == Node.TEXT_NODE) {
                System.out.print("; " + n.getTextContent().trim());
            }

            if (n.hasAttributes()) {
                System.out.print("; Attributes: [");
                NamedNodeMap attrs = n.getAttributes();
                for (int j = 0; j < attrs.getLength(); j++) {
                    Node attr = attrs.item(j);
                    System.out.print(attr.getNodeName() + ": " + attr.getNodeValue());
                    if (j < attrs.getLength() - 1) {
                        System.out.print(";");
                    }
                }
            }

            System.out.println();
            if (n.hasChildNodes()) {
                printNodes(n.getChildNodes(), (level + 1));
            }
        }
    }

    public String getNameForNodeType(final short type) {
        switch (type) {
        case Node.ATTRIBUTE_NODE:
            return "Attribute Node";
        case Node.CDATA_SECTION_NODE:
            return "CDATA Section Node";
        case Node.COMMENT_NODE:
            return "Comment Node";
        case Node.DOCUMENT_FRAGMENT_NODE:
            return "Document Fragment Node";
        case Node.DOCUMENT_NODE:
            return "Document Node";
        case Node.DOCUMENT_TYPE_NODE:
            return "Document Type Node";
        case Node.ELEMENT_NODE:
            return "Element Node";
        case Node.ENTITY_NODE:
            return "Entity Node";
        case Node.ENTITY_REFERENCE_NODE:
            return "Entity Reference Node";
        case Node.NOTATION_NODE:
            return "Notation Node";
        case Node.PROCESSING_INSTRUCTION_NODE:
            return "Processing Instruction Node";
        case Node.TEXT_NODE:
            return "Text Node";
        default:
            return "No Node Type";
        }
    }

}
