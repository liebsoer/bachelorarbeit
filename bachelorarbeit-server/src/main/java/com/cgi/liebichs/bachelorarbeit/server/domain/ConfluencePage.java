package com.cgi.liebichs.bachelorarbeit.server.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class ConfluencePage implements Serializable {
    private static final long serialVersionUID = -5033964729135820832L;
    
    @Column @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column
    private String title;
    @Column
    private String url;
    @Column
    private int attachments;
    @Column
    private int childPages;
    @Column
    private int comments;
    @Column
    private String projectName;
    @Column
    private String projectKey;
    @Column
    private String parent;
    @Column(unique = true)
    private String pageId;
    
    public ConfluencePage() {}

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getAttachments() {
        return this.attachments;
    }

    public void setAttachments(int attachments) {
        this.attachments = attachments;
    }

    public int getChildPages() {
        return this.childPages;
    }

    public void setChildPages(int childPages) {
        this.childPages = childPages;
    }

    public int getComments() {
        return this.comments;
    }

    public void setComments(int comments) {
        this.comments = comments;
    }

    public String getProjectName() {
        return this.projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectKey() {
        return this.projectKey;
    }

    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
    }

    public String getParent() {
        return this.parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getPageId() {
        return this.pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (this.id ^ (this.id >>> 32));
        result = prime * result + ((this.pageId == null) ? 0 : this.pageId.hashCode());
        result = prime * result + ((this.parent == null) ? 0 : this.parent.hashCode());
        result = prime * result + ((this.projectKey == null) ? 0 : this.projectKey.hashCode());
        result = prime * result + ((this.projectName == null) ? 0 : this.projectName.hashCode());
        result = prime * result + ((this.title == null) ? 0 : this.title.hashCode());
        result = prime * result + ((this.url == null) ? 0 : this.url.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ConfluencePage other = (ConfluencePage) obj;
        if (this.id != other.id)
            return false;
        if (this.pageId == null) {
            if (other.pageId != null)
                return false;
        } else if (!this.pageId.equals(other.pageId))
            return false;
        if (this.parent == null) {
            if (other.parent != null)
                return false;
        } else if (!this.parent.equals(other.parent))
            return false;
        if (this.projectKey == null) {
            if (other.projectKey != null)
                return false;
        } else if (!this.projectKey.equals(other.projectKey))
            return false;
        if (this.projectName == null) {
            if (other.projectName != null)
                return false;
        } else if (!this.projectName.equals(other.projectName))
            return false;
        if (this.title == null) {
            if (other.title != null)
                return false;
        } else if (!this.title.equals(other.title))
            return false;
        if (this.url == null) {
            if (other.url != null)
                return false;
        } else if (!this.url.equals(other.url))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ConfluencePage [id=" + this.id + ", title=" + this.title + ", url=" + this.url + ", attachments="
            + this.attachments + ", childPages=" + this.childPages + ", comments=" + this.comments + ", projectName="
            + this.projectName + ", projectKey=" + this.projectKey + ", parent=" + this.parent + ", pageId="
            + this.pageId + "]";
    }
    
    

}
