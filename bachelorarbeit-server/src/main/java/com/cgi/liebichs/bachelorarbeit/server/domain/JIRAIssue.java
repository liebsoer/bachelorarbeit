package com.cgi.liebichs.bachelorarbeit.server.domain;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Entity
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class JIRAIssue implements Serializable{
    
    private static final long serialVersionUID = 3318075340122923968L;
    
    @Column @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column
    private String issueKey;
    @Column
    private String progress;
    @Column
    private String summary;
    @Column
    private String issuetype;
    @Column
    private String timespent;
    @Column
    private String reporter;
    @Column
    private String updated;
    @Column
    private String created;
    @Column
    private String priority;
    @Column(columnDefinition="TEXT") @Lob
    private String description;
    @Column
    private String status;
    @Column
    private String labels;
    @Column
    private String workratio;
    @Column
    private String project;
    @Column
    private String environment;
    @Column
    private String aggregateprogress;
    @Column
    private String lastVieweed;
    @Column
    private String components;
    @Column
    private String resolution;
    @Column
    private String aggregatetimeoriginalestimate;
    @Column
    private String aggregatetimeestimate;
    @Column
    private String timeestimate;
    @Column
    private int changes;
    
    public long getId() {
        return this.id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getIssueKey() {
        return this.issueKey;
    }
    public void setIssueKey(String key) {
        this.issueKey = key;
    }
    public String getProgress() {
        return this.progress;
    }
    public void setProgress(String progress) {
        this.progress = progress;
    }
    public String getSummary() {
        return this.summary;
    }
    public void setSummary(String summary) {
        this.summary = summary;
    }
    public String getIssuetype() {
        return this.issuetype;
    }
    public void setIssuetype(String issuetype) {
        this.issuetype = issuetype;
    }
    public String getTimespent() {
        return this.timespent;
    }
    public void setTimespent(String timespent) {
        this.timespent = timespent;
    }
    public String getReporter() {
        return this.reporter;
    }
    public void setReporter(String reporter) {
        this.reporter = reporter;
    }
    public String getUpdated() {
        return this.updated;
    }
    public void setUpdated(String updated) {
        this.updated = updated;
    }
    public String getCreated() {
        return this.created;
    }
    public void setCreated(String created) {
        this.created = created;
    }
    public String getPriority() {
        return this.priority;
    }
    public void setPriority(String priority) {
        this.priority = priority;
    }
    public String getDescription() {
        return this.description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getStatus() {
        return this.status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getLabels() {
        return this.labels;
    }
    public void setLabels(String labels) {
        this.labels = labels;
    }
    public String getWorkratio() {
        return this.workratio;
    }
    public void setWorkratio(String workratio) {
        this.workratio = workratio;
    }
    public String getProject() {
        return this.project;
    }
    public void setProject(String project) {
        this.project = project;
    }
    public String getEnvironment() {
        return this.environment;
    }
    public void setEnvironment(String environment) {
        this.environment = environment;
    }
    public String getAggregateprogress() {
        return this.aggregateprogress;
    }
    public void setAggregateprogress(String aggregateprogress) {
        this.aggregateprogress = aggregateprogress;
    }
    public String getLastVieweed() {
        return this.lastVieweed;
    }
    public void setLastVieweed(String lastVieweed) {
        this.lastVieweed = lastVieweed;
    }
    public String getComponents() {
        return this.components;
    }
    public void setComponents(String components) {
        this.components = components;
    }
    public String getResolution() {
        return this.resolution;
    }
    public void setResolution(String resolution) {
        this.resolution = resolution;
    }
    public String getAggregatetimeoriginalestimate() {
        return this.aggregatetimeoriginalestimate;
    }
    public void setAggregatetimeoriginalestimate(String aggregatetimeoriginalestimate) {
        this.aggregatetimeoriginalestimate = aggregatetimeoriginalestimate;
    }
    public String getAggregatetimeestimate() {
        return this.aggregatetimeestimate;
    }
    public void setAggregatetimeestimate(String aggregatetimeestimate) {
        this.aggregatetimeestimate = aggregatetimeestimate;
    }
    public String getTimeestimate() {
        return this.timeestimate;
    }
    public void setTimeestimate(String timeestimate) {
        this.timeestimate = timeestimate;
    }
    public int getChanges() {
        return changes;
    }
    public void setChanges(int changes) {
        this.changes = changes;
    }
    @Override
    public String toString() {
        return "Issue [id=" + this.id + ", key=" + this.issueKey + ", progress=" + this.progress + ", summary="
            + this.summary + ", issuetype=" + this.issuetype + ", timespent=" + this.timespent + ", reporter="
            + this.reporter + ", updated=" + this.updated + ", created=" + this.created + ", priority=" + this.priority
            + ", description=" + this.description + ", status=" + this.status + ", labels=" + this.labels
            + ", workratio=" + this.workratio + ", project=" + this.project + ", environment=" + this.environment
            + ", aggregateprogress=" + this.aggregateprogress + ", lastVieweed=" + this.lastVieweed + ", components="
            + this.components + ", resolution=" + this.resolution + ", aggregatetimeoriginalestimate="
            + this.aggregatetimeoriginalestimate + ", aggregatetimeestimate=" + this.aggregatetimeestimate
            + ", timeestimate=" + this.timeestimate + ", changes= " + changes + "]";
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (this.id ^ (this.id >>> 32));
        result = prime * result + ((this.issueKey == null) ? 0 : this.issueKey.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        JIRAIssue other = (JIRAIssue) obj;
        if (this.id != other.id)
            return false;
        if (this.issueKey == null) {
            if (other.issueKey != null)
                return false;
        } else if (!this.issueKey.equals(other.issueKey))
            return false;
        return true;
    }

    

}
