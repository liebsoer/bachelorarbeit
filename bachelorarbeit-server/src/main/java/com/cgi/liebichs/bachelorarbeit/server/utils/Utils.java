package com.cgi.liebichs.bachelorarbeit.server.utils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javassist.tools.reflect.CannotCreateException;

import org.apache.commons.lang.ArrayUtils;

public class Utils {

    public static <T> String toJSONString(final T obj) throws IllegalArgumentException, IllegalAccessException,
        CannotCreateException {
        return toJSONString(obj, false);
    }

    public static <T> String toJSONString(final T obj, final boolean alsoStatics) throws IllegalArgumentException,
        IllegalAccessException, CannotCreateException {

        final Field[] fields = obj.getClass().getDeclaredFields();
        final StringBuilder sb = new StringBuilder();
        sb.append("{");

        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            field.setAccessible(true);
            sb.append("\"" + field.getName() + "\":");
            if (java.util.List.class.isInstance(field.get(obj))) {
                sb.append(extractListToJSON((List<?>) field.get(obj), alsoStatics));
            } else if (java.util.Map.class.isInstance(field.get(obj))) {
                @SuppressWarnings("unchecked")
                final String items = extractMapToJSON((Map<String, ?>) field.get(obj), alsoStatics);
                sb.append(items);
            } else if (field.getType() == String.class) {
                sb.append("\"" + ((String) field.get(obj)) + "\"");
            } else if (field.getType() == int.class) {
                sb.append(field.getInt(obj));
            } else if (field.getType() == double.class) {
                sb.append(field.getDouble(obj));
            } else if (field.getType() == boolean.class) {
                sb.append(field.getBoolean(obj));
            } else {
                sb.append(toJSONString(field.get(obj), alsoStatics));
            }

            if (i < fields.length - 1) {
                sb.append(",");
            }
            field.setAccessible(false);
        }
        sb.append("}");
        return sb.toString().replaceAll("\\r", "\\\\r").replaceAll("\\n", "\\\\n").replaceAll("\\t", "\\\\t");
    }

    public static String extractListToJSON(List<?> list, final boolean alsoStatics) throws IllegalArgumentException,
        IllegalAccessException, CannotCreateException {
        final Iterator<?> it = list.iterator();
        final StringBuilder sb = new StringBuilder();

        sb.append("[");
        while (it.hasNext()) {
            final Object item = it.next();
            sb.append(extractJSONItem(item, alsoStatics));

            if (it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("]");

        return sb.toString();
    }

    public static String extractJSONItem(final Object item, final boolean alsoStatics) throws IllegalAccessException,
        CannotCreateException {
        final StringBuilder sb = new StringBuilder();
        if (item instanceof String) {
            sb.append("\"" + ((String) item) + "\"");
        } else if (item instanceof Integer) {
            sb.append(((Integer) item).toString());
        } else if (item instanceof Double) {
            sb.append(((Double) item).toString());
        } else if (item instanceof Boolean) {
            sb.append(((Boolean) item).toString());
        } else if (implementsInterface(item, java.util.Map.class)) {
            @SuppressWarnings("unchecked")
            final String items = extractMapToJSON((Map<String, ?>) item, alsoStatics);
            sb.append(items);
        } else if (implementsInterface(item, java.util.List.class)) {
            @SuppressWarnings("unchecked")
            final String items = extractListToJSON((List<String>) item, alsoStatics);
            sb.append(items);
        } else {
            sb.append(toJSONString(item, alsoStatics));
        }
        return sb.toString();
    }

    public static String extractMapToJSON(Map<String, ?> map, final boolean alsoStatics)
        throws IllegalArgumentException, IllegalAccessException, CannotCreateException {
        final Set<?> entries = map.entrySet();
        final Iterator<?> it = entries.iterator();
        final StringBuilder sb = new StringBuilder();

        sb.append("{");
        while (it.hasNext()) {
            @SuppressWarnings("unchecked")
            Entry<String, ?> entry = (Entry<String, ?>) it.next();
            sb.append("\"" + entry.getKey() + "\":");

            final Object item = entry.getValue();
            sb.append(extractJSONItem(item, alsoStatics));

            if (it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    private static boolean implementsInterface(Object obj, Class<?> interf) {
        return ArrayUtils.contains(obj.getClass().getInterfaces(), interf);
    }

    public static String convertStringToURLFriendly(String str) {

        char[] chars = str.toCharArray();

        StringBuffer hex = new StringBuffer();
        for (int i = 0; i < chars.length; i++) {
            final char c = chars[i];
            if (c < '0' && c > '9' && c < 'A' && c > 'Z' && c < 'a' && c > 'z') {
                hex.append(Integer.toHexString((int) chars[i]));
            }
        }

        return hex.toString();
    }

    public static String convertURLFriendlyToString(String hex) {

        StringBuilder sb = new StringBuilder();
        StringBuilder temp = new StringBuilder();

        // 49204c6f7665204a617661 split into two characters 49, 20, 4c...
        for (int i = 0; i < hex.length() - 1; i++) {
            final char c = hex.charAt(i);
            if (c == '%') {
                // grab the hex in pairs
                String output = hex.substring(i, (i + 2));
                // convert hex to decimal
                int decimal = Integer.parseInt(output, 16);
                // convert the decimal to character
                sb.append((char) decimal);
                temp.append(decimal);

                i += 2;
            }
        }

        return sb.toString();
    }

    public static String getIconTypeName(final String type) {
        final Map<String, String> res = new HashMap<String, String>();
        res.put("jira", "JIRA");
        res.put("confluence", "Confluence");
        res.put("spiraTest", "SpiraTest");

        return res.get(type);
    }

    public static String getIconTypeImageUrl(final String type) {
        final Map<String, String> res = new HashMap<String, String>();
        res.put("jira", "img/JIRA_Logo_32x32px.png");
        res.put("confluence", "img/Confluence_Logo_32x32px.png");
        res.put("spiraTest", "img/SpiraTest_Logo_32x32px.png");

        return res.get(type);
    }
}
