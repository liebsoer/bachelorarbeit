package com.cgi.liebichs.bachelorarbeit.server.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ResourceUtils {

	private static String getResourceFileAbsolutePath(final String dirName, final String fileName){
		return getResourcesAbsolutePath() + dirName + "/" + fileName;
	}
	
	private static String getResourcesAbsolutePath(){
		return ResourceUtils.class.getResource("/resources/").getPath();
	}
	
	public static File getPropertiesFile(final String fileName){
		String resourceURL = getResourceFileAbsolutePath("properties", fileName);
		if(resourceURL == null){
			return null;
		}
		return new File(resourceURL);
	}
	
	public static File getJSONFile(final String fileName){
		final String resourceURL = getResourceFileAbsolutePath("json", fileName);
		if(resourceURL == null){
			return null;
		}
		return new File(resourceURL);
	}
	
	public static boolean writeJSONFile(final String fileName, final String content) throws IOException{
		final File subDir = new File(getResourcesAbsolutePath() + "json");
		if(!subDir.exists()){
			subDir.mkdirs();
		}
		
		final String fileAbsPath = getResourceFileAbsolutePath("json", fileName);
		final File jsonFile = fileAbsPath == null || fileAbsPath.isEmpty() ? new File(subDir + fileName) : getJSONFile(fileName);
		
		final FileWriter fileWriter = new FileWriter(jsonFile);
		final BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
		bufferedWriter.write(content);
		bufferedWriter.close();
		fileWriter.close();
		return true;
	}
}
