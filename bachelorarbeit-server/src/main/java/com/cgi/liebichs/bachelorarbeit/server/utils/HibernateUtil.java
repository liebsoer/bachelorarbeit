package com.cgi.liebichs.bachelorarbeit.server.utils;

import java.io.Serializable;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateUtil implements Serializable {
    private static final long serialVersionUID = -8469839504691000160L;

    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        final Configuration configuration = new Configuration().configure();

        final ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(
            configuration.getProperties()).buildServiceRegistry();
        final SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        
        return sessionFactory;
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    
    public static Session getOpenSession(){
        return getSessionFactory().openSession();
    }
    
    public static void closeSession(final Session session){
        session.close();
    }

    public static void shutdown() {
        getSessionFactory().close();
    }

    
}
