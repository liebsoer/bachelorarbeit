package com.cgi.liebichs.bachelorarbeit.server.dao;

import com.cgi.liebichs.bachelorarbeit.server.domain.ConfluencePage;
import com.cgi.liebichs.bachelorarbeit.server.domain.JIRAIssue;
import com.cgi.liebichs.bachelorarbeit.server.domain.SpiraTestTestCase;
import com.cgi.liebichs.bachelorarbeit.server.domain.SpiraTest_TC;
import com.cgi.liebichs.bachelorarbeit.server.utils.HibernateUtil;
import org.apache.log4j.*;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.hibernate.*;
import org.hibernate.criterion.*;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

public class ImportDAO {

    /*
     * private static final String TABLE_NAME_CONNECTION = "Connection"; private
     * static final String TABLE_NAME_ISSUE = "ISSUE";
     */

    private static Logger log = Logger.getLogger(ConnectionDAO.class);
    static {
        try {
            log.addAppender(new FileAppender(new SimpleLayout(), "/home/liebsoer/logs/bacServer/"
                + ImportDAO.class.getSimpleName(), false));
            log.addAppender(new ConsoleAppender(new SimpleLayout(), "System.out"));
            log.setLevel(Level.ALL);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    final private SessionFactory sessionFactory;

    final private ObjectMapper mapper;

    public ImportDAO() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
        this.mapper = new ObjectMapper().configure(Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
    }

    public Session getSession() {
        return sessionFactory.openSession();
    }

    public boolean saveImportedIssues(ArrayNode issues) throws JsonParseException, JsonMappingException, IOException {
        final Session session = this.getSession();
        final Transaction tx = session.beginTransaction();

        final HashMap<String, Integer> counter2 = new HashMap<>();

        int counter = 0;
        for (final JsonNode node : issues) {
            final JIRAIssue issue = mapper.readValue(node, JIRAIssue.class);

            final Query q = session.createQuery("SELECT 1 FROM Issue WHERE issueKey = :key");
            q.setString("key", issue.getIssueKey());
            if(q.uniqueResult() != null){
                continue;
            }
            
            if (!counter2.containsKey(node.get("priority").asText())) {
                counter2.put(node.get("priority").asText(), new Integer(0));
            }
            int i = counter2.get(node.get("priority").asText()).intValue() + 1;
            counter2.put(node.get("priority").asText(), new Integer(i));

            session.save(issue);

            if (counter++ == 100) {
                session.flush();
            }
        }

        log.debug(counter2);

        session.flush();
        tx.commit();
        session.close();
        return true;
    }

    public List<String> getImportedStatusTypes() {
        final Session session = this.getSession();
        final Query q = session.createQuery("select distinct status from Issue");
        @SuppressWarnings("unchecked")
        final List<String> list = q.list();
        session.close();
        return list;
    }

    public Map<String, String> getImportetFieldCounts(final String field) {
        final Session session = this.getSession();
        final Query q = session.createQuery("select distinct " + field + " from Issue");
        @SuppressWarnings("unchecked")
        final List<String> list = q.list();
        return countType(field, list);
    }

    public Map<String, String> countType(final String field, final String... types) {
        final Session session = this.getSession();

        final Map<String, String> res = new HashMap<String, String>();

        for (final String type : types) {
            final String query = "select count(" + field + ") from Issue where " + field + " = '" + type + "'";
            final Query q = session.createQuery(query);
            Object result = q.uniqueResult();
            res.put(type, String.valueOf(result));
        }
        session.close();
        return res;
    }

    public Map<String, String> countType(final String field, final List<String> types) {
        final String[] sa = new String[types.size()];
        types.toArray(sa);
        return countType(field, sa);
    }

    public Map<String, String> getImportetComponentCounts() {
        final Session session = this.getSession();
        final Query q = session.createQuery("select distinct components from Issue");
        @SuppressWarnings("unchecked")
        final List<String> list = q.list();

        final Set<String> components = new HashSet<>();
        for (final String component : list) {
            final String[] sa = component.replaceAll("[\\[\\]\\\"]", "").split(",");
            components.addAll(Arrays.asList(sa));
        }

        final Map<String, String> res = new HashMap<String, String>();

        for (final String type : components) {
            // final String query =
            // "SELECT `components`FROM `Issue` WHERE `components` LIKE '%Tool%'";
            final String query = "SELECT count(*) FROM Issue WHERE components LIKE '%\"" + type + "\"%'";
            final Query q2 = session.createQuery(query);
            final Object result = q2.uniqueResult();
            res.put(type, String.valueOf(result));
        }
        session.close();

        return res;
    }

    public ArrayNode getIssues(HashMap<String, List<String>> selects) {
        final Session session = this.getSession();
        final ArrayNode result = mapper.createArrayNode();

        final StringBuffer queryString = new StringBuffer("SELECT * FROM `Issue` WHERE ");
        final Iterator<String> keyIt = selects.keySet().iterator();

        final Conjunction and = Restrictions.and();
        while (keyIt.hasNext()) {
            final String key = keyIt.next();

            final Iterator<String> valueIt = selects.get(key).iterator();
            if (valueIt.hasNext()) {
                queryString.append("(");
            }

            final Disjunction or = Restrictions.or();

            while (valueIt.hasNext()) {
                final String value = valueIt.next();

                or.add(Restrictions.like(key, value, MatchMode.ANYWHERE));

                if (!key.equals("components")) {
                    queryString.append("`" + key + "`" + " like '" + value + "'");
                } else {
                    queryString.append("`" + key + "`" + " like '%\"" + value + "\"%'");
                }

                if (valueIt.hasNext()) {
                    queryString.append(" OR ");
                }
            }

            and.add(or);

            queryString.append(")");

            if (keyIt.hasNext()) {
                queryString.append(" AND ");
            }

        }

        final Criteria criteria = session.createCriteria(JIRAIssue.class).add(and);

        @SuppressWarnings("unchecked")
        final List<JIRAIssue> list = criteria.list();

        for (final JIRAIssue issue : list) {
            final ObjectNode node = mapper.createObjectNode();

            node.put("id", issue.getId());
            node.put("components", issue.getComponents());
            node.put("status", issue.getStatus());
            node.put("priority", issue.getPriority());
            node.put("name", issue.getSummary());
            node.put("resolution", issue.getResolution());
            node.put("type", issue.getIssuetype());
            node.put("key", issue.getIssueKey());
            node.put("project", issue.getProject());

            result.add(node);
        }

        session.close();
        return result;

    }

    public boolean saveImportedPages(List<ConfluencePage> pageList) {
        final Session session = this.getSession();
        final Transaction tx = session.beginTransaction();

        int counter = 0;
        for (final ConfluencePage page : pageList) {

            if (!isPageExisting(page.getPageId(), session)) {
                session.saveOrUpdate(page);
                if (counter++ == 100) {
                    session.flush();
                }
            }
        }

        session.flush();
        tx.commit();
        session.close();
        return true;
    }

    private boolean isPageExisting(final String id, final Session session) {
        return session.createCriteria(ConfluencePage.class).add(Restrictions.like("pageId", id, MatchMode.EXACT))
            .uniqueResult() != null;
    }

    public Map<String, Long> getConfluenceProjectsWithCount() {
        final Session session = this.getSession();
        final Query q = session.createQuery("select distinct projectKey from ConfluencePage");
        @SuppressWarnings("unchecked")
        final List<String> list = q.list();

        final Map<String, Long> result = new HashMap<String, Long>();
        for (final String project : list) {
            final Query query = session
                .createQuery("select projectName, count(*) from ConfluencePage where projectKey like '" + project + "'");
            final Object[] o = (Object[]) query.uniqueResult();
            result.put(project + "${#}" + (String) o[0], (Long) o[1]);
        }

        session.close();
        return result;
    }

    public ArrayNode getConfluencePages(final String project, final String search) {
        log.debug("project: " + project + "; search: " + (search != null ? search : "null"));

        final Session session = this.getSession();
        final ArrayNode result = mapper.createArrayNode();

        final Criteria crit = session.createCriteria(ConfluencePage.class).add(
            Restrictions.like("projectKey", project, MatchMode.EXACT));
        if (search != null && !search.isEmpty()) {
            crit.add(Restrictions.like("title", search, MatchMode.ANYWHERE));
        }

        @SuppressWarnings("unchecked")
        final List<ConfluencePage> list = crit.list();
        for (final ConfluencePage cp : list) {
            result.addPOJO(cp);
        }

        log.debug("result: " + result.toString());

        session.close();
        return result;
    }

    public ArrayNode getConfluencePages(String project) {
        return this.getConfluencePages(project, null);
    }

    public boolean saveImportedTestCases(ArrayList<SpiraTestTestCase> list) {
        final Session session = this.getSession();
        final Transaction tx = session.beginTransaction();

        int counter = 0;
        for (final SpiraTestTestCase page : list) {

//            if (!isTestCaseExisting(page.getTestCaseId(), session)) {
                session.saveOrUpdate(page);
                if (counter++ == 100) {
                    session.flush();
                }
//            }
        }

        session.flush();
        tx.commit();
        session.close();
        return true;
    }

    private boolean isTestCaseExisting(int testCaseId, Session session) {
        final String id = String.valueOf(testCaseId);
        
        final SimpleExpression rest = Restrictions.like("testCaseId", id, MatchMode.EXACT);
        final Criteria crit = session.createCriteria(SpiraTestTestCase.class).add(rest);//.add(Restrictions.like("testCaseId", id, MatchMode.EXACT));
        final Object res = crit.list();
        //.add(Restrictions.like("testCaseId", id, MatchMode.EXACT)).uniqueResult();
        
        return crit != null;
    }

    public List<JIRAIssue> getJIRAIssues(Map<String, List<String>> dataMap) {
        final Session session = this.getSession();
        
        final Criteria crit = session.createCriteria(JIRAIssue.class);
        final Iterator<Entry<String, List<String>>> it = dataMap.entrySet().iterator();
        while(it.hasNext()){
            final Entry<String, List<String>> ele = it.next();
            
            final Conjunction and = Restrictions.and();
            and.add(Restrictions.like("project", ele.getKey(), MatchMode.EXACT));
            and.add(Restrictions.in("issueKey", ele.getValue()));
            
            crit.add(and);
        }
        
        List<JIRAIssue> l = crit.list();
        
        session.close();
        return l;
    }

    public List<ConfluencePage> getConfluencePages(Map<String, List<String>> dataMap) {
        final Session session = this.getSession();
        
        final Criteria crit = session.createCriteria(ConfluencePage.class);
        final Iterator<Entry<String, List<String>>> it = dataMap.entrySet().iterator();
        while(it.hasNext()){
            final Entry<String, List<String>> ele = it.next();
            
            final Conjunction and = Restrictions.and();
            and.add(Restrictions.like("projectKey", ele.getKey(), MatchMode.EXACT));
            and.add(Restrictions.in("pageId", ele.getValue()));
            
            crit.add(and);
        }
        
        List<ConfluencePage> l = crit.list();
        
        session.close();
        return l;
    }

    public List<SpiraTest_TC> getSpiraTestCases(Map<String, List<String>> dataMap) {
        return null;
    }

}
