package com.cgi.liebichs.bachelorarbeit.server.service;

import com.cgi.liebichs.bachelorarbeit.server.dao.ImportDAO;
import com.cgi.liebichs.bachelorarbeit.server.dao.ManageDAO;
import com.cgi.liebichs.bachelorarbeit.server.domain.Component;
import com.cgi.liebichs.bachelorarbeit.server.domain.ConfluencePage;
import com.cgi.liebichs.bachelorarbeit.server.domain.JIRAIssue;
import com.cgi.liebichs.bachelorarbeit.server.domain.SpiraTest_TC;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

@Path("/manage")
public class ComponentManagementService {

    final private static Logger log = Logger.getLogger(ConnectionManagementService.class);
    static {
        try {
            log.addAppender(new FileAppender(new SimpleLayout(),
                "/home/liebsoer/logs/bacServer/ComponentManagementService", false));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private final ManageDAO manageDAO;
    private final ObjectMapper mapper;
    
    @Context UriInfo ui;

    public ComponentManagementService() {
        manageDAO = new ManageDAO();
        mapper = new ObjectMapper();
    }

    @GET
    @Path("/component")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayNode getComponents(@QueryParam("types[]") List<String> filter) {

        final ArrayNode array = mapper.createArrayNode();

        final Iterator<Component> it = manageDAO.getComponents(filter).iterator();
        while (it.hasNext()) {
            final Component element = it.next();
            final ObjectNode node = mapper.createObjectNode();
            node.put("id", element.getId());
            node.put("name", element.getName());
            node.put("description", element.getDescription());
            node.put("type", element.getType());
            node.put("specCount", (element.getConfluenceSpecs().size() + element.getJiraSpecIssues().size()));
            node.put("bugCount", element.getJiraBugIssues().size());
//            node.put("testCount", element.getSpiraTestCases().size());

            array.add(node);
        }

        return array;
    }

    @GET
    @Path("/component/meta")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayNode getComponentsMeta() {
        final ArrayNode array = mapper.createArrayNode();

        final Map<String, Integer> map = manageDAO.getComponentsMeta();

        final Iterator<Entry<String, Integer>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            final Entry<String, Integer> ele = it.next();
            final ObjectNode node = mapper.createObjectNode();
            node.put("name", ele.getKey());
            node.put("count", ele.getValue());

            array.add(node);
        }

        return array;
    }

    @POST
    @Path("/component")
    public void postComponent(@FormParam("name") final String name, @FormParam("description") final String description, @FormParam("type") final String type) {
        final Component component = new Component();
        component.setName(name);
        component.setDescription(description);
        component.setType(type);
        component.setConfluenceSpecs(new HashSet<ConfluencePage>());
        component.setJiraBugIssues(new HashSet<JIRAIssue>());
        component.setJiraSpecIssues(new HashSet<JIRAIssue>());
//        component.setSpiraTestCases(new HashSet<SpiraTest_TC>());
        manageDAO.addComponent(component);
    }

    @GET
    @Path("/component/{id}")
    public Component getComponent(@PathParam("id") final long id) {
        return manageDAO.getComponent(id);
    }

//    @PUT
//    @Path("/component/{id}")
//    public void putComponent(@PathParam("id") final long id, @FormParam("name") final String name, @FormParam("description") final String description, @FormParam("type") final String type) {
//        final Component component = manageDAO.getComponent(id);
//        component.setName(name);
//        component.setDescription(description);
//        component.setType(type);
//        
//        manageDAO.updateComponent(component);
//    }
    
    @PUT @Path("/component/{id}")
    public void putComponentByFieldName(@PathParam("id") final long id){
        final Iterator<Entry<String, List<String>>> it = ui.getQueryParameters().entrySet().iterator();
        final Component component = manageDAO.getComponent(id);
        while(it.hasNext()){
            final Entry<String, List<String>> element = it.next();
            
            switch(element.getKey()){
            case "name":
                break;
            case "description":
                break;
            case "type":
                break;
            default:
                break;
            }
        }
    }
    
    @DELETE @Path("/component/{id}")
    public void deleteComponent(@PathParam("id") final long id) {
        manageDAO.removeComponent(id);
    }
    
    @POST @Path("/component/{id}/item")
    public void postAddItems(@PathParam("id") final long id, @FormParam("type") final String type, @FormParam("system") final String system, @FormParam("data[]") List<String> data) throws JsonProcessingException, IOException{
        final StringBuilder sb = new StringBuilder();
        sb.append("{id: " + id + ", type: " + type + ", data: [");
        sb.append(data);
//        for(int i = 0; i < data.size(); i++){
//            sb.append(data.get(i));
//            if(i < data.size() - 1){
//                sb.append(",");
//            }
//        }
        sb.append("]}");
        log.debug(sb.toString());
        
        
        final Map<String, List<String>> dataMap = new HashMap<String, List<String>>();
        final Iterator<JsonNode> it = mapper.readTree(data.toString()).iterator();
        while(it.hasNext()){
            final JsonNode ele = it.next();
            final String key = ele.get(0).getTextValue();
            final String project = ele.get(1).getTextValue();
            if(!dataMap.containsKey(project)){
                dataMap.put(project, new ArrayList<String>());
            }
            dataMap.get(project).add(key);
        }
        
        final ImportDAO importDAO = new ImportDAO();
        final Component component = manageDAO.getComponent(id);
        
        switch(system){
        case "jira":
            final List<JIRAIssue> jiraIssues = importDAO.getJIRAIssues(dataMap);
            switch(type){
                case "specification":
                    component.getJiraSpecIssues().addAll(jiraIssues);
                    break;
                case "bug":
                    component.getJiraBugIssues().addAll(jiraIssues);
                    break;
            }
            break;
        case "confluence":
            switch(type){
                case "specification":
                final List<ConfluencePage> confluencePages = importDAO.getConfluencePages(dataMap);
                component.getConfluenceSpecs().addAll(confluencePages);
                    break;
                }
            break;
        case "spiraTest":
            switch(type){
                case "test":
                final List<SpiraTest_TC> spiraTestCases = importDAO.getSpiraTestCases(dataMap);
//                component.getSpiraTestCases().addAll(spiraTestCases);
                    break;
            }
            break;
        }
        
        manageDAO.updateComponent(component);
    }
    
    private void saveItems(final Component component, final List<String> data){
        
    }
}
