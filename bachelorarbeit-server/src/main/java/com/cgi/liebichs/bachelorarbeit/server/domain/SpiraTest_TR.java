package com.cgi.liebichs.bachelorarbeit.server.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class SpiraTest_TR implements Serializable{
    private static final long serialVersionUID = -6229893221404403612L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private long id;
    
    @Column
    private String data;

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getData() {
        return this.data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SpiraTest_TR [id=" + this.id + ", data=" + this.data + "]";
    }
    
    
}
