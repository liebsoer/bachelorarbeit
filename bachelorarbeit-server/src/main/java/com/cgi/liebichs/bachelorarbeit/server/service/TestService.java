package com.cgi.liebichs.bachelorarbeit.server.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.xml.sax.SAXException;

import com.cgi.liebichs.bachelorarbeit.server.utils.XMLUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Path("/test")
public class TestService {

    private final static Logger log = Logger.getLogger(TestService.class);
    static{
        try {
            log.addAppender(new FileAppender(new SimpleLayout(), "/home/liebsoer/logs/bacServer/TestService", false));
            log.setLevel(Level.ALL);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @GET
    public String test(){
        return "Hello World";
    }
    
    private final ObjectMapper mapper = new ObjectMapper();
    
    @GET @Produces(MediaType.APPLICATION_JSON)
    public ObjectNode testJSON(){
        final ObjectNode obj = mapper.createObjectNode();
        obj.put("Hello", "World");
        
        return obj;
    }
    
     
        @POST
        @Path("/upload")
        @Consumes(MediaType.MULTIPART_FORM_DATA)
        @Produces(MediaType.APPLICATION_JSON)
        public Response uploadFile(
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) throws SAXException, IOException, ParserConfigurationException {
     
            String uploadedFileLocation = "/home/liebsoer/upload/" + fileDetail.getFileName();
     
            new File("/home/liebsoer/upload/").mkdirs();
            
            // save it
            int length = writeToFile(uploadedInputStream, uploadedFileLocation);
     
            XMLUtils utils = new XMLUtils(uploadedInputStream);
            
            ObjectNode result = mapper.createObjectNode();
            ArrayNode files = mapper.createArrayNode();
            ObjectNode file = mapper.createObjectNode();
            
            file.put("name", fileDetail.getFileName());
            //file.put("size", length);
            file.put("error", "abort");
            file.put("type", fileDetail.getType());
            
            files.add(file);
            result.put("files", files);
            
            log.debug(result);
            return Response.status(200).entity(result.toString()).build();
//            return Response.status(200).entity(output).build();
//            return result;
     
        }
        
     // save uploaded file to new location
        private int writeToFile(InputStream uploadedInputStream,
            String uploadedFileLocation) {
            
            int counter = 0;
            try {
                OutputStream out = new FileOutputStream(new File(
                        uploadedFileLocation));
                int read = 0;
                byte[] bytes = new byte[1024];
     
                out = new FileOutputStream(new File(uploadedFileLocation));
                while ((read = uploadedInputStream.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                    counter += read;
                }
                
                
                out.flush();
                out.close();
                
                return counter;
            } catch (IOException e) {
     
                e.printStackTrace();
            }
     
            return -1;
        }

}
