package com.cgi.liebichs.bachelorarbeit.server.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Connection {
    @Column @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name="conectionKey")
    private String key;
    @Column
    private String name;
    @Column
    private String description;
    @Column @Enumerated(EnumType.STRING)
    private EServerType type;
    @Column
    private String iconImageUrl;
    @Column
    private String iconText;
    @Column
    private String username;
    @Column
    private String password;
    @Column
    private String typeName;
    @Column
    private String typeUrl;
    @Column
    private String url;
    
    public long getId() {
        return this.id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getKey() {
        return this.key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return this.description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public EServerType getType() {
        return this.type;
    }
    public void setType(EServerType type) {
        this.type = type;
    }
    public String getIconImageUrl() {
        return this.iconImageUrl;
    }
    public void setIconImageUrl(String iconUrl) {
        this.iconImageUrl = iconUrl;
    }
    public String getIconText() {
        return this.iconText;
    }
    public void setIconText(String iconText) {
        this.iconText = iconText;
    }
    public String getUsername() {
        return this.username;
    }
    public void setUsername(String user) {
        this.username = user;
    }
    public String getPassword() {
        return this.password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getTypeName() {
        return this.typeName;
    }
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
    public String getTypeUrl() {
        return this.typeUrl;
    }
    public void setTypeUrl(String typeUrl) {
        this.typeUrl = typeUrl;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (this.id ^ (this.id >>> 32));
        result = prime * result + ((this.key == null) ? 0 : this.key.hashCode());
        result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Connection other = (Connection) obj;
        if (this.id != other.id)
            return false;
        if (this.key == null) {
            if (other.key != null)
                return false;
        } else if (!this.key.equals(other.key))
            return false;
        if (this.name == null) {
            if (other.name != null)
                return false;
        } else if (!this.name.equals(other.name))
            return false;
        return true;
    }
    @Override
    public String toString() {
        return "Connection [id=" + this.id + ", key=" + this.key + ", name=" + this.name + ", description="
            + this.description + ", type="  + this.type + ", iconUrl=" + this.iconImageUrl + ", iconType=" + this.iconText
            + ", user=" + this.username + ", password=" + this.password + ", typeUrl=" + this.typeUrl + "]";
    }
    
    

}
