package com.cgi.liebichs.bachelorarbeit.server.dao.client;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map.Entry;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.cgi.liebichs.bachelorarbeit.server.dao.ConnectionDAO;
import com.cgi.liebichs.bachelorarbeit.server.domain.ClientIsNotAvailableException;
import com.cgi.liebichs.bachelorarbeit.server.domain.Connection;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.core.util.Base64;

public class JIRAClient {

    private final static Logger LOGGER = Logger.getLogger(JIRAClient.class.getName());
    {
        try {
            final String path = System.getProperty("user.home") + "/log/bacServer";
            new File(path).mkdirs();
            final String logPath = path + "/" + new SimpleDateFormat("yyyy-dd-MM_hh-mm").format(new Date()) + "_" + JIRAClient.class.getName();
            LOGGER.addAppender(new FileAppender(new SimpleLayout(), logPath, false));
            LOGGER.addAppender(new ConsoleAppender(new SimpleLayout(), "System.out"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        LOGGER.setLevel(Level.ALL);
    }

    private String auth;
    private String user;
    private String url;
    private Client client;
    private int issueCounter = 0;
    final private ObjectMapper mapper;
    private int totalIssuesCount;

    public static JIRAClient createJIRAClientForConnection(final long id) {
        final Connection con = new ConnectionDAO().getConnection(id);
        return new JIRAClient(con.getUsername(), con.getPassword(), con.getUrl());
    }

    public JIRAClient(final String username, final String password, final String serverURL) {
        this.user = username;
        this.url = serverURL;

        if (this.url.charAt(this.url.length() - 1) == '/') {
            this.url = this.url.substring(0, this.url.length() - 1);
        }

        this.auth = "Basic " + new String(Base64.encode(user + ":" + password));

        this.client = Client.create();

        this.mapper = new ObjectMapper();
    }

    private ClientResponse createResponse(final String url) throws ClientIsNotAvailableException {
        if (!this.isClientAvailable()) {
            throw new ClientIsNotAvailableException();
        }

        LOGGER.debug(url);

        final ClientResponse response = client.resource(url).header("Authorization", this.auth)
            .type("application/json").accept("application/json").get(ClientResponse.class);

        LOGGER.info("User " + this.user + " access " + url);

        return response;
    }

    private boolean isClientAvailable() {
        return (this.client != null);
    }

    public void destroyClient() {
        this.client.destroy();
    }

    public ArrayNode getProjects() throws IOException, ClientIsNotAvailableException {
        final ArrayNode result;

        final ClientResponse response = this.createResponse(this.url + "/project");

        LOGGER.info("Status:" + response.getStatus());
        if (response.getStatus() == 200) {
            final String resp = response.getEntity(String.class);
            result = mapper.readValue(resp, ArrayNode.class);
        } else {
            result = mapper.createArrayNode();
        }
        response.close();

        return result;
    }

    public String getProjectURL() {
        return this.url;
    }

    public String getProjectJSON(final String projectKey) throws ClientIsNotAvailableException {
        LOGGER.info(this.getClass().getName() + " getProject");
        final String url = this.url + (this.url.charAt(this.url.length() - 1) == '/' ? "" : "/") + "project/"
            + projectKey;
        final ClientResponse response = this.createResponse(url);

        LOGGER.info(String.valueOf(response.getStatus()));
        if (response.getStatus() == 200) {
            return response.getEntity(String.class);
        }
        return null;
    }

    public String getCreateMeta() throws ClientIsNotAvailableException {
        final String url = this.url + (this.url.charAt(this.url.length() - 1) == '/' ? "" : "/")
            + "issue/createmeta?expand=projects.issuetypes.fields";

        LOGGER.info(url);

        final ClientResponse response = this.createResponse(url);
        if (response.getStatus() == Status.OK.getStatusCode()) {
            return response.getEntity(String.class);
        }
        return null;
    }

    public String getIssuesByType(final String issueType, final String project) throws ClientIsNotAvailableException {
        LOGGER.debug(this.getClass().getName() + " getProject");
        final String searchQuery = "project = \"" + project + "\" AND issuetype = \"" + issueType + "\"";
        LOGGER.debug(searchQuery);
        final String url = this.url + (this.url.charAt(this.url.length() - 1) == '/' ? "" : "/")
            + "search?maxResults=1000&jql=" + convertStringToURLFriendly(searchQuery);

        LOGGER.debug(url);
        final ClientResponse response = this.createResponse(url);

        LOGGER.debug(String.valueOf(response.getStatus()));
        if (response.getStatus() == 200) {
            return response.getEntity(String.class);
        }
        return null;
    }

    public String convertStringToURLFriendly(String str) {

        char[] chars = str.toCharArray();

        StringBuffer hex = new StringBuffer();
        for (int i = 0; i < chars.length; i++) {
            final char c = chars[i];

            if (!((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))) {
                hex.append("%" + Integer.toHexString((int) c).toUpperCase());
            } else {
                hex.append(c);
            }
        }
        return hex.toString();
    }

    public List<String> parseCustomFieldOption() throws ClientIsNotAvailableException, IOException {
        final int start = 2000;
        final int n = 4000;
        final String url = this.url + (this.url.charAt(this.url.length() - 1) == '/' ? "" : "/") + "customFieldOption/";
//        final BufferedWriter bw = new BufferedWriter(new FileWriter(new File("Q:/temp/bac/customFieldOptions.json"),
//            true));
//        bw.write("[");
        for (int i = start; i < n; i++) {
            final ClientResponse response = this.createResponse(url + "1" + (i < 1000 ? "0" : "")
                + (i < 100 ? "0" : "") + (i < 10 ? "0" : "") + i);

            LOGGER.debug(String.valueOf(response.getStatus()));
            if (response.getStatus() == 200) {
                final String s = response.getEntity(String.class);
                LOGGER.info(s);
                // list.add(s);
//                bw.write(s);
                if (i < n - 1) {
//                    bw.write(",");
                }
            }
        }
//        bw.write("]");
//        bw.close();
        return null;
    }

    /**
     * 
     * @param projectKey
     *            Key of JIRA project
     * @param attrKeys
     *            Attribute field names of project
     * @return JSONObject of @projectKey with fields given with @attrKeys
     * @throws JSONException
     * @throws ClientIsNotAvailableException
     */
    public JSONObject getProject(final String projectKey, final String... attrKeys) throws JSONException,
        ClientIsNotAvailableException {
        final JSONObject project = new JSONObject(getProjectJSON(projectKey));
        final JSONObject obj = new JSONObject();

        for (final String key : attrKeys) {
            if (project.has(key)) {
                obj.put(key, project.get(key));
            }
        }
        return obj;
    }

    public ObjectNode getFilterValues(String project) throws JsonProcessingException, ClientIsNotAvailableException,
        IOException {
        final ObjectNode filterValues = mapper.createObjectNode();

        final ArrayNode issues = getItemCounts(project, "issuetype", getIssueTypes(project));
        filterValues.put("issuetype", issues);

        final ArrayNode status = getItemCounts(project, "status", getStatusTypes());
        filterValues.put("status", status);

        final ArrayNode priority = getItemCounts(project, "priority", getPriorities());
        filterValues.put("priority", priority);

        final ArrayNode component = getItemCounts(project, "component", getComponents(project));
        filterValues.put("component", component);

        return filterValues;
    }

    public ArrayNode getItemCounts(final String project, final String type, final ArrayNode items)
        throws JsonProcessingException, ClientIsNotAvailableException, IOException {
        final ArrayNode itemCount = mapper.createArrayNode();

        for (final JsonNode itemType : items) {
            if (!itemType.isObject()) {
                continue;
            }
            final String search = "search?maxResults=1&fields=total&jql="
                + convertStringToURLFriendly("project = \"" + project + "\" AND " + type + " = \""
                    + itemType.get("name").asText() + "\"");

            LOGGER.debug("search?maxResults=1&fields=total&jql=project = \"" + project + "\" AND " + type + " = \""
                + itemType.get("name").asText() + "\"");

            final String url = this.url + "/" + search;

            final ClientResponse response = createResponse(url);
            if (response.getStatus() == Status.OK.getStatusCode()) {
                final String resp = response.getEntity(String.class);
                final JsonNode tree = mapper.readTree(resp);

                final ObjectNode item = (ObjectNode) itemType;
                item.put("id", itemType.get("id"));
                item.put("name", itemType.get("name"));
                item.put("description", itemType.get("description"));
                if (item.get("iconUrl") != null) {
                    item.put("iconUrl", itemType.get("iconUrl"));
                }
                item.put("count", tree.get("total"));

                itemCount.add(item);
            }

        }

        return itemCount;
    }

    public ArrayNode getIssueTypes(final String project) throws ClientIsNotAvailableException, JsonProcessingException,
        IOException {
        final String url = this.url + "/issue/createmeta";

        final ClientResponse response = this.createResponse(url);

        if (response.getStatus() == 200) {
            final String resp = response.getEntity(String.class);
            final JsonNode node = mapper.readTree(resp);

            for (final JsonNode prjNode : node.get("projects")) {
                if (prjNode.get("key").asText().equals(project)) {

                    final JsonNode issueTypesNode = mapper.readTree(prjNode.get("issuetypes").toString());
                    if (issueTypesNode.isArray()) {
                        final ArrayNode issueTypes = mapper.createArrayNode();

                        for (final JsonNode issueType : issueTypesNode) {
                            final ObjectNode item = mapper.createObjectNode();
                            item.put("id", issueType.get("id"));
                            item.put("name", issueType.get("name"));
                            item.put("description", issueType.get("description"));
                            if (issueType.get("iconUrl") != null) {
                                item.put("iconUrl", issueType.get("iconUrl"));
                            }

                            issueTypes.add(item);
                        }

                        return issueTypes;
                    }

                }
            }
        }

        return null;
    }

    public ArrayNode getStatusTypes() throws ClientIsNotAvailableException, JsonProcessingException, IOException {
        final String url = this.url + "/status";

        final ClientResponse response = this.createResponse(url);

        if (response.getStatus() == Status.OK.getStatusCode()) {

            final JsonNode node = mapper.readTree(response.getEntity(String.class));

            if (node.isArray()) {
                final ArrayNode statusType = mapper.createArrayNode();

                for (final JsonNode statusNode : node) {
                    final ObjectNode item = mapper.createObjectNode();
                    item.put("id", statusNode.get("id"));
                    item.put("name", statusNode.get("name"));
                    item.put("description", statusNode.get("description"));
                    if (statusNode.get("iconUrl") != null) {
                        item.put("iconUrl", statusNode.get("iconUrl"));
                    }

                    statusType.add(item);
                }

                return statusType;
            }
        }

        return null;
    }

    public ArrayNode getPriorities() throws ClientIsNotAvailableException, JsonProcessingException, IOException {
        final String url = this.url + "/priority";

        final ClientResponse response = this.createResponse(url);

        if (response.getStatus() == Status.OK.getStatusCode()) {

            final JsonNode node = mapper.readTree(response.getEntity(String.class));

            if (node.isArray()) {
                final ArrayNode priorities = mapper.createArrayNode();

                for (final JsonNode priority : node) {
                    final ObjectNode item = mapper.createObjectNode();
                    item.put("id", priority.get("id"));
                    item.put("name", priority.get("name"));
                    item.put("description", priority.get("description"));
                    if (priority.get("iconUrl") != null) {
                        item.put("iconUrl", priority.get("iconUrl"));
                    }

                    priorities.add(item);
                }

                return priorities;
            }
        }

        return null;
    }

    public ArrayNode getComponents(final String project) throws ClientIsNotAvailableException, JsonProcessingException,
        IOException {
        final String url = this.url + "/project/" + project + "/components";

        final ClientResponse response = this.createResponse(url);

        if (response.getStatus() == Status.OK.getStatusCode()) {

            final JsonNode node = mapper.readTree(response.getEntity(String.class));

            if (node.isArray()) {
                final ArrayNode components = mapper.createArrayNode();

                for (final JsonNode component : node) {
                    final ObjectNode item = mapper.createObjectNode();
                    item.put("id", component.get("id"));
                    item.put("name", component.get("name"));
                    item.put("description", component.get("description"));
                    if (component.get("iconUrl") != null) {
                        item.put("iconUrl", component.get("iconUrl"));
                    }

                    components.add(item);
                }

                return components;
            }
        }

        return null;
    }

    public int getTotalIssues() throws IOException, ClientIsNotAvailableException {
        int total = 0;

        for (final JsonNode prj : this.getProjects()) {
            final String key = prj.get("key").asText();
            final String search = "search?maxResults=1&fields=total&jql="
                + this.convertStringToURLFriendly("project=\"" + key + "\"");
            final String url = this.url + "/" + search;

            final ClientResponse response = this.createResponse(url);
            final String entity = response.getEntity(String.class);

            final JsonNode node = mapper.readTree(entity);
            if (node.isObject()) {
                total += node.get("total").asInt();
            }
        }

        return total;
    }

    public ArrayNode getImportIssues(final JsonNode root) throws ClientIsNotAvailableException, IOException {
        if (root.isObject()) {
            final Iterator<Entry<String, JsonNode>> it = root.getFields();
            final StringBuffer search = new StringBuffer();
            while (it.hasNext()) {
                final Entry<String, JsonNode> item = it.next();

                if(item.getValue().size() == 1 && item.getValue().get(0).asText().length() == 0){
                    continue;
                }
                
                if (item.getValue().isArray() && item.getValue().size() > 0) {
                    final StringBuffer subSearch = new StringBuffer();

                    subSearch.append(item.getKey() + " in (");
                    final Iterator<JsonNode> subIt = item.getValue().iterator();
                    while (subIt.hasNext()) {
                        final JsonNode subNode = subIt.next();
                        subSearch.append("\"" + subNode.asText() + "\"");
                        if (subIt.hasNext()) {
                            subSearch.append(", ");
                        }
                    }
                    subSearch.append(") ");
                    search.append(subSearch);

                    if (it.hasNext()) {
                        search.append(" AND ");
                    }
                }

            }

            LOGGER.debug(search.toString());

            final String searchQuery = this.convertStringToURLFriendly(search.toString());

            LOGGER.debug(searchQuery);
            final int totalIssuesCount = getTotalIssueCountForSearch(searchQuery);
            
            this.totalIssuesCount = totalIssuesCount;
            
            final int maxIssuesPerRequest = 1000;

            final ArrayNode issues = mapper.createArrayNode();
            HashMap<String, ObjectNode> map = new HashMap<>();
            for (int i = 0; i < totalIssuesCount; i += maxIssuesPerRequest) {
                map.putAll(getIssues(searchQuery, i, maxIssuesPerRequest));
                //issues.addAll(getIssues(searchQuery, i, maxIssuesPerRequest));
                //LOGGER.debug((i * maxIssuesPerRequest) + " issues imported");
            }

            LOGGER.debug(map.size() + " issues imported");
            
            for(final ObjectNode on: map.values()){
                issues.add(on);
            }
            
            return issues;
        }
        return null;
    }

    private HashMap<String, ObjectNode> getIssues(final String query, final int start, final int maxResults)
        throws ClientIsNotAvailableException, IOException {
        final String[] keys = new String[] { "key", "progress", "summary", "issuetype", "timespent", "reporter",
            "updated", "created", "priority", "description", "status", "labels", "workratio", "project", "environment",
            "aggregateprogress", "lastVieweed", "components", "resolution", "aggregatetimeoriginalestimate",
            "aggregatetimeestimate", "timeestimate" };

        final String url = this.url + "/search?maxResults=" + maxResults + "&startAt=" + start + "&jql=" + query;
        final ClientResponse response = this.createResponse(url);
        final String entity = response.getEntity(String.class);
        final JsonNode node = mapper.readTree(entity);
        
        if (node.isObject()) {
            final HashMap<String, ObjectNode> map = new HashMap<>();
            
            final JsonNode issues = node.get("issues");
            if (issues.isArray()) {
                final Iterator<JsonNode> it2 = issues.iterator();
                while (it2.hasNext()) {
                    
                    this.issueCounter++;
                    
                    final JsonNode issue = it2.next();

                    final ObjectNode item = mapper.createObjectNode();
                    item.put("issueKey", issue.get("key").asText());
                    
                    
                    for (int i = 0; i < keys.length; i++) {
                        final String k = keys[i];
                        if (issue.get("fields").get(k) != null) {
                            JsonNode item2 = issue.get("fields").get(k);
                            
                            if(item2 == null || String.valueOf(item2) == "null" || (item2.isArray() && item2.size() < 1)){
                                continue;
                            }
                            
                            if (k.equals("issuetype") || k.equals("resolution") || k.equals("priority") || k.equals("status") || k.equals("issuetype")) {
                                item.put(k, item2.get("name").asText());
                            } else if (k.equals("reporter")) {
                                item.put(k, item2.get("displayName").asText());
                            } else if (k.equals("project")) {
                                item.put(k, item2.get("key").asText());
                            } else if (k.equals("components")) {
                                final ArrayNode comp = mapper.createArrayNode();
                                for(JsonNode n: item2){
                                    comp.add(n.get("name").asText());
                                }
                                item.put(k, comp.toString());
                            } else if(k.equals("progress") || k.equals("aggregateprogress")){
                                item.put(k,  item2.toString());
                            }else{
                                item.put(k, item2.asText());
                            }
                        }
                    }
                    
                    item.put("changes", 
                            mapper.readValue(this.getChangesForIssueAsString(issue.get("key").asText()), Integer.class));

                    map.put(issue.get("key").asText(), item);
                    //result.add(item);
                }

                
                
                return map;
            }
        }

        return null;
    }

    private int getTotalIssueCountForSearch(final String searchQuery) throws ClientIsNotAvailableException, IOException {
        final String url = this.url + "/search?maxResults=1&fields=total&jql=" + searchQuery;
        final ClientResponse response = this.createResponse(url);
        if (response.getStatus() == Status.OK.getStatusCode()) {
            final String entity = response.getEntity(String.class);
            final JsonNode node = mapper.readTree(entity);
            return node.get("total").asInt();
        }

        return 0;
    }
    
    
    private String getChangesForIssueAsString(final String issue) throws ClientIsNotAvailableException, IOException{
        return String.valueOf(this.getChangesForIssue(issue));
    }
    private int getChangesForIssue(final String issue) throws ClientIsNotAvailableException, IOException{
        
        /*
         * Open -> In Progress
         * Waiting for Info -> In Progress
         * Technical Review -> In Progress
         * Technical Review -> Reopen / Open
         * Test Logica -> Reopen / Open
         * Test Customer -> Reopen / Open
         * Closed -> Reopen / Opend
         * * -> Open: !From Create
         * * -> Reopen
         * * -> In Progress
         */
        
        int changes = 0;
        
        final List<String> status = new ArrayList<String>();
        status.add("Open");
        status.add("Reopened");
        status.add("In Progress");
        
        final String url = this.url + "/issue/" + issue + "?expand=changelog&fields=changelog";
        final ClientResponse response = this.createResponse(url);
        final String entity = response.getEntity(String.class);
        final JsonNode node = mapper.readTree(entity);
        final JsonNode changelog = node.get("changelog");
        if(changelog.get("total").asInt() > 0){
            final Iterator<JsonNode> it = changelog.get("histories").iterator();
            final List<JsonNode> list = new ArrayList<JsonNode>();
            while(it.hasNext()){
                final JsonNode items = it.next();
                final Iterator<JsonNode> itemsIt = it.next().iterator();
                while(itemsIt.hasNext()){
                    final JsonNode item = itemsIt.next();
                    if(item.get("field").asText().equals("status") 
                            && !item.get("fromString").equals("Create") 
                            && status.contains(item.get("toString").asText())){
                        changes++;
                    }
                }
                
            }
        }
        return changes;
    }

}
