\chapter{Verwendete Berechnungen}
\label{theory}
\lhead{\emph{Verwendete Berechnungen}}

Dieses Kapitel beschreibt die Berechnung der beiden Charakteristika Komplexität und Risikowert. Diese dienen der in Kapitel \ref{impl} beschriebenen Umsetzung und Verwendung.\\
Zu beachten ist, dass beide Werte in keiner direkten Verbindung zueinander stehen und daher getrennt betrachtet werden müssen.

\section[Komplexitätsberechnung]{Berechnung der Komplexität}
\label{Theorie:Berechnung:Komplexität}

Die in Abschnitt \ref{basic:begriffe:complexity} beschriebene Komplexität stellt mit einer einzigen Zahl dar, welcher Schwierigkeitsgrad bei der Verwendung einer Komponente zu erwarten ist. Diese Komplexität einer Komponente setzt sich aus der ihr zugeordneten Elemente und deren individuell zugeordnete Komplexität zusammen. Dabei wird der Komplexitätsfaktor eines Elements beim Hinzufügen zu einer Komponente manuell angegeben. Weil nicht alle Elemente den selben Test- und Entwicklungsumfang besitzen, modifiziert sich dieser Faktor in der Regel zwischen -1 und +1. Die folgenden Abschnitte beschreiben die Berechnung der einzelnen Faktoren.\\
Ausnahme bilden die Elemente aus den Confluence Systemen. Diese besitzen keine Daten aus denen ein Modifikationsfaktor berechnet werden kann. Ersatzweise werden die benötigten Daten aus dem entsprechenden JIRA-Issue entnommen.

\subsection[Spira Test Test Case Komplexität]{Komplexitätsberechnung eines Spira Test Test Case}
\label{theory:calc:spiraTestcase}

Die Komplexität eines SpiraTest Test Cases (C$_\text{STC}$) (siehe Formel \ref{theory:formel:complexity:spiraTestCase:resultSum}) setzt sich aus einer manuell angegebenen Komplexität (C$_{\text{MANUELL}_\text{STC}}$), sowie einem Wert (C$_{\text{MOD}_\text{STC}}$) zusammen, welcher sich aus den folgenden 12 Faktoren berechnen lässt:
\begin{itemize}
\item Dauer (D): Benötigte mittlere Ausführungszeit eines Testfalls.
\item $\varnothing$ Dauer ( $\varnothing$ D): Mittelwert der benötigten Ausführungszeit aller Testfälle.
\item Testschrittezahl (TSC): Anzahl der Schritte, die bei einem Testfall ausgeführt werden müssen.
\item $\varnothing$ Testschrittezahl ($\varnothing$ TSC): Durchschnittliche Testschittezahl aller Testfälle.
\item Testzeit (TT): Zeit, die durchschnittlich benötigt wurde den Test Case durchzuführen.
\item $\varnothing$ Testzeit ($\varnothing$ TT): Durchschnittliche Zeit aller Test Cases, die für die Durchführung benötigt wurde.
\item Fehlerzahl (Optional) (BC): Anzahl der Fehler, die einem Testfall zugeordnet sind.
\item $\varnothing$ Fehlerzahl (Optional) ($\varnothing$ BC): Mittlere Anzahl an Fehlern.
\item Fehlerpriorität (Optional) (BP): Durchschnittliche Priorität aller Fehler, die einem Testfall zugeordnet sind.
\item $\varnothing$ Fehlerpriorität (Optional) ($\varnothing$ BP): Durchschnittliche Priorität aller Fehler, die einem Testfall zugeordnet sind.
\item Warnungsanzahl (Optional) (WC): Anzahl der Warnungen, die in einem Testfall enthalten sind.
\item $\varnothing$ Warnungsanzahl (Optional) ($\varnothing$ WC): Mittlere Anzahl an Warnungen, die einem Testfall zugeordnet sind.
\end{itemize}

\begin{equation}
\label{theory:formel:complexity:spiraTestCase:resultSum}
C_{STC} = C_{MANUELL_{STC}} + C_{MOD_{STC}}
\end{equation}

Der Komplexitätsmodifikator eines Spira Test Cases (C$_{\text{MOD}_\text{STC}}$) ergibt sich aus der Summe einer Basissumme und optionalen Werten bei Vorhandensein von Fehlern bzw. Warnungen, geteilt durch die Anzahl der verschiedenen Anzahl der Faktordeltas (DC)  (siehe Formel \ref{theory:formel:complexity:spiraTestCase:resultSumMod}). Dieses Delta bewegt sich in einem Wertebereich zwischen 3 und 6.

\begin{equation}
% Gewichtete Gesamtsumme
\label{theory:formel:complexity:spiraTestCase:resultSumMod}
C_{MOD_{STC}} = \frac{C_{MOD_{STC}}BASIC + C_{MOD_{STC}}BUG + C_{MOD_{STC}}WARNING}{DC}
\end{equation}

Die Basissumme (C$_{\text{MOD}_\text{STC}}SUM$) berechnet sich aus den drei Deltas der Faktoren Dauer (D), Testschrittezahl (TSC) und Testzeit (TT) zu ihrem jeweiligen Durchschnitt ($\varnothing$ Dauer ($\varnothing$ D), $\varnothing$ Testschrittezahl ($\varnothing$ TSC) und $\varnothing$ Testzeit ($\varnothing$ TT)) (siehe Formel \ref{theory:formel:complexity:spiraTestCase:basicSum}).

\begin{equation} 
	\label{theory:formel:complexity:spiraTestCase:basicSum}
	C_{MOD_{STC}}SUM = (\varnothing D - D) + (\varnothing TSC - TSC) + (\varnothing TT - TT)
  \end{equation}

Beim Vorhandensein eines Fehlers kommen die Differenzen der Fehlerzahl (BC) und Fehlerpriorität (BP) zu ihrem jeweiligen Durchschnitt ($\varnothing$ Fehlerzahl und $\varnothing$ Fehlerpriorität) hinzu (siehe Formel \ref{theory:formel:complexity:spiraTestCase:bugSum}).

\begin{equation}
% Modifikation bei vorhandenem Fehler
\label{theory:formel:complexity:spiraTestCase:bugSum}
C_{MOD_{STC}}BUG = C_{STC}SUM + (\varnothing BC - BC) + (\varnothing BP - BP)
\end{equation}

Sind zudem Warnungen (WC) enthalten, wird die Differenz zu ihrem Durchschnitt ($\varnothing$ Warnungsanzahl) addiert (siehe Formel \ref{theory:formel:complexity:spiraTestCase:warningSum}).

\begin{equation}
% Modifikation bei vorhandenen Warnungen
\label{theory:formel:complexity:spiraTestCase:warningSum}
C_{MOD_{STC}}WARNING = C_{STC}SUM + (\varnothing WC - WC)
\end{equation}

\subsection[JIRA Issue Komplexität]{Komplexitätsberechnung eines JIRA Issues}

Die Komplexität eines JIRA Issues (C$_\text{JI}$) (siehe Formel \ref{theory:formel:complexity:JIRAIssue:resultSum}) setzt sich aus einem manuell angegebenen Komplexitätsfaktor (C$_{\text{MANUELL}_\text{JI}}$), sowie einem Wert (C$_{\text{MOD}_\text{JI}}$) zusammen, welcher sich aus aus den folgenden Faktoren berechnet:
\begin{itemize}
\item Priorität (P): Definierte Priorität des JIRA Issues.
\item $\varnothing$ Priorität ($\varnothing$ P): Durchschnittliche Priorität aller JIRA Issues.
\item Statusänderungsanzahl (SCC): Anzahl der Änderungen, die ein JIRA Issue durchlaufen hat. Der Status beschreibt den aktuellen Zustand des Issues (z.B. \glqq Open\grqq\ oder \glqq Closed\grqq).
\item $\varnothing$ Statusänderungsanzahl ($\varnothing$ SC): Durchschnittliche Anzahl an Statusänderungen aller JIRA Issues.
\item Umsetzungszeit (RT): Zeit die für die Umsetzung bzw. Fehlerbehebung benötigt wurde.
\item $\varnothing$ Umsetzungszeit ($\varnothing$ RT): Durchschnittliche Zeit aller JIRA Issues, die für die Umsetzung bzw. Fehlerbehebung benötigt wurden.
\end{itemize}

\begin{equation}
\label{theory:formel:complexity:JIRAIssue:resultSum}
C_{JI} = C_{MANUELL_{JI}} + C_{MOD_{JI}}
\end{equation}

Der Komplexitätsmodifikator (CM$_\text{STC}$)  (siehe Formel \ref{theory:formel:complexity:JIRAIssue:modSum}) berechnet sich aus den Differenzen der Durchschnitte der Priorität ($\varnothing$ P), Statusänderungsanzahl ($\varnothing$ SCC) und Umsetzungszeit ($\varnothing$ RT) zu ihrem jeweiligen Faktor (Priorität (P), Statusänderungsanzahl (SCC) und Umsetzungszeit (RT)).

\begin{equation}
\label{theory:formel:complexity:JIRAIssue:modSum}
C_{MOD_{JI}} = \frac{(\varnothing P - P) + (\varnothing SCC - SCC) + (\varnothing RT - RT)}{3}
\end{equation}


\subsection[Komponentenkomplexität]{Komplexitätsberechnung einer Komponente}
\label{theory:complexity:component}

Die Komplexität einer Komponente (C$_C$) (siehe Formel \ref{theory:formel:complexity:component}) ermittelt sich aus dem Mittelwert der Komplexitätswerte aller angefügten Elemente in zwei Stufen. In der ersten Stufen werden die Komplexitätsmittelwert der Bereiche \glqq Spezifikation\grqq, \glqq Testfälle\grqq\ und \glqq Fehler\grqq, die in Daten einer Komponente unterteilt sind, berechnet. Die summierten Einzelwerte ergeben gedrittelt die Komplexität einer Komponente. \\
\begin{itemize}
\item Spezifikation (C$_S$): Komplexitätswert eines als Spezifikation deklarieten JIRA Issues.
\item Testfall (C$_T$): Komplexitätswert eines als Test Fall deklarierten Spira Test Cases.
\item Fehler (C$_B$): Komplexitätswert eines als Fehler deklarierten JIRA Issues.
\end{itemize}

\begin{equation}
  \label{theory:formel:complexity:component}
  C_C = \frac{(\ \sum_{i=0}^{n} (C_S(i))\  /\ n\ ) +\ (\ \sum_{i=0}^{n} C_T(i))\ /\ n\ )  + (\ \sum_{i=0}^{n}C_(i)\ /\ n) }{3}
\end{equation}

\subsection[Projektkomplexität]{Komplexitätsberechnung eines Projektes}
\label{theory:complexity:projekt}

Die Komplexität eines Projektes (C$_P$) errechnet sich aus dem durchschnittlichen Wert aller verknüpften Komponenten (siehe Formel \ref{theorie:complexity:projekt}).

\begin{equation}
\label{theorie:complexity:projekt}
  C_P = (\sum_ {i = 0}^{n} C_C(i)) / n
\end{equation}


\section[Risikowertberechnung]{Berechnungn des Risikowertes}
\label{theory:risikowert}

Zur Abschätzung wie risikohaft eine Komponente oder ein Projekt ist, stehen eine Vielzahl an unterschiedlichen Zahlen und Faktoren zur Verfügung. Der \nameref{basic:begriffe:risikowert} ist eine Vereinfachung all dieser Werte und Faktoren und drückt das \nameref{basic:begriffe:risiko} einer \nameref{basic:begriffe:component} bzw. eines Projekts in einer einzigen Zahl aus. Die vereinfachte Formel des Risikowertes veranschaulicht Formel \ref{theory:formel:risikowert:basic} \cite[S. 254]{GanzITPrjMan}: 
\begin{equation}
\label{theory:formel:risikowert:basic}
  Risikowert = Eintrittswahrscheinlichkeit * Schadensh"ohe
\end{equation}

Der nachfolgende Abschnitt \ref{theory:risikowert:komponente} beschreibt die verwendete Formel auf Basis der Formel \ref{theory:formel:risikowert:basic}.\\
Die Verwendung der Eintrittswahrscheinlichkeit in der Formel ist ungeeignet. Es wird davon ausgegangen, dass jede Komponente fast sicher früher oder später einen Fehler produziert. Bei Komponenten, denen bisher kein Fehler zugeordnet wurde, besteht die Wahrscheinlichkeit, dass zukünftig dennoch ein Fehler eintritt. So stellt sich weniger die Frage ob und wie wahrscheinlich das Eintreten ist, sondern vielmehr, wie schwerwiegend die zu erwartenden Fehler in Summe sind. So wird auch weniger überlegt, welche Fehler alle auftreten können. Eine Auflistung aller möglichen Fehler wäre in den meisten Fällen sehr unpraktikabel und übermäßig teuer. Bei dem Risikowert geht es somit hauptsächlich um eine Gesamteinschätzung der Komponente.\\
Der Risikowert wird zuerst aus den angefügten Testfällen und freien Fehlern für jede Komponente einzelnen ermittelt. Aus diesen einzelnen Risikowerten errechnet sich abschließend der Gesamtrisikowert eines Projektes.

\subsection[Komponentenrisiko]{Risikowertberechnung einer Komponente}
\label{theory:risikowert:komponente}

Um einen Gesamtrisikowert (siehe Formel \ref{theory:formel:risikowert:komponente:KomponenteRisiko}) einer Komponente zu erhalten, müssen die einzelnen Risikowerte der Elemente zusammengerechnet werden. Dies geschieht durch die Bildung von drei Teilwerten. Jeder dieser Teilwerte repräsentiert die Summe einer der drei Bereiche \glqq fehlerfreie Testfälle\grqq, \glqq fehlerbehaftete Testfälle\grqq\ oder \glqq freie Fehler\grqq.\\
Diese drei Bereiche können noch durch einen vorhernfestgelegten Faktor gewichtet werden. Diese drei Faktoren sind in der Formel \glqq F$_\text{TFF}$\grqq\ für fehlerfreie Testfälle, \glqq F$_\text{TF}$\grqq\ für fehlerbehaftete Testfälle und \glqq F$_\text{FU}$\grqq\ für die unabhängigen Fehler. Der Faktorwert liegt standardmäßig bei 1.
\begin{equation}
\label{theory:formel:risikowert:komponente:KomponenteRisiko}
    RV_{Comp} = (\sum_{i = 0}^{n_{TCBL}} RV_{TCBL_i}) * F_{LTBL} +  (\sum_{i = 0}^{n_{TCCE}} RV_{TCCE_i}) * F_{LCE} +  (\sum_{i = 0}^{n_{B}} RV_ {B_i}) * F_{LB}
\end{equation}

Die Fehler einer Komponente können sowohl einem spezifizierten Testfall zugeordnet sein, als auch frei durchgeführte Tests als Quelle haben.Beide Fehlerarten benötigen daher eine eigene Betrachtung. Nur so lässt sich sicherstellen, dass alle Fehler ausreichend berücksichtig werden.\\
Die Berechnung des Risikowertes einer Komponente erfolgt in folgenden Schritten:
\begin{enumerate}
\item Berechnung der fehlerfreien und fehlerbehafteten Testfälle
\item Berechnung der freien Fehler
\item Bestimmung des Gesamtrisikowertes
\end{enumerate}

\subsection[Testfallrisiko]{Risikowert eines Testfalles}
\label{theory:risikowert:komponente:testfall}
Ein Testfall kann zwei Zusände erreichen. Ist ein Testfall bei allen vergangenen Testdurchführungen stets fehlerfrei geblieben, so erreicht er den fehlerfreien Zustand.  Im anderen Fall, erreicht er erwartungsgemäß den fehlerbehafteten Zustand.

\paragraph[Fehlerfreier Testfall]{Risikowert eines fehlerfreien Testfalles}
\label{theory:risikowert:komponente:testfall:fehlerfrei}
Diesen Zustand erreicht ein Testfall, wenn ihm kein Fehler zugeordnet ist.\\
In diesem Zustand gibt es keine Fehler, die es ermöglichen einen Risikowert direkt zu ermitteln. Es ist jedoch unwahrscheinlich, dass eine Komponente auch zukünfitig immer fehlerfrei bleiben wird.\\
Die Idee ist es, dass je umfangreicher ein Testfall ist, d.h. je mehr Schritte vorhanden sind, desto wahrscheinlicher es ist, dass zukünftig mindestens ein Fehler auftreten wird.\\
Auf dieser Annahme aufbauend, berechnet sich der Risikowert eines Testlaufes (RV$_\text{TC}$) aus der benötigten Zeit (T) und der Anzahl der Schritte (N). Diese beiden Werte werden ins Verhältnis zum jeweiligen Maximum (T$_\text{MAX}$ und N$_\text{MAX}$) in der Komponente gesetzt. Formel \ref{theory:formel:risikowert:komponente:testfall:fehlerfrei} zeigt die Berechnung:

\begin{equation}
\label{theory:formel:risikowert:komponente:testfall:fehlerfrei}
RV_{TCBL} = \frac{N_i}{N_{MAX}} + \frac{T_i}{T_{MAX}}
\end{equation}


\paragraph[Fehlerbehafteter Testfall]{Risikowert eines fehlerbehaften Testfalles}
\label{theory:risikowert:komponente:testfall:fehlerhaft}
Ist im Laufe der durchgeführten Testdurchführungen ein dokumentierter Fehler aufgetreten, ist ein Testfall im fehlerbehafteten Zustand.\\
Der Risikowert (RV$_\text{TCCE}$) eines fehlerbehafteten Testfalles berechnet sich aus einem Mehrkostenfaktor (F$_\text{AC}$) und den summierten Risikowerten (RV$_\text{TCB}$) der zugeordneten Fehler (siehe Formel \ref{theory:formel:risikowert:komponente:testfall:fehler:risk}).

\begin{equation}
\label{theory:formel:risikowert:komponente:testfall:fehler:risk}
  RV_{TCCE} = F_{AC} + \sum_{i = 0}^{n_{Fehler}}RV_{TCB_i}
\end{equation}

Für den Mehrkostenfaktor (siehe Formel \ref{theory:formel:risikowert:komponente:testfall:additionalTestCost}) wird zuerst die benötigte Zeit für Fehlerbehebung (T$_\text{B}$) und erneutes Testen (T$_\text{TC}$) ermittelt. Diese Zeiten werden mit den entsprechenden, vorher definierten Kosten (MT$_{\text{C}_\text{B}}$ für den jeweiligen Fehler und MT$_{\text{C}_\text{T}}$ für den Testfall) multipliziert (siehe auch Abschnitt \ref{impl:verwendung:projekt:config}). Für den so errechneten Wert wird der passende Faktor aus einer im Vorfeld definierten Tablle (MT$_\text{ACF}$) entnommen.

\begin{equation}
\label{theory:formel:risikowert:komponente:testfall:additionalTestCost}
F_{AC} =  MT_{ACF}(\sum_{j = 0}^{n_{Fehler}} (TCB_j * MT_{C_B} + T_{TC} * MT_{C_T}))
\end{equation}

Der Risikowert eines einzelnen Fehlers (RV$_\text{TCB}$) (siehe Formel \ref{theory:formel:risikowert:komponente:testfall:fehler:prio}) berechnet sich aus dem Verhältnis der Fixkosten (C$_\text{TCB}$) zu den Entwicklungskosten der Komponente (C$_\text{Comp}$) und der Fixzeit (T$_\text{TCB}$) im Verhältnis zu Entwicklungszeit (T$_\text{Comp}$). Um auszudrücken, dass ein kritischer Fehler stärker gewichtet werden soll, als ein trivialer Fehler, wird die Priorität als Faktor (F$_\text{TCBP}$) dazu multipliziert.

\begin{equation}
\label{theory:formel:risikowert:komponente:testfall:fehler:prio}
  RV_{TCB} = (\frac{C_{TCB}}{C_{Comp}} + \frac{T_{TCB}}{T_{Comp}}) * F_{TCBP}
\end{equation}

\subsection[Fehlerrisiko]{Berechnung der freien Fehler}

Fehler, die einer Komponente zwar zugeordnet, jedoch mit keinem Testfall in Verbindung stehen, müssen ebenso berücksichtigt werden. Ein Problem dabei ist, dass es keine Testfälle als Bezug gibt. Also muss überlegt werden, worauf sich die einzelnen Fehler beziehen können. Die einzigen zur Verfügung stehenden Bezugsgrößen sind die Priorität, Zeit und Kosten der Entwicklung der Komponente.\\
Sind der Komponente keine Spezifikationselemente zugewiesen, werden als Vergleichswerte die Durchschnitte aller verfügbaren Spezifikationselemente genommen.\\
Bei der in Formel \ref{theory:formel:risikowert:komponente:fehler:unabhaengig:gesamt} gezeigten Berechnung des Risikowertes (RV$_\text{B}$) erfolgt die Berechnung über das Verhältnis von den Kosten, die der Fehler verursacht zu den Entwicklungskosten der Komponente. Um zu gewichten, wie schwerwiegend ein Fehler ist, wird der errechnete Wert mit einem Prioritätsfaktor (F$_\text{BP}$) verrechnet. Dieser Faktor hängt von der 

\begin{equation}
\label{theory:formel:risikowert:komponente:fehler:unabhaengig:gesamt}
    RV_ B = \frac{C_B + C_{TB}}{C_{Comp}} * F_{BP}
\end{equation}

Die verursachten Kosten des Fehlers gliedern sich in die Behebungskosten und die Kosten eines erneuten Testens auf. Die Behebungskosten (siehe Formel \ref{theory:formel:risikowert:komponente:fehler:unabhaengig:bugCost}) ergeben sich aus der benötigten Zeit (T$_\text{B}$) und den vorher definierten Kosten pro Stunde die ein Entwickler zur Behebung des Fehlers (MT$_\text{C}$) benötigt. 

\begin{equation}
\label{theory:formel:risikowert:komponente:fehler:unabhaengig:bugCost}
C_B = T_B * MT_{CB}
\end{equation}

Die Kosten des erneuten Testens (siehe Formel \ref{theory:formel:risikowert:komponente:fehler:unabhaengig:bugTestCost}) berechnen sich ebenfalls auf dieselbe Weise (T$_\text{TB}$ und MT$_\text{C}$).

\begin{equation}
\label{theory:formel:risikowert:komponente:fehler:unabhaengig:bugTestCost}
C_{TB} = T_{TB} * MT_{TCB}
\end{equation}

\subsection[Projektrisiko]{Risikowert eines Projektes}
\label{theory:risikowert:projekt}

Der Risikowert eines Projektes (RV$_\text{P}$) setzt sich aus der Summe der einzelnen Risikowerten der Komponenten zusammen.

\begin{equation}
\label{theory:formel:risikowert:projekt:risiko}
    RV_P = \sum_{i = 0}^{n_{Comp}}RV_{C_i}
\end{equation}